import reducer from '../../../src/redux/reducers/modal_reducer';
import * as types from '../../../src/redux/actions/types';

const mockFunction = jest.fn();

const INITIAL_STATE = {
  archive: false,
  sell: false,
  modalArchiveFunc: null,
  modalSellFunc: null,
};

// check for initial state on startup
describe('testing modal_reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it('should handle ON_ARCHIVE_MODAL_OPEN', () => {
    expect(
      reducer(
        {},
        {
          type: types.ON_ARCHIVE_MODAL_OPEN,
          payload: mockFunction,
        }
      )
    ).toEqual({
      modalArchiveFunc: mockFunction,
    });
  });

  it('should handle ON_SELL_MODAL_OPEN', () => {
    expect(
      reducer(
        {},
        {
          type: types.ON_SELL_MODAL_OPEN,
          payload: mockFunction,
        }
      )
    ).toEqual({
      modalSellFunc: mockFunction,
    });
  });

  it('should handle SET_ACRHIVE_MODE to TRUE', () => {
    expect(
      reducer(
        {
          archive: false,
          sell: false,
          modalArchiveFunc: null,
          modalSellFunc: null,
        },
        {
          type: types.SET_ACRHIVE_MODE,
          payload: true,
        }
      )
    ).toEqual({
      archive: true,
      sell: false,
      modalArchiveFunc: null,
      modalSellFunc: null,
    });
  });

  it('should handle SET_ACRHIVE_MODE to FALSE', () => {
    expect(
      reducer(
        {
          archive: true,
          sell: false,
          modalArchiveFunc: null,
          modalSellFunc: null,
        },
        {
          type: types.SET_ACRHIVE_MODE,
          payload: false,
        }
      )
    ).toEqual({
      archive: false,
      sell: false,
      modalArchiveFunc: null,
      modalSellFunc: null,
    });
  });

  it('should handle SET_SELL_MODE to TRUE', () => {
    expect(
      reducer(
        {
          archive: false,
          sell: false,
          modalArchiveFunc: null,
          modalSellFunc: null,
        },
        {
          type: types.SET_SELL_MODE,
          payload: true,
        }
      )
    ).toEqual({
      archive: false,
      sell: true,
      modalArchiveFunc: null,
      modalSellFunc: null,
    });
  });

  it('should handle SET_SELL_MODE to FALSE', () => {
    expect(
      reducer(
        {
          archive: true,
          sell: true,
          modalArchiveFunc: null,
          modalSellFunc: null,
        },
        {
          type: types.SET_SELL_MODE,
          payload: false,
        }
      )
    ).toEqual({
      archive: true,
      sell: false,
      modalArchiveFunc: null,
      modalSellFunc: null,
    });
  });
});
