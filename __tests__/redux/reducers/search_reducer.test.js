import reducer from '../../../src/redux/reducers/search_reducer';
import * as types from '../../../src/redux/actions/types';

const INITIAL_STATE = {
  searchText: '',
  statuses: [],
  categories: [],
  searchText_friends: '',
  offset: 5,
};

// check for initial state on startup
describe('testing search_reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it('should handle ON_SEARCH_TEXT_CHANGE', () => {
    expect(
      reducer(
        {},
        {
          type: types.ON_SEARCH_TEXT_CHANGE,
          payload: 'search query',
        }
      )
    ).toEqual({
      searchText: 'search query',
    });
  });

  it('should handle ON_CATEGORIES_CHANGE', () => {
    expect(
      reducer(
        {
          categories: [],
        },
        {
          type: types.ON_CATEGORIES_CHANGE,
          payload: ['category1', 'category2'],
        }
      )
    ).toEqual({
      categories: ['category1', 'category2'],
    });
  });

  it('should handle ON_FRIEND_SEARCH_TEXT_CHANGE', () => {
    expect(
      reducer(
        {},
        {
          type: types.ON_FRIEND_SEARCH_TEXT_CHANGE,
          payload: 'search query',
        }
      )
    ).toEqual({
      searchText_friends: 'search query',
    });
  });

  it('should handle ON_SEARCH_CLEAR', () => {
    expect(
      reducer(
        {
          searchText: 'some text',
          statuses: ['status 1', 'status 2'],
          categories: ['category1', 'category2'],
          offset: 10,
        },
        {
          type: types.ON_SEARCH_CLEAR,
          payload: false,
        }
      )
    ).toEqual(INITIAL_STATE);
  });
});
