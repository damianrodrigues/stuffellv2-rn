import configureStore from 'redux-mock-store'; //ES6 modules
import * as actions from '../../../src/redux/actions';

const middlewares = [];
const mockStore = configureStore(middlewares);

const mockFunction = jest.fn();

describe('testing modal_actions', () => {
  // Initialize mockstore with empty state
  const initialState = {};
  const store = mockStore(initialState);

  // clear actions call stack
  beforeEach(() => {
    store.clearActions();
  });

  it('running ON_ARCHIVE_MODAL_OPEN with search query', () => {
    store.dispatch(actions.onArchiveModalOpen(mockFunction));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('running ON_SELL_MODAL_OPEN', () => {
    store.dispatch(actions.onSellModalOpen(mockFunction));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('running SET_ACRHIVE_MODE', () => {
    store.dispatch(actions.setArchiveMode(true));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('running SET_SELL_MODE', () => {
    store.dispatch(actions.setSellMode(true));
    expect(store.getActions()).toMatchSnapshot();
  });
});
