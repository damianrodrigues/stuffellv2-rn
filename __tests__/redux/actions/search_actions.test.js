import configureStore from 'redux-mock-store'; // ES6 modules
import * as actions from '../../../src/redux/actions';

const middlewares = [];
const mockStore = configureStore(middlewares);

describe('testing search_actions', () => {
  // Initialize mockstore with empty state
  const initialState = {};
  const store = mockStore(initialState);

  // clear actions call stack
  beforeEach(() => {
    store.clearActions();
  });

  it('running ON_SEARCH_TEXT_CHANGE with search query', () => {
    store.dispatch(actions.onSearchTextChange('search query'));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('running ON_SEARCH_CLEAR', () => {
    store.dispatch(actions.onSearchClear());
    expect(store.getActions()).toMatchSnapshot();
  });

  it('running ON_FRIEND_SEARCH_TEXT_CHANGE with search query', () => {
    store.dispatch(actions.onFriendSearchTextChange('search query'));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('running ON_STATUSES_CHANGE with a new status', () => {
    store.dispatch(actions.onStatusesChange('new status'));
    expect(store.getActions()).toMatchSnapshot();
  });

  it('running ON_CATEGORIES_CHANGE with a new category', () => {
    store.dispatch(actions.onCategoriesChange('new category'));
    expect(store.getActions()).toMatchSnapshot();
  });
});

// ANOTHER APROACH
// describe('actions', () => {
//   it('should create an action for a search query', () => {
//     const text = 'Search query';
//     const expectedAction = {
//       type: types.ON_SEARCH_TEXT_CHANGE,
//       payload: text,
//     };
//     expect(actions.onSearchTextChange(text)).toEqual(expectedAction);
//   });
//   it('should create an action for a search clear', () => {
//     const expectedAction = {
//       type: types.ON_SEARCH_CLEAR,
//     };
//     expect(actions.onSearchClear()).toEqual(expectedAction);
//   });
// });
