import gql from 'graphql-tag';
import { postFragment } from '../Post/fragment';
import { itemRequestFragment } from '../ItemRequest/fragment';

// DOCS
// =========================
/**
 *
 * @namespace Common
 * @description Common queries, like fetching posts and Item Requests all together
 * @memberof Common
 *
 */

// CREATE ITEM REQUEST OBJECT
// =========================
const Common = {};

/** ========================================
 * @namespace GetItemRequestsAndPosts
 * @description get item requests and posts
 * @memberof ItemRequest
 * @memberof Post
 */

Common.itemRequestAndPosts = gql`
  query getPostsAndItemRequests {
    posts {
      ...postFragment
    }
    itemRequests {
      ...itemRequestFragment
    }
  }
  ${postFragment}
  ${itemRequestFragment}
`;

export default Common;
