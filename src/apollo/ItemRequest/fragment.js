import gql from 'graphql-tag';
import { messageFragment } from '../Message/fragment';
import { likeFragment } from '../Like/fragment';
import { userFragment } from '../User/fragment';

export const itemRequestFragment = gql`
  fragment itemRequestFragment on ItemRequest {
    _id
    __typename
    title
    description
    createdAt
    rankScore
    timeScore
    activityScore
    totalLikes
    buyerIds
    conversationId
    numberOfComments
    currenUserHasLiked
    currentUserHasSeen
    firstComment {
      ...messageFragment
    }
    lastComments {
      ...messageFragment
    }
    lastLikes {
      ...likeFragment
    }
    owner {
      ...userFragment
    }
  }
  ${userFragment}
  ${messageFragment}
  ${likeFragment}
`;
