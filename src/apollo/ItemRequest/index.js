// TOP LEVEL IMPORTS
import Query from './Query';
import Mutation from './Mutation';

// POST
export const ItemRequest = {
  Query,
  Mutation,
};
