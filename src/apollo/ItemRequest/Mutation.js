// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

// CREATE ITEM REQUEST OBJECT
// =========================
const Mutation = {};

/** ========================================
 * @namespace createItemRequest
 * @description create an Item Request
 * @memberof ItemRequest
 */
Mutation.createItemRequest = gql`
  mutation CreateItemRequest($params: ItemRequestParams) {
    createItemRequest(params: $params) {
      _id
    }
  }
`;

/** ========================================
 * @namespace saveItemRequest
 * @description save an Item Request
 * @memberof ItemRequest
 */
Mutation.saveItemRequest = gql`
  mutation SaveItemRequest($_id: ID!, $params: ItemRequestParams) {
    saveItemRequest(_id: $_id, params: $params) {
      _id
    }
  }
`;

Mutation.markItemRequestSeen = gql`
  mutation MarkItemRequestSeen($_id: ID!) {
    markItemRequestSeen(_id: $_id) {
      _id
    }
  }
`;

// EXPORT
// ====================
export default Mutation;
