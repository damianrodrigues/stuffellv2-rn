// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
// FRAGMENTS
import { itemRequestFragment } from './fragment';
// import { userFragment } from '../User/fragment';
import { messageFragment } from '../Message/fragment';
import { likeFragment } from '../Like/fragment';
import { ownerFragment } from '../User/fragment';

// DOCS
// =========================
/**
 *
 * @namespace ItemRequest
 * @description all query names on the client will match the query code on the server
 * @memberof Queries
 *
 */

// CREATE ITEM REQUEST OBJECT
// =========================
const ItemRequest = {};

/** ========================================
 * @namespace itemRequest
 * @description get an item request by it's id
 * @memberof ItemRequest
 */
ItemRequest.itemRequest = gql`
  query GetItemRequestById($_id: ID!) {
    itemRequest(_id: $_id) {
      ...itemRequestFragment
      comments {
        ...messageFragment
      }
    }
  }
  ${itemRequestFragment}
  ${messageFragment}
`;

/** ========================================
 * @namespace itemRequests
 * @description get all item requests, typically for the HomeScreen feed.
 * @memberof Post
 */
ItemRequest.itemRequests = gql`
  query GetItemRequests($params: ItemRequestParams) {
    itemRequests(params: $params) {
      ...itemRequestFragment
    }
  }
  ${itemRequestFragment}
`;

/** ========================================
 * @namespace ItemRequestsCount
 * @description get the count of item requests in the database, AKA the "total hits".
 * This is used in infinite loading so we can see if we have more to load or not.
 * @memberof ItemRequest
 */
ItemRequest.itemRequestsCount = gql`
  query GetItemRequestCount($params: ItemRequestParams) {
    itemRequestsCount(params: $params) {
      count
    }
  }
`;

/** ========================================
 * @namespace myItemRequests
 * @description get a list of the current users posts
 * @memberof Post
 */

ItemRequest.myItemRequests = gql`
  query GetMyItemRequests {
    myItemRequests {
      ...itemRequestFragment
      firstComment {
        ...messageFragment
      }
      lastComments {
        ...messageFragment
      }
      lastLikes {
        ...likeFragment
      }
      owner {
        ...ownerFragment
      }
    }
  }
  ${itemRequestFragment}
  ${ownerFragment}
  ${messageFragment}
  ${likeFragment}
`;

/** ========================================
 * @namespace myItemRequestsCount
 * @description get the count of item requests in the database, AKA the "total hits" for the myItemRequests query.
 * This is used in infinite loading so we can see if we have more to load or not.
 * @memberof Post
 */

ItemRequest.myItemRequestsCount = gql`
  query GetMyItemRequestsCount {
    myItemRequestsCount {
      ...itemRequestFragment
    }
  }
  ${itemRequestFragment}
`;

/** ========================================
 * @namespace GetItemRequestById
 * @description get specific item Request by it's ID
 * @memberof ItemRequest
 */

ItemRequest.itemRequestById = gql`
  query GetItemRequestById($_id: ID!) {
    itemRequest(_id: $_id) {
      ...itemRequestFragment
    }
  }
  ${itemRequestFragment}
`;

// EXPORT
// ====================
export default ItemRequest;
