// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
import { postFragment } from '../Post/fragment'

const Query = { };



Query.hasUnreadNotifications = gql`
    query HasUnreadNotifications {
        hasUnreadNotifications {
        result
        }
  }
`;



Query.activityNotifications = gql`
    query ActivityNotifications {
    activityNotifications {
        _id
        parentId
        ownerId
        title
        description
        parentModelType
        message {
        _id
        post {
            _id
        }
        }
        like {
        _id
        post {
            image
            ...postFragment
        }
        }
    }
    }
    ${postFragment}
`;


export default Query;


