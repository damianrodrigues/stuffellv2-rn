// TOP LEVEL IMPORTS
import gql from 'graphql-tag';


const Mutation = { };


Mutation.hasUnreadBuying = gql`
    mutation MarkTransactionsRead {
        markTransactionsRead {
            _id
        }
    }
`;


export default Mutation;

