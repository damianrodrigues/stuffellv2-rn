// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
import { userFragment } from '../User/fragment';
import { messageFragment } from '../Message/fragment';

// create an object to store all the Conversation queries in
const Query = {};

Query.hasUnreadBuying = gql`
  query HasUnreadBuying {
    hasUnreadBuying {
      result
    }
  }
`;

Query.hasUnreadConversations = gql`
  query HasUnreadConversations {
    hasUnreadConversations {
      result
    }
  }
`;



Query.hasUnreadSelling = gql`
  query HasUnreadSelling {
    hasUnreadSelling {
      result
    }
  }
`;

Query.conversationById = gql`
  query ConversationById($_id: ID, $params: ConversationParams) {
    conversationById(_id: $_id, params: $params) {
      _id
      parentId
      userHasRead
      participantIds
      participants {
        _id
        profile {
          firstName
          lastName
          image
        }
      }
      post {
        _id
        title
        image
      }
    }
  }
`;

Query.conversations = gql`
  query Conversations($params: ConversationParams) {
    conversations(params: $params) {
      _id
      parentId
      userHasRead
      participantIds
      lastConversationOn
      participants {
        ...userFragment
      }
      post {
        _id
        price
        title
        image
        ownerId
        conversationId
        statuses
        hasUnreadConversation
      }
      itemRequest {
        _id
        title
        ownerId
        conversationId
        hasUnreadConversation
        currentUserHasSeen
      }
    }
  }
  ${userFragment}
`;

// getMessagesByConversationId
// =============================================
// here we are reusing the conversationById query, but just returning the messages
// for the conversation/chat page, we could have gotten the messages when we got the conversation
// as the conversation is the partent of a message, however we wanted to re-run the query to check for message updates
// so we broke them up into two queries. First we get the conversation (title, the post, the users involved in the conversation, etc)
// then we have a component that displays the chat messages. That component just gets the messages, and it will re-run/poll the query every X seconds
Query.getMessagesByConversationId = gql`
  query ConversationById($_id: ID, $params: ConversationParams) {
    conversationById(_id: $_id, params: $params) {
      _id
      parentId
      userHasRead
      participantIds
      messages {
        ...messageFragment
      }
    }
  }
  ${messageFragment}
`;
export default Query;
