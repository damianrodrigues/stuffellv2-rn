// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

const Mutation = {};

Mutation.markConversationRead = gql`
  mutation MarkConversationRead($_id: ID) {
    markConversationRead(_id: $_id) {
      __typename
      _id
      userHasRead
    }
  }
`;

Mutation.createConversation = gql`
  mutation CreateConversation($params: ConversationParams) {
    createConversation(params: $params) {
      _id
    }
  }
`;

export default Mutation;
