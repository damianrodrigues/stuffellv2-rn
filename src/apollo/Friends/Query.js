// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
// FRAGMENTS
import { ownerFragment } from './fragment';

const Friends = {};
// all query names on the client will match the query code on the server

Friends.getCurrentUser = gql`
  query GetFriendsOfFriends($params: UserSearchParams) {
    friendsOfFriends(params: $params) {
      ...ownerFragment
    }
  }
  ${ownerFragment}
`;

Friends.usersSearch = gql`
  query UsersSearch($params: UserSearchParams) {
    usersSearch(params: $params) {
      ...ownerFragment
    }
  }
  ${ownerFragment}
`;

Friends.getFriendsAndOutgoing = gql`
  query GetFriendsAndOutgoing($params: UserSearchParams) {
    getFriendsAndOutgoing(params: $params) {
      ...ownerFragment
    }
  }
  ${ownerFragment}
`;

export default Friends;
