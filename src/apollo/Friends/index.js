// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
// FRAGMENTS
import Query from './Query';

export const Friends = {
  Query,
};
