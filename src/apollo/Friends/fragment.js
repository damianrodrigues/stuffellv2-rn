import gql from 'graphql-tag';

export const ownerFragment = gql`
  fragment ownerFragment on User {
    _id
    __typename
    following
    follow {
      _id
      accepted
      rejected
      parentId
      ownerId
    }
    profile {
      firstName
      lastName
      image
    }
  }
`;
