// TOP LEVEL IMPORTS
import { AsyncStorage } from 'react-native';
// import fetch from 'unfetch';
// APOLLO
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setTokenStore } from 'meteor-apollo-accounts';
// APOLLO-LINK
import { ApolloLink } from 'apollo-link';
// import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { setContext } from 'apollo-link-context';
// import { RetryLink } from 'apollo-link-retry';
import { BatchHttpLink } from 'apollo-link-batch-http';
// import { createHttpLink } from 'apollo-link-http';

// SETUP METEOR-APOLLO-ACCOUNTS
// Then you'll have to define a TokenStore for your user data using setTokenStore
// (for instance when your component is mounted):
setTokenStore({
  async set({ userId, token, tokenExpires }) {
    await AsyncStorage.setItem('Meteor.userId', userId);
    await AsyncStorage.setItem('Meteor.loginToken', token);
    // AsyncStorage doesn't support Date type so we'll store it as a String
    await AsyncStorage.setItem('Meteor.loginTokenExpires', tokenExpires.toString());
  },
  async get() {
    return {
      userId: await AsyncStorage.getItem('Meteor.userId'),
      token: await AsyncStorage.getItem('Meteor.loginToken'),
      tokenExpires: await AsyncStorage.getItem('Meteor.loginTokenExpires'),
    };
  },
});

// const retryLink = new RetryLink();

// const httpLink = createHttpLink({
//   uri: 'https://stuffell-dev.meteorapp.com/graphql',
//   fetch,
// });

const httpLink = new BatchHttpLink({
  uri: 'https://stuffell-dev.meteorapp.com/graphql',
  fetch,
  // uri: 'http://172.16.12.25:3000/graphql' //'' //'https://stuffell-dev.meteorapp.com/graphql'
  // uri: 'http://172.20.10.2:3000/graphql',
  // uri: 'http://192.168.0.101:3000/graphql',
  // uri: 'http://192.168.0.15:3000/graphql',
});
// setup a variable to hold the token
let token;

// get the login token (if it exists) and add it to the context (like middleware/header setting globally)
const withToken = setContext(async () => {
  token = await AsyncStorage.getItem('Meteor.loginToken');
  return {
    headers: {
      'meteor-login-token': token || null,
    },
  };
});

// use apollo-link-error to reset tokens on errors
const resetToken = onError(err => {
  const { networkError } = err;
  console.log(`========> reset token`);
  console.log(err);
  if (networkError && networkError.statusCode === 401) {
    // remove cached token on 401 from the server
    token = undefined;
  }
});

// setup a variable to pass into ApolloLink
const authFlowLink = withToken.concat(resetToken);

const errorLink = onError(({ networkError, graphQLErrors }) => {
  console.log({ networkError, graphQLErrors });
  if (graphQLErrors) {
    graphQLErrors.map(err => {
      const { message, locations, path } = err;
      console.log('====================================');
      console.log(err);
      console.log('message');
      console.log(message);
      console.log('locations');
      console.log(locations);
      console.log('path');
      console.log(path);
      console.log('====================================');
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
    });
  }
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

// concat all the apollo link configurations together
// and create the link to pass into ApolloClient
const link = ApolloLink.from([
  // retryLink,
  authFlowLink,
  errorLink,
  httpLink,
]);

// DOCS
// =========================
/**
 * @namespace ApolloClient
 * @description Apollo client handles our client-side data layer
 */

const client = new ApolloClient({
  link,
  cache: new InMemoryCache({
    dataIdFromObject: obj => obj._id || null,
  }),
});

export default client;
