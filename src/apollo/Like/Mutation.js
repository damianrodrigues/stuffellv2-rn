// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

const Mutation = {};

Mutation.toggleLike = gql`
  mutation ToggleLike($params: LikeParams) {
    toggleLike(params: $params) {
      _id
      currenUserHasLiked
    }
  }
`;

Mutation.toggleLikeItemRequest = gql`
  mutation ToggleLikeRequest($params: LikeParams) {
    toggleLikeRequest(params: $params) {
      _id
      currenUserHasLiked
    }
  }
`;

Mutation.toggleLikePost = gql`
  mutation ToggleLikePost($params: LikeParams) {
    toggleLikePost(params: $params) {
      _id
      currenUserHasLiked
    }
  }
`;

export default Mutation;
