// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
import Query from './Query'
import Mutation from './Mutation'

// LIKE
export const Like = { 
    Query,
    Mutation
};