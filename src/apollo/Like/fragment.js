import gql from 'graphql-tag';


export const likeFragment = gql`
fragment likeFragment on Like {
    _id
    __typename
    ownerId
    userFullName
  }
`;
