// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
import Query from './Query'
import Mutation from './Mutation'

// MESSAGE
export const Message = { 
    Query,
    Mutation
};