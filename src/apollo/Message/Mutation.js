// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

const Mutation = {};

Mutation.createMessage = gql`
  mutation CreateMessage($params: MessageParams) {
    createMessage(params: $params) {
      _id
    }
  }
`;

export default Mutation;
