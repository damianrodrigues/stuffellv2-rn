// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

const Query = {};

Query.messagesByPostId = gql`
  query MessagesByPostId($postId: ID!) {
    messagesByPostId(postId: $postId) {
      _id
      createdAt
      messageValue
      post {
        _id
        title
      }
      ownerId
      userFullName
    }
  }
`;

export default Query;
