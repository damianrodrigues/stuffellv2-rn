import gql from 'graphql-tag';

export const messageFragment = gql`
  fragment messageFragment on Message {
      _id
      __typename
      messageValue
      parentId
      parentModelType
      createdAt
      userFullName
      ownerId
    }
`;