// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
// FRAGMENTS
import Mutation from './Mutation';
import Query from './Query';


export const User = { 
  Query,
  Mutation
};
