// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
// FRAGMENTS
import { userFragment } from './fragment';
import { postFragment } from '../Post/fragment';
import { messageFragment } from '../Message/fragment';

const User = {};
// all query names on the client will match the query code on the server

User.getCurrentUser = gql`
  query getCurrentUser {
    user {
      emails { address, verified },
      roles,
      profile {
        firstName
        lastName
        cellPhone
        image
        description
        workPhone
      }
      _id
    }
  }
`;

User.getUserById = gql`
  query getUserById($_id: ID!) {
    getUserById(_id: $_id) {
      _id
      emails { address, verified }
      roles
      numberOfFollowers
      numberFollowing
      numberOfPosts
      following
      follow {
        _id
        accepted
        rejected
      }
      profile { 
        firstName, 
        lastName, 
        image
        description
      }
    }
  }
`;

User.friendsOfFriends = gql`
  query GetFriendsOfFriends ($params: UserSearchParams){
    friendsOfFriends (params: $params){
      ...userFragment
    }
  }
  ${userFragment}
`;

export default User;
