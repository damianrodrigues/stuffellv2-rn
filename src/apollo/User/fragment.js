import gql from 'graphql-tag';

export const userFragment = gql`
  fragment userFragment on User {
    _id
    __typename
    following
    follow {
      _id
      accepted
      rejected
      parentId
      ownerId
    }
    profile {
      firstName
      lastName
      image
    }
  }
`;

export const ownerFragment = gql`
  fragment ownerFragment on User {
    _id
    __typename
    following
    follow {
      _id
      accepted
      rejected
      parentId
      ownerId
    }
    profile {
      firstName
      lastName
      image
    }
  }
`;
