
// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

const Mutation = { };

Mutation.saveUserImage = gql`
	mutation saveUserImage($image: String!){
        saveUserImage(image:$image) {
          _id
        }
    }
`;

Mutation.saveUserAccount = gql`
  mutation SaveUserAccount($_id: ID, $params: UserParams) {
    saveUserAccount(_id: $_id, params: $params) {
      _id
    }
  }
`;


export default Mutation;