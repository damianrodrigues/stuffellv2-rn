import gql from 'graphql-tag';
import { messageFragment } from '../Message/fragment';
import { likeFragment } from '../Like/fragment';
import { userFragment } from '../User/fragment';

export const postFragment = gql`
  fragment postFragment on Post {
    _id
    __typename
    title
    description
    image
    base64
    category
    createdAt
    statuses
    rankScore
    timeScore
    activityScore
    price
    totalLikes
    sold
    archived
    buyerIds
    base64
    conversationId
    numberOfComments
    currenUserHasLiked
    firstComment {
      ...messageFragment
    }
    lastComments {
      ...messageFragment
    }
    lastLikes {
      ...likeFragment
    }
    owner {
      ...userFragment
    }
  }
  ${userFragment}
  ${messageFragment}
  ${likeFragment}
`;
