// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
// FRAGMENTS
import { postFragment } from './fragment';
// import { userFragment } from '../User/fragment';
import { messageFragment } from '../Message/fragment';
import { likeFragment } from '../Like/fragment';
import { ownerFragment } from '../User/fragment';

// DOCS
// =========================
/**
 *
 * @namespace Post
 * @description all query names on the client will match the query code on the server
 * @memberof Queries
 *
 */

// CREATE POST OBJECT
// =========================
const Post = {};

/** ========================================
 * @namespace post
 * @description get a post by it's id
 * @memberof Post
 */
Post.post = gql`
  query GetPostById($_id: ID!) {
    post(_id: $_id) {
      ...postFragment
      comments {
        ...messageFragment
      }
    }
  }
  ${postFragment}
  ${messageFragment}
`;

/** ========================================
 * @namespace posts
 * @description get all posts, typically for the HomeScreen feed.
 * @memberof Post
 */
Post.posts = gql`
  query GetPosts($params: PostParams) {
    posts(params: $params) {
      ...postFragment
    }
  }
  ${postFragment}
`;

/** ========================================
 * @namespace postsCount
 * @description get the count of posts in the database, AKA the "total hits".
 * This is used in infinite loading so we can see if we have more to load or not.
 * @memberof Post
 */
Post.postsCount = gql`
  query GetPostsCount($params: PostParams) {
    postsCount(params: $params) {
      count
    }
  }
`;

/** ========================================
 * @namespace myPosts
 * @description get a list of the current users posts
 * @memberof Post
 */
Post.myPosts = gql`
  query GetMyPosts {
    myPosts {
      _id
      __typename
      title
      description
      image
      category
      createdAt
      statuses
      rankScore
      timeScore
      activityScore
      price
      totalLikes
      sold
      archived
      buyerIds
      conversationId
      numberOfComments
      currenUserHasLiked
      firstComment {
        ...messageFragment
      }
      lastComments {
        ...messageFragment
      }
      lastLikes {
        ...likeFragment
      }
      owner {
        ...ownerFragment
      }
    }
  }
  ${ownerFragment}
  ${messageFragment}
  ${likeFragment}
`;

/** ========================================
 * @namespace myPostsCount
 * @description get the count of posts in the database, AKA the "total hits" for the myPosts query.
 * This is used in infinite loading so we can see if we have more to load or not.
 * @memberof Post
 */

Post.myPostsCount = gql`
  query GetMyPostsCount {
    myPostsCount {
      ...postFragment
    }
  }
  ${postFragment}
`;

/** ========================================
 * @namespace GetPostById
 * @description get specific post by it's ID
 * @memberof Post
 */

Post.postById = gql`
  query GetPostById($_id: ID!) {
    post(_id: $_id) {
      ...postFragment
    }
  }
  ${postFragment}
`;

// EXPORT
// ====================
export default Post;
