// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
// FRAGMENTS
import { postFragment } from './fragment';
import { messageFragment } from '../Message/fragment';



// CREATE POST OBJECT
// =========================
const Mutation = { };




/** ========================================
 * @namespace createPost
 * @description save a post
 * @memberof Post
 */
Mutation.createPost = gql`
  mutation CreatePost($params: PostParams) {
    createPost(params: $params) {
      _id
    }
  }
`;

/** ========================================
 * @namespace savePost
 * @description save a post
 * @memberof Post
 */
Mutation.savePost = gql`
  mutation SavePost($_id: ID!, $params: PostParams) {
    savePost(_id: $_id, params: $params) {
      _id
    }
  }
`;



/** ========================================
 * @namespace togglePostArchived
 * @description save a post
 * @memberof Post
 */
Mutation.togglePostArchived = gql`
    mutation togglePostArchived ( $_id: ID!){
        togglePostArchived (_id: $_id){
            _id
            archived
        }
    }
`;

/** ========================================
 * @namespace togglePostSold
 * @description save a post
 * @memberof Post
 */
Mutation.togglePostSold = gql`
    mutation togglePostSold ( $_id: ID!){
        togglePostSold (_id: $_id){
        _id
        sold
        }
    }
`;





// EXPORT
// ====================
export default Mutation;