// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

const Mutation = {};

Mutation.createSupportMessage = gql`
  mutation CreateSupportMessage($params: MessageParams) {
    createSupportMessage(params: $params) {
      _id
    }
  }
`;

export default Mutation;
