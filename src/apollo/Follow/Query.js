// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
import { userFragment } from '../User/fragment';

const Follow = {};

Follow.incomingFollows = gql`
  query Followers($params: FollowParams) {
    incomingFollows(params: $params) {
      _id
      parentId
      owner {
        ...userFragment
        follow {
          _id
          accepted
          rejected
          parentId
          ownerId
        }
      }
      parent {
        ...userFragment
        follow {
          _id
          accepted
          rejected
          parentId
          ownerId
        }
      }
    }
  }
  ${userFragment}
`;

export default Follow;
