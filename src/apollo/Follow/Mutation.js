// TOP LEVEL IMPORTS
import gql from 'graphql-tag';

const Mutation = {};

Mutation.createFollow = gql`
  mutation CreateFollow($params: FollowParams) {
    __typename
    createFollow(params: $params) {
      _id
    }
  }
`;

Mutation.acceptFollow = gql`
  mutation AcceptFollow($_id: ID, $params: FollowParams) {
    acceptFollow(_id: $_id, params: $params) {
      __typename
      _id
    }
  }
`;

Mutation.unfollow = gql`
  mutation unfollow($_id: ID!) {
    unfollow(_id: $_id) {
      __typename
      _id
    }
  }
`;

export default Mutation;
