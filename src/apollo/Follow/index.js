// TOP LEVEL IMPORTS
import gql from 'graphql-tag';
import Query from './Query';
import Mutation from './Mutation';

// FOLLOW
export const Follow = {
  Query,
  Mutation,
};
