// @flow
import * as React from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { Text, View, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';

import { MomentHelpers } from '../../../lib/helpers';

import TouchableAvatar from '../TouchableAvatar';

import s from './styles';

const CardLeft = ({ item, navigation }) => {
  const { owner } = item;
  const { _id, profile } = owner;
  return (
    <View style={s.cardLeftContent}>
      <TouchableAvatar image={profile.image} userId={_id} navigation={navigation} />
    </View>
  );
};
const CardCenter = ({ labelColor, item }) => {
  const { owner, createdAt, title } = item;
  const { profile } = owner;
  const name = `${profile.firstName} ${profile.lastName}`;
  return (
    <View style={{ paddingVertical: 10, paddingHorizontal: 5 }}>
      <View style={s.cardCenterHeader}>
        <Text style={[s.cardCenterTitle, { color: labelColor }]}>{name}</Text>
        <Text style={s.cardHeaderTime} numberOfLines={1}>
          {MomentHelpers.timeAgo(createdAt)}
        </Text>
      </View>
      <Text numberOfLines={2} style={s.cardCenterSubtitle}>
        Is looking for {`"${title}"`}
      </Text>
    </View>
  );
};

const CardRight = ({ item, labelColor, navigation }) => (
  <View style={s.cardRightContent}>
    <TouchableWithoutFeedback
      onPressIn={() =>
        navigation.navigate('cardDetail', {
          item,
          type: 'itemRequest',
        })
      }
      style={{ flex: 1 }}>
      <FontAwesome name="angle-right" size={40} color={labelColor || '#000'} />
    </TouchableWithoutFeedback>
  </View>
);

type Props = {
  item: Object,
  navigation: Object,
};

export default ({ item, navigation }: Props) => {
  const { currentUserHasSeen } = item;
  const highlighted = !currentUserHasSeen;
  const labelColor = highlighted ? '#ff5500' : '#000';
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('cardDetail', {
          item,
          type: 'itemRequest',
        })
      }
      style={s.rowStyleItem}
      delayPressIn={1000}
      activeOpacity={0.7}>
      <View style={s.rowStyle}>
        <View style={{ flex: 2 }}>
          <CardLeft item={item} navigation={navigation} />
        </View>
        <View style={{ flex: 7 }}>
          <CardCenter labelColor={labelColor} item={item} />
        </View>
        <View style={{ flex: 1 }}>
          <CardRight labelColor={labelColor} item={item} navigation={navigation} />
        </View>
      </View>
    </TouchableOpacity>
  );
};
