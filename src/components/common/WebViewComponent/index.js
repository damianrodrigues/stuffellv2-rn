import React from 'react';
import { View, WebView, StyleSheet } from 'react-native';
// LIB
// COMPONENTS
import { TitleText } from '../../text';
import RegularButton from '../RegularButton'



// can be used for a loading screen or empty state
const WebViewComponent = ({ title, buttonTitle, animationName, onPress }) => {
    let uri;

    if (animationName === 'antenna') {
        uri = 'https://lottiefiles.com/iframe/910-antenna'
    }

    if (animationName === 'camera') {
        uri = 'https://lottiefiles.com/iframe/873-camera'
    }

    return (
            <View style={s.container}>
                <View>
                    <View style={s.webviewContainer}>
                        <WebView source={{ uri }} style={s.webview} />
                    </View>
                    <TitleText style={s.title}>
                        {title || ''}
                    </TitleText>
                    {buttonTitle && onPress && (
                        <RegularButton
                            title={buttonTitle || "Go to Friends"}
                            buttonStyle={s.buttonStyle}
                            onPress={onPress}
                        />
                    )}
                </View>
            </View>
        );
    
}


WebViewComponent.defaultProps = {
    animationName: 'antenna' // pass in simple name to decide which lottie animation to use.. in the future we should store the lottie file locally rather than use a webview
}


const s = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonStyle:{
        alignSelf: 'center', 
        width: 200, 
        marginTop: 15, 
        margin: 'auto'
    },
    webview: { 
        height: 250, 
        width: 300, 
        borderColor: '#000', 
        borderWidth: 2  
    },
    webviewContainer: { 
        height: 250, 
        width: 300  
    },
    title: { 
        textAlign: 'center',
        marginTop: 10, 
        color: '#999' 
    }
});


export default WebViewComponent;

