// @flow
import { StyleSheet } from 'react-native';

import { StyleHelpers } from '../../../lib/helpers';
import { stylesConfig, styleConstants } from '../../../lib/config';

const { normalizeHeight } = StyleHelpers;
const { fontWeight, fontSize, color } = styleConstants;

export default StyleSheet.create({
  rowStyleItem: {
    backgroundColor: '#fff',
  },
  rowStyle: {
    flexDirection: 'row',
    display: 'flex',
    width: '100%',
    height: normalizeHeight(80),
    marginBottom: 5,
  },
  cardLeftContent: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardCenterHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  cardHeaderTime: {
    color: '#aaa',
    fontWeight: fontWeight.bold,
    width: 90,
  },
  cardCenterTitle: {
    margin: 2,
    fontSize: fontSize.md,
    fontWeight: fontWeight.bold,
  },
  cardCenterSubtitle: {
    margin: 2,
    fontSize: fontSize.sm,
    fontFamily: stylesConfig.regularFont,
  },
  cardRightContent: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
