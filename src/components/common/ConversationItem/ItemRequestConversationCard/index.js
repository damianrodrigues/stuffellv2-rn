// @flow
import * as React from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { Text, View, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';

import { MomentHelpers } from '../../../../lib/helpers';

import TouchableAvatar from '../../TouchableAvatar';

import s from '../styles';

const CardLeft = ({ conversation, navigation }) => {
  const { participants } = conversation;
  const { profile } = participants[0];
  const { image } = profile;
  return (
    <View style={s.cardLeftContent}>
      <TouchableAvatar image={image} userId={participants[0]._id} navigation={navigation} />
    </View>
  );
};

const CardCenter = ({ itemRequest, labelColor, conversation }) => {
  const { lastConversationOn } = conversation;
  const { profile } = conversation.participants[0];
  const name = `${profile.firstName} ${profile.lastName}`;
  const { title } = itemRequest;
  return (
    <View style={{ paddingVertical: 10, paddingHorizontal: 5 }}>
      <View style={s.cardCenterHeader}>
        <Text style={[s.cardCenterTitle, { color: labelColor }]}>{name}</Text>
        <Text style={s.cardHeaderTime} numberOfLines={1}>
          {MomentHelpers.timeAgo(lastConversationOn)}
        </Text>
      </View>
      <Text numberOfLines={2} style={s.cardCenterSubtitle}>
        Is looking for {`"${title}"`}
      </Text>
    </View>
  );
};

const CardRight = ({ conversation, labelColor, navigation }) => (
  <View style={s.cardRightContent}>
    <TouchableWithoutFeedback
      onPressIn={() => navigation.navigate('conversation', { _id: conversation._id })}
      style={{ flex: 1 }}>
      <FontAwesome name="angle-right" size={40} color={labelColor || '#000'} />
    </TouchableWithoutFeedback>
  </View>
);

type Props = {
  conversation: Object,
  navigation: Object,
};

export default ({ conversation, navigation }: Props) => {
  const { itemRequest } = conversation;
  const highlighted = !conversation.userHasRead;
  const labelColor = highlighted ? '#ff5500' : '#000';
  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('conversation', {
          _id: conversation._id,
        })
      }
      style={s.rowStyleItem}
      delayPressIn={1000}
      activeOpacity={0.7}>
      <View style={s.rowStyle}>
        <View style={{ flex: 2 }}>
          <CardLeft conversation={conversation} navigation={navigation} labelColor={labelColor} />
        </View>
        <View style={{ flex: 7 }}>
          <CardCenter
            itemRequest={itemRequest}
            navigation={navigation}
            labelColor={labelColor}
            conversation={conversation}
          />
        </View>
        <View style={{ flex: 1 }}>
          <CardRight
            labelColor={labelColor}
            conversation={conversation}
            itemRequest={itemRequest}
            navigation={navigation}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};
