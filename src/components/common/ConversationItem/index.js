// @flow
import * as React from 'react';
import { View } from 'react-native';

import ItemRequestConversationCard from './ItemRequestConversationCard';
import PostConversationCard from './PostConversationCard';

const ConversationItem = (props: Object) => {
  const { conversationType } = props;
  switch (conversationType) {
    case 'post':
      return <PostConversationCard {...props} />;
    case 'itemRequest':
      return <ItemRequestConversationCard {...props} />;
    default:
      return <View />;
  }
};

export default ConversationItem;
