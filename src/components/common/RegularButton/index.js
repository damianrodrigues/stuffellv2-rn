// TOP LEVEL IMPORTS
import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';
import { StyleHelpers } from '../../../lib/helpers/StyleHelpers';
// COMPONENTS
import { Button } from 'react-native-elements';

const RegularButton = ({ loading, title, onPress, buttonStyle, textStyle, disabled }) => {
  return (
    <Button
      onPress={onPress}
      loading={loading}
      disabled={disabled}
      title={title}
      textStyle={[s.defaultTextStyle, textStyle]}
      buttonStyle={[s.defaultButtonStyle, buttonStyle]}
      //containerStyle={[s.defaultButtonStyle, buttonStyle]}
    />
  );
};

const s = StyleSheet.create({
  defaultTextStyle: {
    fontSize: StyleHelpers.normalizeHeight(16),
  },
  defaultButtonStyle: {
    width: Dimensions.get('window').width - 96,
    borderWidth: 0, //1,
    backgroundColor: styleConstants.color.primary,
    borderColor: styleConstants.color.buttonBorder,
    height: StyleHelpers.normalizeHeight(35),
    borderRadius: 5,
    padding: 0,
  },
});

RegularButton.defaultProps = {
  loading: false,
  title: '',
  onPress: () => {},
  buttonStyle: {},
  textStyle: {},
  disabled: false,
};

export default RegularButton;
