import React from 'react';
import { shallow } from 'enzyme';
import LoadingIndicator from './index.js';
import { reduxStore } from '../../../lib/mocks';


let DEFAULT_PROPS = { 
    ...reduxStore,
    data: {
      loading: false
    }
};



describe('<LoadingIndicator />', () => {
  const app = shallow(<LoadingIndicator {...DEFAULT_PROPS} />);
 
  it('<LoadingIndicator /> should render', () => {
    expect(app.exists()).toBe(true);
  });

});