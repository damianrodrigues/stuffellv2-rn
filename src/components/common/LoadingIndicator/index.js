import React from 'react';
import { View, ActivityIndicator, Text } from 'react-native';

const LoadingIndicator = ({ label }) => (
  <View
    style={{
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <View>
      <ActivityIndicator size="large" />
      <Text style={{ textAlign: 'center', marginTop: 5 }}>{label}</Text>
    </View>
  </View>
);

export default LoadingIndicator;
