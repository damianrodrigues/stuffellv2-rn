// @flow
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { styleConstants } from '../../../../lib/config';
import RegularButton from '../../RegularButton';

type Props = {
  onInvitePress: Function,
};

const s = StyleSheet.create({
  outterContainer: {
    backgroundColor: styleConstants.color.primary,
    borderColor: styleConstants.color.primary,
    borderRadius: 20,
    height: 300,
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 30,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    color: 'white',
  },
  subTitle: {
    textAlign: 'center',
    color: 'white',
  },
  buttonStyle: {
    width: '100%',
    borderColor: '#666',
    margin: 'auto',
    backgroundColor: '#FFF',
  },
});

const ModalContent = ({ onInvitePress }: Props) => (
  <View style={s.outterContainer}>
    <View style={s.innerContainer}>
      <Text style={s.title}>Invite your friends to join Stuffell!</Text>
      <Text style={s.subTitle}>We've created your personal invitation link.</Text>
      <Text style={s.subTitle}>
        Share this link with your friends via text message, email, Facebook, or any of your favorite
        apps!
      </Text>
      <RegularButton
        title="Copy Link"
        buttonStyle={s.buttonStyle}
        textStyle={{ color: '#666' }}
        onPress={onInvitePress}
      />
    </View>
  </View>
);

export default ModalContent;
