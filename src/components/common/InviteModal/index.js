// TOP LEVEL IMPORTS
import React from 'react';
import { View, TouchableOpacity, Clipboard } from 'react-native';

// COMPONENTS
import Modal from 'react-native-modal';
import ModalContent from './ModalContent';
// import RegularButton from '../RegularButton'
import { BodyText } from '../../text';
import { appConfig, styleConstants } from '../../../lib/config';
import SuccessModalContent from './SuccessModalContent';

// EXPORTED COMPONENTS
// =================================
class InviteModal extends React.PureComponent {
  state = {
    isVisible: false,
    success: false,
  };

  toggleModal = () => {
    this.setState({ isVisible: !this.state.isVisible });
  };

  getInviteString = () => {
    const { data } = this.props.screenProps;
    const inviteText =
      'Join me on Stuffell to buy, sell, share and give away stuff with people you know and trust: ';
    const url = `${inviteText + appConfig.apiUrl}/invite/${data.user._id}`;
    return url;
  };

  onInvitePress = async () => {
    const { data } = this.props.screenProps;
    if (data.user) {
      const url = this.getInviteString();
      await Clipboard.setString(url);
      this.setState({
        success: true, // will show new content in the modal
      });
    }
  };

  onOk = () => {
    this.setState({
      success: false,
      isVisible: false,
    });
  };

  render() {
    const { isVisible, success } = this.state;

    return (
      <View>
        <TouchableOpacity
          style={{
            position: 'relative',
          }}
          onPress={this.toggleModal}>
          <BodyText style={{ color: '#fff', marginLeft: styleConstants.defaultMargin }}>
            Invite
          </BodyText>
        </TouchableOpacity>
        <Modal
          onBackdropPress={this.toggleModal}
          onBackButtonPress={this.toggleModal}
          isVisible={isVisible}
          avoidKeyboard>
          {!success ? (
            <ModalContent onInvitePress={this.onInvitePress} />
          ) : (
            <SuccessModalContent onOk={this.onOk} />
          )}
        </Modal>
      </View>
    );
  }
}

export default InviteModal;
