import React from 'react';
import { shallow } from 'enzyme';
import SuccessModalContent from './index.js';
import { reduxStore } from '../../../../lib/mocks';


let DEFAULT_PROPS = { 
    ...reduxStore,
    data: {
      loading: false
    }
};



describe('<SuccessModalContent />', () => {
  const app = shallow(<SuccessModalContent {...DEFAULT_PROPS} />);
 
  it('<SuccessModalContent /> should render', () => {
    expect(app.exists()).toBe(true);
  });

});