// @flow
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { styleConstants } from '../../../../lib/config';
import RegularButton from '../../RegularButton';

type Props = {
  onOk: Function,
};

const s = StyleSheet.create({
  outterContainer: {
    backgroundColor: '#fff',
    borderColor: '#fff',
    borderRadius: 20,
    height: 200,
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 30,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    color: '#000',
  },
  subTitle: {
    textAlign: 'center',
    color: 'white',
  },
  buttonStyle: {
    width: '100%',
    borderColor: '#666',
    margin: 'auto',
    marginTop: 20,
    backgroundColor: styleConstants.color.primary,
  },
});

const SuccessModalContent = ({ onOk }: Props) => (
  <View style={s.outterContainer}>
    <View style={s.innerContainer}>
      <View style={{ marginBottom: 15 }}>
        <Feather name="thumbs-up" size={75} color="#55efc4" />
      </View>
      <Text style={s.title}>Link copied to clipboard!</Text>
      <Text style={s.subTitle} />
      <Text style={s.subTitle} />
      {/* <Text style={s.subTitle}>
                    We've created your personal invitation link.
                </Text>
                <Text style={s.subTitle}>
                    Share this link with your friends via text
                    message, email, Facebook, or any of your
                    favorite apps!
                </Text> */}
      <RegularButton
        title="Okay"
        buttonStyle={s.buttonStyle}
        textStyle={{ color: '#fff' }}
        onPress={onOk}
      />
    </View>
  </View>
);

export default SuccessModalContent;
