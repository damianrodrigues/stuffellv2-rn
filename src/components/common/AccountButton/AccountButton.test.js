import React from 'react';
import { shallow } from 'enzyme';
import AccountButton from './index.js';
import { reduxStore } from '../../../lib/mocks';

let DEFAULT_PROPS = {
  ...reduxStore,
  data: {
    loading: false,
  },
};

describe('<AccountButton />', () => {
  const app = shallow(<AccountButton {...DEFAULT_PROPS} />);

  it('<AccountButton /> should render', () => {
    expect(app.exists()).toBe(true);
  });
});
