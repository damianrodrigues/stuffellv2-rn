import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Feather } from '@expo/vector-icons';

import { StyleHelpers } from '../../../lib/helpers';

const AccountButton = ({ navigation }) => (
  <TouchableOpacity onPress={() => navigation.navigate('account')} style={{ marginRight: 15 }}>
    <Feather name="user" size={StyleHelpers.normalizeWidth(25)} color="#fff" />
  </TouchableOpacity>
);

export default AccountButton;
