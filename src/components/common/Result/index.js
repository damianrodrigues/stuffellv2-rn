// TOP LEVEL IMPORTS
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 18,
  },
  description: {
    textAlign: 'center',
    color: '#999',
    fontSize: 14,
  },
});

const Result = ({ type, title, description, extra, actions }) => {
  const iconMap = {
    error: <FontAwesome name="search" size={60} style={{ marginBottom: 10 }} color="#efefef" />,
    success: <FontAwesome name="search" size={60} style={{ marginBottom: 10 }} color="#efefef" />,
    empty: <FontAwesome name="search" size={60} style={{ marginBottom: 10 }} color="#efefef" />,
  };
  // const clsString = classNames(styles.result, className);
  return (
    <View
      style={{ minHeight: 200, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <View>
        <Text style={{ textAlign: 'center', marginBottom: 8 }}>{iconMap[type]}</Text>
        <Text style={styles.title}>{title}</Text>
        {description && <Text style={styles.description}>{description}</Text>}
        {extra && <Text style={{ textAlign: 'center' }}>{extra}</Text>}
        {actions && <Text style={{ textAlign: 'center' }}>{actions}</Text>}
      </View>
    </View>
  );
};

// EXAMPLE
// https://github.com/ant-design/ant-design-pro/blob/master/src/components/Result/index.js
/*
<Result 
  description={'The result page is used to feed back the results of a series of operational tasks. If only a simple operation, use the Message global prompt feedback. The text area can show a simple supplement, if there is a similar display "document" needs, the following gray area can present more complex content.'}
  title="Submitted successfully" 
  type="empty" 
/>
*/

export default Result;
