import React from 'react';
import { TouchableOpacity, Image } from 'react-native';

import { StyleHelpers } from '../../../lib/helpers';

import avatar from '../../../../assets/images/avatar.jpg';

export default class TouchableAvatar extends React.PureComponent {
  onAvatarPress = () => {
    const { navigation, userId, noAvatar } = this.props;
    if (noAvatar) {
      return null;
    }
    navigation.navigate('userDetail', { _id: userId });
  };

  render() {
    const { image } = this.props;
    const uri = image || avatar;
    return (
      <TouchableOpacity onPress={this.onAvatarPress}>
        <Image
          style={{
            height: StyleHelpers.normalizeWidth(52),
            width: StyleHelpers.normalizeWidth(52),
            borderRadius: StyleHelpers.normalizeWidth(26),
          }}
          source={typeof uri === 'string' ? { uri } : uri}
        />
      </TouchableOpacity>
    );
  }
}
