// @flow
import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { width } from 'react-native-dimension';
import { appConfig } from '../../../lib/config';
import FollowButton from '../FollowButton';
import { RegularText } from '../../text';

// DOCS
// =========================
/**
 *
 * @namespace UserCard
 * @description A card to display a user Avatar, full name, and Follow/Unfollow button.
 * Used in contructing the results for a UsersSearch component.
 * @prop targetUser {object} - the target user is the person displayed on the user card.
 * @prop currentUser {object} - The currently logged in user. we use properties from this object to match
 * against the target user and answer questions like " are these peope already friends?"
 */

type Props = {
  targetUser?: Object,
  currentUser?: Object,
  navigation: Object,
};

class UserCard extends React.PureComponent<Props> {
  static defaultProps = {
    targetUser: null,
    currentUser: null,
  };

  renderFollowButton = () => {
    const { targetUser, currentUser, incoming, follow } = this.props;

    if (targetUser && currentUser && targetUser._id === currentUser._id) {
      return null;
    }

    return <FollowButton follow={follow} targetUser={targetUser} incoming={incoming} />;
  };

  renderName = () => {
    const { targetUser, navigation } = this.props;
    return (
      <RegularText
        onPress={() =>
          navigation.navigate('userDetail', {
            _id: targetUser._id,
          })
        }>
        {`${targetUser.profile.firstName} ${targetUser.profile.lastName}`}
      </RegularText>
    );
  };

  render() {
    const { targetUser, navigation } = this.props;

    // set a default image source
    let imageSource = appConfig.images.defaultAvatar;

    // if user has a profile picture, show it
    if (targetUser.profile && targetUser.profile.image) {
      imageSource = { uri: targetUser.profile.image };
    }

    return (
      <View style={styles.cardContainer}>
        <TouchableOpacity
          style={styles.leftCol}
          onPress={() =>
            navigation.navigate('userDetail', {
              _id: targetUser._id,
            })
          }>
          <Image
            style={{
              width: 40,
              height: 40,
              borderRadius: 20,
            }}
            alt="post-avatar"
            source={imageSource}
          />
        </TouchableOpacity>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingLeft: width(8),
          }}>
          {this.renderName()}
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          {this.renderFollowButton()}
        </View>
      </View>
    );
  }
}

// STYLES
// ===================================
const styles = StyleSheet.create({
  cardContainer: {
    minHeight: 55,
    marginTop: 3,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftCol: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: width(2),
  },
});

export default UserCard;
