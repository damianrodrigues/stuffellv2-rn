import React from 'react';
import { shallow } from 'enzyme';
import ModalContent from './index.js';
import { reduxStore } from '../../../../lib/mocks';


let DEFAULT_PROPS = { 
    ...reduxStore,
    data: {
      loading: false
    }
};



describe('<ModalContent />', () => {
  const app = shallow(<ModalContent {...DEFAULT_PROPS} />);
 
  it('<ModalContent /> should render', () => {
    expect(app.exists()).toBe(true);
  });

});