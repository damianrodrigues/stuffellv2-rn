// @flow
import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { styleConstants } from '../../../../lib/config';
import RegularButton from '../../RegularButton';

type Props = {
  closeModal: Function,
  goToAccount: Function,
};

const s = StyleSheet.create({
  outterContainer: {
    backgroundColor: styleConstants.color.primary,
    borderColor: styleConstants.color.primary,
    borderRadius: 20,
    height: 300,
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 30,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    color: 'white',
  },
  subTitle: {
    textAlign: 'center',
    color: 'white',
  },
  buttonStyle: {
    width: '100%',
    borderColor: '#666',
    margin: 'auto',
    backgroundColor: '#FFF',
  },
  link: {
    color: styleConstants.color.screenBackground,
    fontWeight: styleConstants.fontWeight.bold,
    fontSize: styleConstants.fontSize.sm,
  },
});

const ModalContent = ({ closeModal, goToAccount }: Props) => (
  <View style={s.outterContainer}>
    <View style={s.innerContainer}>
      <Text style={s.title}>
        Your Item Request has been posted, and is viewable by your network.
      </Text>
      <Text style={s.subTitle}>
        You can view and edit your Item Requests on your{' '}
        <Text onPress={() => goToAccount()} style={s.link}>
          Account
        </Text>{' '}
        page
      </Text>
      <RegularButton
        title="Close"
        buttonStyle={s.buttonStyle}
        textStyle={{ color: '#666' }}
        onPress={closeModal}
      />
    </View>
  </View>
);

export default ModalContent;
