// @flow
import * as React from 'react';
import { View } from 'react-native';

// COMPONENTS
import Modal from 'react-native-modal';
import ModalContent from './ModalContent';
import RegularButton from '../RegularButton';

type Props = {
  isButtonActive: boolean,
  s: Object,
  onRequestPress: Function,
  onCloseModal: Function,
  isModalVisible: boolean,
  navigation: Object,
  clearItemRequestForm: Function,
};

// EXPORTED COMPONENTS
// =================================
class ItemRequestModalButton extends React.PureComponent<Props> {
  onCloseModalHandler = () => {
    const { onCloseModal, navigation, clearItemRequestForm } = this.props;
    onCloseModal();
    clearItemRequestForm();
    navigation.goBack(null);
  };

  onGoToAccount = () => {
    const { onCloseModal, navigation, clearItemRequestForm } = this.props;
    onCloseModal();
    clearItemRequestForm();
    navigation.navigate('account', { toMyRequests: true });
  };

  render() {
    const { s, onRequestPress, isButtonActive, isModalVisible, navigation } = this.props;
    return (
      <View>
        <RegularButton
          onPress={() => onRequestPress()}
          title="Request Item"
          disabled={isButtonActive}
          buttonStyle={s.formButton}
          textStyle={s.formButtonText}
        />
        <Modal isVisible={isModalVisible} avoidKeyboard>
          {isModalVisible && (
            <ModalContent
              navigation={navigation}
              goToAccount={this.onGoToAccount}
              closeModal={this.onCloseModalHandler}
            />
          )}
        </Modal>
      </View>
    );
  }
}

export default ItemRequestModalButton;
