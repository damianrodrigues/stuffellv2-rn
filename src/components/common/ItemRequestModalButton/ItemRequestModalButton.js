import React from 'react';
import { shallow } from 'enzyme';
import InviteModal from './index.js';
import { reduxStore } from '../../../lib/mocks';

let DEFAULT_PROPS = {
  ...reduxStore,
  data: {
    loading: false,
  },
};

describe('<InviteModal />', () => {
  const app = shallow(<InviteModal {...DEFAULT_PROPS} />);

  it('<InviteModal /> should render', () => {
    expect(app.exists()).toBe(true);
  });
});
