// @flow
import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { Feather } from '@expo/vector-icons';
// APOLLO
import { graphql, compose } from 'react-apollo';
import { Follow, ItemRequest, Conversation, Post } from '../../../apollo';
import { StyleHelpers } from '../../../lib/helpers';

const POLL_INTERVAL = 600000;

const s = StyleSheet.create({
  container: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  notification: {
    right: 0,
    top: 6,
    position: 'absolute',
    width: StyleHelpers.normalizeWidth(10),
    height: StyleHelpers.normalizeWidth(10),
    borderRadius: StyleHelpers.normalizeWidth(5),
    backgroundColor: 'red',
  },
});

// DOCS
/**
 * @namespace TabNoticationIcon
 * @description Used in the TabNavigator, usually, for the Friends and Activity tabs,
 * which have some notification-like functionality.
 * This icon will show up with a red dot when users have new messages.
 * We should probably abstract this logic to the back end...
 * @prop incomingFollows {object} -
 * an apollo query that holds an array of requests from other users who want
 * to follow me (in other words, friend me)
 * @prop hasUnreadNotificationsData {object} -
 * tells us if the current user has unread notifications.
 * @prop hasUnreadBuyingData {object} -
 * the target user is the person displayed on the user card.
 * @prop hasUnreadSellingData {object} -
 * the target user is the person displayed on the user card.
 * @prop type {string} -
 * we can pass a 'type' prop into TabNoticationIcon to determine if
 * this icon is used for the friends tab or the acvitity tab, and it's used
 * in our lofic to display the dot or not.
 */

class TabNoticationIcon extends React.PureComponent {
  render() {
    const {
      tintColor,
      type,
      name,
      incomingFollows,
      hasUnreadConversationsQuery,
      itemRequestsQuery,
      postsQuery,
    } = this.props;

    // const { itemRequests } = itemRequestsQuery;
    const { hasUnreadConversations } = hasUnreadConversationsQuery;

    // we use a switch statement to check if we should show a red circle/dot around the tab's icon.
    let highlightBadge = false;
    const hasUnseenPosts =
      postsQuery.posts && !!postsQuery.posts.find(el => el.currentUserHasSeen === false);
    const hasUnseenRequests =
      itemRequestsQuery.itemRequests &&
      !!itemRequestsQuery.itemRequests.find(el => el.currentUserHasSeen === false);

    const hasUnreadMessages =
      hasUnreadConversationsQuery.hasUnreadConversations && hasUnreadConversations.result;

    switch (type) {
      case 'main':
        if (!itemRequestsQuery.loading && !postsQuery.loading) {
          highlightBadge = hasUnseenPosts || hasUnseenRequests;
        }
        break;
      case 'activity':
        if (!itemRequestsQuery.loading && !hasUnreadConversationsQuery.loading) {
          highlightBadge = hasUnreadMessages || hasUnseenRequests;
        }
        break;
      case 'friends':
        if (!incomingFollows.loading) {
          highlightBadge = !!(
            incomingFollows.incomingFollows.length && incomingFollows.incomingFollows.length > 0
          );
        }
        break;
      default:
        highlightBadge = false;
    }
    // return the icon area
    return (
      <View style={s.container}>
        <Feather name={name} size={StyleHelpers.normalizeWidth(30)} color={tintColor} />
        {highlightBadge && <View style={s.notification} />}
      </View>
    );
  }
}

export default compose(
  graphql(Follow.Query.incomingFollows, {
    options: { pollInterval: POLL_INTERVAL },
    name: 'incomingFollows',
  }),
  graphql(Conversation.Query.hasUnreadConversations, {
    options: { pollInterval: POLL_INTERVAL },
    name: 'hasUnreadConversationsQuery',
  }),
  graphql(ItemRequest.Query.itemRequests, {
    name: 'itemRequestsQuery',
    options: () => ({
      variables: {
        params: {
          offset: 100,
        },
      },
      pollInterval: POLL_INTERVAL,
    }),
  }),
  graphql(Post.Query.posts, {
    name: 'postsQuery',
    options: () => ({
      variables: {
        params: {
          offset: 100,
        },
      },
      pollInterval: POLL_INTERVAL,
    }),
  })
)(TabNoticationIcon);
