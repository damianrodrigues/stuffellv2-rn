import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingContainer: {
    height: 150,
    backgroundColor: 'purple',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
