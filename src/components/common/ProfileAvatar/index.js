// @flow
import * as React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { ImagePicker } from 'expo';
// MODULES
import { graphql } from 'react-apollo';
import { UploadHelpers, StyleHelpers } from '../../../lib/helpers';
import ImageHelpers from '../../../lib/helpers/ImageHelpers';
import avatar from '../../../../assets/images/avatar.jpg';
// APOLLO
import { User } from '../../../apollo';

import s from './styles';

type Props = {
  user: Object,
  saveUserImage: Function,
  size?: number,
  type?: string,
  borderRadius?: number,
};

type State = {
  image: Object | null,
  loading: boolean,
};

// More info on all the options is below in the
// README...just some common use cases shown here
// const options = {
//   title: 'Select Avatar',
//   customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
//   storageOptions: {
//     skipBackup: true,
//     path: 'images',
//   },
// };

// DOCS
// ==============================
/**
 * @namespace ProfileAvatar
 * @description Reusable avatar component. When tapped, user can upload a new image.
 * @prop size {number} -
 * used to create the size of the avatar
 */

const IMAGE_PICKER_OPTIONS = {
  allowsEditing: true,
  aspect: [2, 2],
  quality: 0.2,
  base64: true,
};

// const RESIZE_OPTIONS = [
//   { resize: { width: 75, height: 75 } },
//   { crop: { originX: 0, originY: 0, width: 75, height: 75 } },
// ];

const EmptyAvatar = () => (
  <View>
    <Image
      style={{
        height: StyleHelpers.normalizeWidth(82),
        width: StyleHelpers.normalizeWidth(82),
        borderRadius: 100 / 2,
        marginRight: 20,
      }}
      source={avatar}
    />
  </View>
);

// EXPORTED COMPONENT
// ==============================
class ProfileAvatar extends React.PureComponent<Props, State> {
  static defaultProps = {
    type: 'circle',
    size: 80,
    borderRadius: 80 / 2,
  };

  state = { loading: false, image: null };

  // componentWillMount() {
  //   this.askCameraRollPermission();
  // }

  onAfterImageUpload = response => {
    const { user, saveUserImage } = this.props;
    const optimisticResponse = {
      saveUserImage: {
        _id: user._id,
        __typename: 'User',
        'profile.image': response,
      },
    };

    const refetchQueries = [{ query: User.Query.getCurrentUser }];

    saveUserImage({
      variables: { image: response },
      refetchQueries,
      optimisticResponse,
    })
      .then(() => {
        this.setState({ image: response, loading: false });
      })
      .catch(e => console.log(e));
  };

  //
  onAfterManipulation = manipResult => {
    UploadHelpers.handleFileUpload(manipResult, (error, response) => {
      if (error) {
        return console.log(error);
      }
      return this.onAfterImageUpload(response);
    });
  };

  //
  onAfterImageSelected = async result => {
    this.setState({ loading: true, image: result.base64 });

    // next we manipulate the image
    const manipResult = await ImageHelpers.makeAvatarSmaller(result.uri);

    this.onAfterManipulation(manipResult);
  };

  //
  onImageClick = async () => {
    let result;

    try {
      result = await ImagePicker.launchImageLibraryAsync(IMAGE_PICKER_OPTIONS);
    } catch (err) {
      this.setState({ loading: false });
      return console.log(err);
    }

    if (!result.cancelled) {
      return this.onAfterImageSelected(result);
    }

    return this.setState({ loading: false });
  };

  // askCameraRollPermission = async () => {
  //   const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
  //   this.setState({
  //     hasCameraRollPermission: status === 'granted',
  //   });
  // };

  render() {
    const { image, loading } = this.state;
    const { user, size, type, borderRadius } = this.props;

    let imageStyle = {
      width: size,
      height: size,
      borderRadius: type === 'circle' ? size / 2 : borderRadius,
    };

    if (image || user.profile.image) {
      imageStyle = {
        // borderStyle: "dashed",
        width: size,
        height: size,
        borderRadius: type === 'circle' ? size / 2 : borderRadius,
        borderColor: '#d6d7da',
        borderWidth: image || user.profile.image ? 0 : 2,
      };
    }

    const source = { uri: image || user.profile.image };

    return (
      <View style={[s.container, { minHeight: this.props.size }]}>
        <TouchableOpacity onPress={this.onImageClick} style={imageStyle}>
          {image || user.profile.image ? (
            <Image source={source} style={imageStyle} />
          ) : (
            <EmptyAvatar />
          )}
        </TouchableOpacity>
        {loading && (
          <View style={{ marginTop: 4 }}>
            <Text>uploading....</Text>
          </View>
        )}
      </View>
    );
  }
}

export default graphql(User.Mutation.saveUserImage, { name: 'saveUserImage' })(ProfileAvatar);
