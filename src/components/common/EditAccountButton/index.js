// TOP LEVEL IMPORTS
import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  text: {
    display: 'flex',
    fontSize: 14,
    flex: 2,
    textAlign: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  button: {
    display: 'flex',
    margin: "auto",
    justifyContent: "center",
    alignItems: "center",
    width: 110,
    borderWidth: 2,
    height: 33,
    marginTop: 3,
    flexDirection: "row"
  }
});

const EditAccountButton = ({ onPress, buttonStyle, textStyle}) => {
  // standard button styles
  let style = [styles.button, buttonStyle];

  return (
    <TouchableOpacity onPress={onPress} style={style}>
      <Text style={[styles.text, textStyle]}>Edit account</Text>
    </TouchableOpacity>
  );
};

export default EditAccountButton;
