// TOP LEVEL IMPORTS
import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
// COMPONENTS
import { BodyText } from '../../text';
// LIB
import { styleConstants } from '../../../lib/config';




// EXPORTED COMPONENT
// ====================================
const BackButton = ({ onPress, label }) => {

  if (label) {
    return (
      <TouchableOpacity onPress={onPress}>
        <BodyText
          style={{ color: '#fff', marginLeft: styleConstants.defaultMargin }}
        >
          {label}
        </BodyText>
      </TouchableOpacity>
    );
  }

  return (
    <Icon
      name="chevron-left"
      color="#fff"
      size={35}
      underlayColor={styleConstants.color.primary}
      onPress={onPress}
    />
  );
};


// PROPS
// ====================================
BackButton.defaultProps = {

}

// EXPORT
// ====================================
export default BackButton;