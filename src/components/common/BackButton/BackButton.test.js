import React from 'react';
import { shallow } from 'enzyme';
import BackButton from './index.js';
import { reduxStore } from '../../../lib/mocks';


let DEFAULT_PROPS = { 
    ...reduxStore,
    data: {
      loading: false
    }
};



describe('<BackButton />', () => {
  const app = shallow(<BackButton {...DEFAULT_PROPS} />);
 
  it('<BackButton /> should render', () => {
    expect(app.exists()).toBe(true);
  });

});