// @flow
import * as React from 'react';
import { Text, View } from 'react-native';
import s from './styles';
import { LinkText } from '../../text';

type Props = {
  navigation: Object,
};

export default ({ navigation }: Props): React.Node => {
  const { navigate } = navigation;
  return (
    <View style={s.container}>
      <Text style={s.text}>
        {"Can't"} find what {"your're"} looking for?
      </Text>
      <View style={s.linkText}>
        <LinkText
          onPress={() => navigate('modalScreen', { modalType: 'itemRequest' })}
          style={s.link}>
          Request an Item{' '}
        </LinkText>
        <Text style={s.text}>from your network.</Text>
      </View>
    </View>
  );
};
