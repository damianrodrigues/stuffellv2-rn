import { StyleSheet } from 'react-native';
//
import { styleConstants } from '../../../lib/config';
//
const { color, subHeaderHeight, fontSize } = styleConstants;
//
export default StyleSheet.create({
  container: {
    backgroundColor: color.screenBackgroundGrey,
    height: subHeaderHeight,
    justifyContent: 'center',
    alignContent: 'center',
  },
  text: {
    fontSize: fontSize.md,
    alignSelf: 'center',
  },
  linkText: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  link: {
    fontSize: fontSize.md,
    color: color.primary,
    fontWeight: '700',
  },
});
