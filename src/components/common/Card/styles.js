// @flow
import { StyleSheet } from 'react-native';

import { StyleHelpers } from '../../../lib/helpers';

export default StyleSheet.create({
  card: {
    minHeight: StyleHelpers.normalizeHeight(270),
    backgroundColor: '#fff',
    borderRadius: 3,
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 20,
  },
});
