// TOP LEVEL IMPORTS
import React from 'react';
import { View, StyleSheet } from 'react-native';
// LIB
import { MomentHelpers } from '../../../../../lib/helpers';
import { styleConstants } from '../../../../../lib/config';
// COMPONENTS
import { CaptionText } from '../../../../text';

class PostBottom extends React.PureComponent {
  state = {
    showEditForm: false,
    showCommentForm: false,
  };

  onImagePress = () => {};

  render() {
    const { createdAt } = this.props.item;

    return (
      <View style={s.container}>
        <CaptionText>{MomentHelpers.timeAgo(createdAt)}</CaptionText>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    paddingLeft: styleConstants.defaultMargin,
    paddingRight: styleConstants.defaultMargin,
    marginTop: 4,
  },
});

export default PostBottom;
