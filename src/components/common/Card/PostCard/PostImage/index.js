// @flow
import React from 'react';
import { TouchableOpacity, Dimensions } from 'react-native';
// import AutoHeightImage from 'react-native-auto-height-image'; // we may be able to move this, let's see what chris says (owner of stuffell)
import { Image } from 'react-native-expo-image-cache';
import PostOverlay from '../PostOverlay';
import EmptyImage from './EmptyImage';

type Props = {
  item?: Object,
  onPress?: Function,
  image?: String,
  sold?: Boolion,
  archived?: Boolion,
};

// preview can be a local image or a data uri

const width = Dimensions.get('window').width;

class PostImage extends React.PureComponent<Props> {
  render() {
    const { onPress, archived, item, image, sold } = this.props;

    // if we have both archive and sold options, make a combo variable
    const arhcivedAndSold = archived && sold;

    if (!image) {
      return <EmptyImage archived={archived} sold={sold} onPress={onPress} />;
    }
    const preview = {
      uri:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==',
    };

    if (item.base64) {
      console.log(`===========> item.base64`);
      console.log(item.base64);
      preview.uri = item.base64;
    }

    const uri = image; // "https://firebasestorage.googleapis.com/v0/b/react-native-e.appspot.com/o/b47b03a1e22e3f1fd884b5252de1e64a06a14126.png?alt=media&token=d636c423-3d94-440f-90c1-57c4de921641";

    return (
      <TouchableOpacity
        activeOpacity={0.85}
        style={{ minHeight: 200, width, position: 'relative' }}
        onPress={onPress}>
        <Image style={{ width, flex: 1, minHeight: 400 }} {...{ preview, uri }} />
        {/* <Image style={{ width, flex: 1, minHeight: 400 }} source={{ uri: image }} /> */}
        {archived && !arhcivedAndSold && <PostOverlay label="ARCHIVED" />}
        {sold && !arhcivedAndSold && <PostOverlay label="SOLD" />}
        {arhcivedAndSold && <PostOverlay label="ARCHIVED & SOLD" />}
      </TouchableOpacity>
    );
  }
}

PostImage.defaultProps = {
  item: {},
  onPress: () => {},
  image: '',
  sold: false,
  archived: false,
};

export default PostImage;
