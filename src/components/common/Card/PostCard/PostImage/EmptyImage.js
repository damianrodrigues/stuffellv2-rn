import React from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import PostOverlay from '../PostOverlay';

const EmptyImage = ({ children }) => (
  <View style={s.emptyImage}>
    <View>
      <Ionicons
        style={{ alignSelf: 'center' }}
        name="ios-image-outline"
        color="#dadada"
        size={80}
      />
      <Text style={s.emptyImageText}>Image coming soon...</Text>
    </View>
    {children}
  </View>
);

const EmptyImageContainer = ({ archived, sold, onPress }) => (
  <TouchableOpacity activeOpacity={0.85} style={s.container} onPress={onPress}>
    <EmptyImage style={s.image}>
      {archived && <PostOverlay label="Archived" />}
      {sold && <PostOverlay label="Sold" />}
    </EmptyImage>
  </TouchableOpacity>
);

const s = StyleSheet.create({
  emptyImageText: {
    textAlign: 'center',
    color: '#ddd',
  },
  emptyImage: {
    minHeight: 250,
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  overlayText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 20,
  },
  itemOverlayContainer: {
    bottom: 0,
    right: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    height: 50,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  image: {
    flex: 1,
    backgroundColor: 'purple',
    width: Dimensions.get('window').width,
  },
  container: {
    minHeight: 200,
    width: Dimensions.get('window').width,
    position: 'relative',
  },
});

export default EmptyImageContainer;
