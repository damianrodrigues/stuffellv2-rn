// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { BodyText, LinkText } from '../../../../../text/index';

type Props = {
  comment: Object,
  navigation: Object,
};

class CommentItem extends React.PureComponent<Props> {
  onPress = () => {
    const { comment, navigation } = this.props;
    navigation.navigate('userDetail', { _id: comment.ownerId });
  };

  render() {
    const { comment } = this.props;
    return (
      <View style={s.container}>
        <View style={{ width: '100%' }}>
          <LinkText onPress={this.onPress}>
            {comment.userFullName}: {''}
            <BodyText numberOfLines={2}>{comment.messageValue || ''}</BodyText>
          </LinkText>
        </View>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 3,
  },
});

export default CommentItem;
