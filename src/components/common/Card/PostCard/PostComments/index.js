// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { styleConstants } from '../../../../../lib/config';
import CommentItem from './CommentItem';
import LastComments from './LastComments';
import CommentForm from './CommentForm';

type Props = {
  item: Object,
  navigation: Object,
};

const s = StyleSheet.create({
  container: {
    paddingLeft: styleConstants.defaultMargin,
    paddingRight: styleConstants.defaultMargin,
  },
});

class PostComments extends React.PureComponent<Props> {
  renderFirstComment = () => {
    const { item, navigation } = this.props;
    return (
      <View>
        {item.firstComment.map(comment => (
          <CommentItem key={comment._id} comment={comment} navigation={navigation} />
        ))}
      </View>
    );
  };

  renderCommentList = () => {
    const { item, navigation } = this.props;
    return (
      <View>
        {this.renderFirstComment()}
        <LastComments item={item} navigation={navigation} />
      </View>
    );
  };

  render() {
    return (
      <View style={[{ padding: 0, position: 'relative' }, s.container]}>
        {this.renderCommentList()}
      </View>
    );
  }
}

export default PostComments;
