// @flow
import React from 'react';
import { View, TextInput, StyleSheet, Keyboard } from 'react-native';
import { Button } from 'react-native-elements';
// import KeyboardSpacer from 'react-native-keyboard-spacer';
// APOLLO
import { graphql } from 'react-apollo';
import { Message } from '../../../../../../apollo/Message';
import { Post } from '../../../../../../apollo/Post';
import { Conversation } from '../../../../../../apollo/Conversation';
// LIB
import { styleConstants } from '../../../../../../lib/config';

const s = StyleSheet.create({
  buttonStyle: {
    borderRadius: 1,
    backgroundColor: styleConstants.color.primary,
    borderWidth: 1,
    borderColor: styleConstants.color.primary,
    height: 34,
    margin: 0,
    padding: 0,
    width: '100%',
    position: 'relative',
    zIndex: 100,
  },
  containerViewStyle: {
    height: 34,
    width: '100%',
    marginLeft: 0,
    marginRight: 15,
  },
  formContainer: {
    borderTopWidth: 1,
    borderTopColor: styleConstants.color.screenBackgroundGrey,
    borderStyle: 'solid',
    marginTop: 40,
    padding: 3,
    marginBottom: 0,
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#FFF',
  },
});

type Props = {
  parentId?: String,
  placeholder?: String,
  parentModelType?: String,
  createMessage?: Function,
};

class CommentForm extends React.Component<Props> {
  state = {
    loading: false,
    messageValue: 'bbbbb',
  };

  onSuccessfulValidation = messageValue => {
    const { parentId, parentModelType, createMessage } = this.props;
    // create the variables to pass to graphql
    const variables = {
      params: {
        parentId,
        parentModelType,
        messageValue,
      },
    };

    const refetchQueries = [
      //
      { query: Post.Query.post, variables: { _id: parentId } },
      {
        query: Message.Query.messagesByPostId,
        variables: { postId: parentId },
      },
    ];

    if (parentModelType === 'postConversation') {
      refetchQueries.push({
        query: Conversation.Query.conversationById,
        variables: { _id: parentId },
      });
    }

    createMessage({ variables, refetchQueries })
      .then(() => {
        this.setState({ loading: false, messageValue: '' });
      })
      .catch(e => {
        console.log('createMessage catch');
        console.log(e);
        return this.setState({ loading: false });
      });
  };

  onChangeText = messageValue => {
    this.setState({ messageValue });
  };

  handleSubmit = () => {
    const { messageValue } = this.state;
    Keyboard.dismiss();
    // set the loading state to true
    this.setState({ loading: true }, () => {
      // do some validation on form values
      if (messageValue && messageValue.length > 2) {
        // call a function to submit to the server
        return this.onSuccessfulValidation(messageValue);
      }

      return this.setState({ loading: false });
    });
  };

  render() {
    const { placeholder, autofocus } = this.props;
    const { loading, messageValue } = this.state;

    return (
      <View style={s.formContainer}>
        <View style={{ flex: 4 }}>
          <TextInput
            clear
            returnKeyType="send"
            onSubmitEditing={this.handleSubmit}
            onChangeText={this.onChangeText}
            autoFocus={autofocus}
            // enablesReturnKeyAutomatically
            value={messageValue}
            style={{ height: 34, fontSize: 17 }}
            placeholder={placeholder}
          />
        </View>
        <View style={{ flex: 1 }}>
          <Button
            onPress={this.handleSubmit}
            loading={loading}
            buttonStyle={s.buttonStyle}
            title={loading ? '' : 'Post'}
            containerViewStyle={s.containerViewStyle}>
            Post
          </Button>
        </View>
      </View>
    );
  }
}

// Specifies the default values for props:
CommentForm.defaultProps = {
  parentId: '',
  parentModelType: '',
  placeholder: 'add a comment...',
  createMessage: () => {},
};

export default graphql(Message.Mutation.createMessage, {
  name: 'createMessage',
})(CommentForm);
