import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const PostOverlay = ({ label }) => (
  <View style={s.itemOverlayContainer}>
    <Text style={s.overlayText}>{label}</Text>
  </View>
);

const s = StyleSheet.create({
  overlayText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 20,
  },
  itemOverlayContainer: {
    bottom: 0,
    right: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    height: 50,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});

export default PostOverlay;
