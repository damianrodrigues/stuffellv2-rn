// TOP LEVEL IMPORTS
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
// COMPONENTS
import LikeItem from './LikeItem';
import { BodyText } from '../../../../text';
import { styleConstants } from '../../../../../lib/config';
// STYLES
const s = StyleSheet.create({
  lastLikersContainer: {
    maxWidth: '100%',
    paddingVertical: styleConstants.defaultMargin,
  },
  container: {
    paddingHorizontal: styleConstants.defaultMargin,
  },
});
// EXPORTED COMPONENT
class LikesSection extends React.PureComponent {
  lastLikers = () => {
    const { item, navigation } = this.props;

    if (!item.lastLikes || !item.lastLikes.length || item.lastLikes.length === 0) {
      return null;
    }

    return (
      <Text style={s.lastLikersContainer}>
        <BodyText>Liked by {''}</BodyText>
        {item.lastLikes.map((like, index) => (
          <LikeItem key={like._id} like={like} index={index} item={item} navigation={navigation} />
        ))}
      </Text>
    );
  };

  render() {
    return <View style={s.container}>{this.lastLikers()}</View>;
  }
}

export default LikesSection;
