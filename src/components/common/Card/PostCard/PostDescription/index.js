import React from 'react';
import { View, StyleSheet } from 'react-native';
import { BodyText } from '../../../../text';
import { styleConstants } from '../../../../../lib/config';

const PostDescription = ({ item, detailPage }) => {
  if (!detailPage) {
    return null;
  }
  if (!item.description) {
    return null;
  }

  return (
    <View style={s.container}>
      <BodyText>{item.description}</BodyText>
    </View>
  );
};

const s = StyleSheet.create({
  container: {
    paddingLeft: styleConstants.defaultMargin,
    paddingRight: styleConstants.defaultMargin,
  },
});

export default PostDescription;
