// @flow
import * as React from 'react';
// COMPONENTS
import PostTitle from './PostTitle';
import PostImage from './PostImage';
import PostBottom from './PostBottom';
import PostComments from './PostComments';
import PostActionsRow from './PostActionsRow';
import PostLikes from './PostLikes';
import PostDescription from './PostDescription';

type Props = {
  accountPage?: boolean,
  detailPage?: boolean,
  navigation: Object,
  item: Object,
  user: Object,
};

export default class PostCard extends React.PureComponent<Props, State> {
  static defaultProps = {
    detailPage: false,
    accountPage: false,
  };

  onImagePress = (autofocus = null) => {
    const { detailPage, navigation, item } = this.props;
    if (!detailPage) {
      navigation.navigate('cardDetail', {
        item,
        type: 'post',
        autofocus: autofocus === 'autofocus' ? true : null,
      });
    }
  };

  render() {
    const { item } = this.props;
    const { image, archived, sold } = item;

    return (
      <React.Fragment>
        <PostTitle {...this.props} />
        <PostImage onPress={this.onImagePress} archived={archived} sold={sold} image={image} />
        <PostActionsRow onPress={this.onImagePress} {...this.props} />
        <PostLikes {...this.props} />
        <PostDescription {...this.props} />
        <PostComments {...this.props} />
        <PostBottom item={item} />
      </React.Fragment>
    );
  }
}
