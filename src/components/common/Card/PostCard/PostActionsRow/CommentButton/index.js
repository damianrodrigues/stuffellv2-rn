// TOP LEVE IMPORTS
import React from 'react';
import { Feather } from '@expo/vector-icons';
import { StyleHelpers } from '../../../../../../lib/helpers';

class CommentButton extends React.PureComponent {
  render() {
    return (
      <Feather
        name="message-circle"
        size={StyleHelpers.normalizeWidth(30)}
        color="#262626"
        style={{ margin: 5, marginLeft: 0 }}
        onPress={() => this.props.onPress('autofocus')}
      />
    );
  }
}

export default CommentButton;
