import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import BuyButton from '../index';

const instance = shallow(
  <BuyButton
    store={configureStore()({})}
    item={{
      _id: 'wDMaw3LJkSaBanCvh',
      __typename: 'Post',
      title: 'Post 2',
      description: null,
      image: 'https://stuffell-west.s3.amazonaws.com/mobile1539077195810',
      base64:
        '/9j/4AAQSkZJRgABAQAAkACQAAD/4QCMRXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAIAAIdpAAQAAAABAAAAWgAAAAAAAACQAAAAAQAAAJAAAAABAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAZCgAwAEAAAAAQAAAZAAAAAA/+0AOFBob3Rvc2hvcCAzLjAAOEJJTQQEAAAAAAAAOEJJTQQlAAAAAAAQ1B2M2Y8AsgTpgAmY7PhCfv/AABEIAZABkAMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2wBDAA8PDw8PDxoPDxokGhoaJDEkJCQkMT4xMTExMT5LPj4+Pj4+S0tLS0tLS0taWlpaWlppaWlpaXZ2dnZ2dnZ2dnb/2wBDARITEx4cHjQcHDR7VEVUe3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3v/3QAEABn/2gAMAwEAAhEDEQA/APO6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//Q87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9HzuiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//0vO6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//T87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9TzuiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//1fO6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//W87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9fzuiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//0PO6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//R87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9LzuiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//0/O6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//U87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9XzuiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//1vO6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//X87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9DzuiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//0fO6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//S87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9PzuiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//1PO6KKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP//V87ooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA//9bzuiiigAooooAK1dLsorgvPcnEMQy3v7VlV0Ef7vw+5H8b8/mP8KxryaikuuhrRSbu+gqX2kyyeQ9qEjPG/uPc45/Ws3ULI2d0YFyVPKn1BqhXR6m+ILG5bk7QT78Kaz5fZzSj1Lvzxbl0CSKw0mNFuI/PnYZIJ4FNjjsNWVo4Y/s86jIA6Gl1m1lnkS9twZI3Ucrzik0W0mS5FzMCiDgbuMk8YFY3Xs/ac3vf1pY2s/acltP61uc+ylWKsMEHBFWLS1lvJhDFjPUk9APWptTULqEwH97P51TSR487CRuGDj09K7U3KN0cjSUrM6ZLHTRaXCx/vZIkJL9s4PT8q5uGJp5VhTqxAFbekf8AHje/7n9GqvoShtQU/wB1Sf0xXNGThz3d7f5G8oqfJZWv/mXJ5NN0xhbCATuB87N/+o1XvbW1ntBqNiNoBw6elZd2xe6lY93b+dbGk/PY3sR6bMj64P8AhQ4uEVUvrpcalzycLaHP1q2WnpJF9su3EcA/NvYVlU9pHZFRiSq9B2Ga6ZptWi7HNBpO7Rtaxb2sUVvJapsEgJ+o4x/OmWFrbJavqF4CyqcKo7mptY/487L/AHP6LRpYa7tZLCVGMZORIP4T75rkUn7FO/X8LnU0vatW/qwQ3mm3Ugt5bVYwxwGU8gnp2FZd9a/Y7poM5A5B9jW1a6RBHN5vnCby/mCJjJx+NY2oTS3F28kqlDnG09gKuk1z2g9P67k1U+S81qUq2rXT4I4lvNRbbG3KoOrVi1IZHkZd5J2gAZ7Adq3nFtWTsYQaTu0amt28NtdKkChQUBwPXJp9pbWsFn/aN6u8McInr9af4i/4/U/65j+ZrEMkjII2YlR0GeB+FY0050o6m1RqNSWh0FvNpuoyfZXtxCzfdZT3/IViTW0kVy1r1YNtHv6Vp6RbYc6hOdsUWTk9zWbc3DXFy9wOCzZHt6U6atNxjt+opu8E5b/oLd2c9k4ScAEjIxzWpHBZ6faR3N2nmySjKp2ArHmuJrhg87FyBjJpjySSAB2LbeBk5xVuEpJKT9bEKcYtuK9DoYBp+rBoFhEEoGVK9P6VgpBI84t1Hzltv41taXELKJtTueFxhB3YmodFzNqfmt1wzfif/wBdYqXJzuOy/M1ceblUt2WZm03Sz9n8kXEoHzFumf1qKWKwvrR7m2UQyx8lM8Ee1Png0+CZn1FmeVyWKJ/CD0zUNxp1tJbG805yyr95W6ioi1o7u/foXK+qsrdupn2llcXrMsAB2jJycU+wsmvLoW7fKBkt6gCq8NxPbktA5QkYOKiBIOQa62pO+pypxVtDde+0yBzDDarIo43MeT+YNR6jaW32ePULMFY5Dgr6H/IqO00sXUayGdE3Z+U9eD6VY1YtbwR6eiMsaHO5v4j7Y+tcy5VNRg9ep0O7g3NadCLSYbd0uJ7lN6xLnH5/4U/7dpH/AD6frU+kwiTT7kMwQNgFj2A6/wA6hSLQnYQiSQE8BzwM/lSk05yvf5XHFNRja3zMu7lt5Zd9tH5a46e9VquX1lJYzmFzkdVPqKp11wacVy7HLNPmdwoooqyQooooAKKKKACiiigD/9fzuiiigAooooAK6GxH2vSZ7NOXU7gPXv8A0rnqntrma0lE0JwR+RHoayqwco6bmlOai9diOON5ZBEgJYnAFbeuMqtBZqc+SmCfrj/CkOuzcskMauerYrFkkeVzJIdzMckmoUZSkpSVrFtxjFxi73NZzqWkBQsnyPyMcr+oq1pkt3fXYubpyY4QTnoM4qjbatcW8XkOqyxjoHHSi61e4uYvIULFGeoXvWcqc3dcqv3LVSK1u7dirMXvbt3hUsXYkAcnFVWVlYqwwRwQas2l3LZTedFjOMYPTFQzSvPK00n3mOTXSrp26GDs1fqbmkf8eN7/ALn9GqhpMywX8bN0J2n8eKit72W1ilhjCkTDDZznoRxz71TrP2V3O/X/ACL9pZRt0NDU7d7e9kVhwxLKfUHmtG0U2mkXE8nHnfKo9eMZ/U1BFrc6xiOeNJtvQsOap3uoXF8R5uAq9FHQVHLUklCS0L5oRbnFlYQzGIzBDsBwWxx+dRVeXULhLM2IxsPfHPrVGuiN9bmEraWOg1j/AI87L/c/otPnD/2FD9n+7n95j8ev41kXN7LdRRRSBQIRhcZ9hzz7U6z1C5ssiIgqeqtyDXMqUlBLqnc3dWLk+zRVh83zV8nO/Py465ra8QBftMfTfs+bH1pn9tyICYIIo2P8QFZEssk0hllO5m6k1ajKU1KStYlyjGDine5HSr94UlA4Oa6DA3vEX/H6n/XMfzNU9PsftTGWY7IY+Xb+gqC9vZb6USzBQQNvy/n3J9auW2s3NrAtvGiFV9Qc+vrXKozjSUY7nQ5QlUcpbEeoX/2nEEA2QR8Kvr7ms+MKzqrnaCQCfQVtf2/d/wDPOL8j/jWVdXL3cxnkABOOF6cVdJSS5eWy9Saji3zXuWNRtra2lVLWTzFK5PIOD9RU+n2UbIb28+WBP/Hj6CsitqPXbuKJYlSPCgAZB7fjSnGagox1HBw5uaRTv76S9lyflReEX0FSaROsF/GznAOVP4//AF6t/wBv3f8Azzi/I/41jTStNK0zAAscnHTmiMG4uDjZBKSUlNO7LuqxSRX0vmD7zFgfUGtHR1aK0ubiXiMrgZ7kZqtFrU6xiKdEmC9C45qteanc3iiN8Kg/hXgVDhUlFU2vmUpQjLnTF062tbl3F1L5YUZHIGfz9KZZ2RvZmhjcAqCRnviqNSRSyQyCWI7WXkEVvKMtWmZKUdE0NdGRijjBBwQa6H5z4fb7T/eHl564yP8A6/4VB/bkjAGaGN2H8RFULy/uL1gZjwOijgCsnGc2lJWsaJwgnZ3uPt7CS4tJLiJsmM8pjk+9UVRnYIgyTwAKntbuezk8yA4PQg9D9a0zrkoy0cMauerAc1bdRN2VyUoNK7sP10hTbwk5dE+b9P8ACsCpJZZJpDLKdzN1JqOqpQ5IqJNSfNJyCiiitDMKKKKACiiigAooooA//9k=',
      category: 'Food & Produce',
      createdAt: '2018-10-09T09:26:45.579Z',
      statuses: ['For Free'],
      rankScore: null,
      timeScore: null,
      activityScore: 9999,
      price: '0',
      totalLikes: 0,
      sold: false,
      archived: false,
      buyerIds: [],
      conversationId: null,
      numberOfComments: 2,
      currenUserHasLiked: false,
      firstComment: [
        {
          _id: 'C8bwoJRetWR4D4WXY',
          __typename: 'Message',
          messageValue: 'Gggg',
          parentId: 'wDMaw3LJkSaBanCvh',
          parentModelType: 'post',
          createdAt: '2018-10-10T20:14:37.640Z',
          userFullName: 'Damian Rodriguez',
          ownerId: 'DfJGPZbQ6ox4c6Ld2',
        },
      ],
      lastComments: [
        {
          _id: 'urA7cteP6y4Sgm9Ms',
          __typename: 'Message',
          messageValue: 'Huuu',
          parentId: 'wDMaw3LJkSaBanCvh',
          parentModelType: 'post',
          createdAt: '2018-10-11T05:31:40.333Z',
          userFullName: 'Damian Rodriguez',
          ownerId: 'DfJGPZbQ6ox4c6Ld2',
        },
      ],
      lastLikes: [],
      owner: {
        _id: 'DfJGPZbQ6ox4c6Ld2',
        __typename: 'User',
        following: 'accepted',
        follow: {
          _id: 'Zvxc8sRytuxRsAfK7',
          accepted: true,
          rejected: false,
          parentId: 'TgcTAeWywrrtKBamQ',
          ownerId: 'DfJGPZbQ6ox4c6Ld2',
          __typename: 'Follow',
        },
        profile: {
          firstName: 'Damian',
          lastName: 'Rodriguez',
          image: null,
          __typename: 'Profile',
        },
      },
    }}
    user={{
      user: {
        _id: 'TgcTAeWywrrtKBamQ',
        emails: [
          {
            address: 'bugdonepavlov@gmail.com',
            verified: false,
            __typename: 'Email',
          },
        ],
        roles: null,
        numberOfFollowers: 3,
        numberFollowing: 3,
        numberOfPosts: 3,
        following: 'none',
        follow: null,
        profile: {
          firstName: 'Bogdan',
          lastName: 'pavlov',
          image: null,
          description: null,
          __typename: 'Profile',
        },
        __typename: 'User',
      },
    }}
  />
);

describe('<BuyButton />', () => {
  it('<BuyButton /> should render', () => {
    expect(instance.exists()).toBeTruthy();
    expect(instance).toMatchSnapshot();
  });
});
