// TOP LEVE IMPORTS
import React from 'react';
import { graphql, compose } from 'react-apollo';
import { StyleSheet, View } from 'react-native';
// APOLLO
import { Conversation } from '../../../../../../apollo';

import RegularButton from '../../../../RegularButton';

class BuyButton extends React.PureComponent {
  state = { loading: false };

  onBuyClick = () => {
    const { item, navigation, user } = this.props;
    const conversationId = item.conversationId;

    const conversationExists = this.conversationExists();
    const name = `${item.owner.profile.firstName} ${item.owner.profile.lastName}`;

    // if the clicked post was created by the current user, then just brin gthem to the activity tab
    if (user._id === item.owner._id) {
      return navigation.navigate('activity');
    }

    // if a conversation already exists, then bring the user directly to the given conversation
    if (conversationExists) {
      return navigation.navigate('conversation', { _id: conversationId, name });
    }

    this.setState({ loading: true });
    return navigation.navigate('modalScreen', {
      modalType: 'newMessage',
      _id: conversationId,
      name,
      item,
    });
  };

  conversationExists = () => {
    const { item, user } = this.props;

    return (
      item.buyerIds &&
      item.buyerIds.length > 0 &&
      user &&
      user._id &&
      item.buyerIds.includes(user._id)
    );
  };

  render() {
    const { item, user } = this.props;

    if (user._id === item.owner._id) {
      return null;
    }

    return (
      <View style={styles.container}>
        <RegularButton
          onPress={this.onBuyClick}
          title="Ask / Buy"
          textStyle={styles.buttonTextStyle}
          buttonStyle={styles.buttonStyle}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonTextStyle: {
    display: 'flex',
    fontSize: 14,
    color: '#000',
    flex: 2,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyle: {
    // display: 'flex',
    // margin: 'auto',
    // justifyContent: 'center',
    // alignItems: 'center',
    width: 110,
    borderWidth: 2,
    height: 33,
    marginTop: 3,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  iconContainer: {
    flex: 1,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default compose(
  graphql(Conversation.Mutation.createConversation, {
    name: 'createConversation',
  })
)(BuyButton);
