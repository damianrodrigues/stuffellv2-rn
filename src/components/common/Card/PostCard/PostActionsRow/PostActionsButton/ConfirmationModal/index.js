// TOP LEVEL IMPORTS
import React from 'react';
import { View, ActivityIndicator } from 'react-native';
// COMPONENTS
import Modal from 'react-native-modal';
import { styleConstants } from '../../../../../../../lib/config/styleConstants';
import { BodyText, TitleText } from '../../../../../../text';
import RegularButton from '../../../../../RegularButton';

const ConfirmationModalContent = ({
  loading,
  onCancelText = 'Cancel',
  onOkText = 'Okay',
  onCancel,
  onOk,
  modalContentContainerStyle,
  modalContainerStyles,
  children,
  title,
}) => (
  <View style={modalContainerStyles}>
    <View style={s.modalTitleContainer}>
      <TitleText style={s.title}>{title}</TitleText>
    </View>
    <View style={modalContentContainerStyle}>{children}</View>
    <View style={s.buttonContainer}>
      <RegularButton
        onPress={onOk}
        title={onOkText}
        buttonStyle={{
          width: 130,
          borderWidth: 2,
          height: 33,
          borderColor: 'transparent',
        }}
        disabled={loading}
      />
      <RegularButton
        onPress={onCancel}
        title={onCancelText}
        textStyle={s.textStyle}
        buttonStyle={s.buttonStyle}
        disabled={loading}
      />
    </View>
  </View>
);

// EXPORTED COMPONENTS
// =================================
class ConfirmationModal extends React.PureComponent {
  renderText = () => {
    if (!this.props.type) {
      return null;
    }

    if (this.props.loading) {
      return <ActivityIndicator size="large" />;
    }
    let text = '';

    switch (this.props.type) {
      case 'onUnarchive':
        text = 'Your item will become active again and will be viewable by other users.';
        break;
      case 'onArchive':
        text =
          'Your item will no longer be visible to other users. You will still see your item in your inventory. You can unarchive your item at any time.';
        break;
      case 'onEdit':
        text = 'Want to edit this item?';
        break;
    }

    if (this.props.type === 'onMarkAsSold') {
      text =
        'Your item will be marked as sold. Stuffell automatically archives sold items after two weeks.';
    }
    if (this.props.type === 'onMarkAsUnsold') {
      text =
        'Your item will be marked as sold. Stuffell automatically archives sold items after two weeks.';
    }
    if (this.props.type === 'onEdit') {
      text = 'Want to edit this item?';
    }

    return <BodyText style={{ textAlign: 'center' }}>{text}</BodyText>;
  };

  render() {
    return (
      <Modal
        onBackdropPress={this.props.onCancel}
        onBackButtonPress={this.props.onCancel}
        isVisible={this.props.isVisible}
        avoidKeyboard>
        <ConfirmationModalContent
          renderText={this.renderText}
          title="Are You Sure?"
          onOk={this.props.onOk}
          onCancel={this.props.onCancel}
          loading={this.props.loading}
          modalContentContainerStyle={s.modalContentContainerStyle}
          modalContainerStyles={[this.props.modalContainerStyles, s.defaultModalContainerStyles]}>
          {this.renderText()}
        </ConfirmationModalContent>
      </Modal>
    );
  }
}

ConfirmationModal.defaultProps = {
  modalContainerStyles: {},
};

const s = {
  defaultModalContainerStyles: {
    backgroundColor: '#fff',
    height: 235,
    borderRadius: 5,
  },
  title: {
    textAlign: 'center',
    color: '#fff',
  },
  modalContentContainerStyle: {
    padding: 10,
    paddingTop: 20,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 5,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
    right: 0,
    left: 0,
  },
  textStyle: {
    display: 'flex',
    fontSize: 14,
    color: '#000',
    flex: 2,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyle: {
    display: 'flex',
    margin: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    width: 130,
    borderWidth: 2,
    height: 33,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  modalTitleContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    height: 50,
    backgroundColor: styleConstants.color.primary,
  },
};

export default ConfirmationModal;
