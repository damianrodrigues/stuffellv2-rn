import React from 'react';
import { View } from 'react-native';
import { Feather } from '@expo/vector-icons';
import ActionSheet from 'react-native-actionsheet';
// APOLLO
import { graphql, compose } from 'react-apollo';
import { Post } from '../../../../../../apollo';
// LIB
import { StyleHelpers } from '../../../../../../lib/helpers';
import ConfirmationModal from './ConfirmationModal';

class EditButton extends React.PureComponent {
  state = {
    loading: false,
    isVisible: false,
    type: null,
  };

  onEdit = () => {
    const { item, navigation } = this.props;
    this.onCancel();
    navigation.navigate('modalScreen', {
      item,
      modalType: 'editPost',
    });
    this.props.onPostFormFieldChange({
      _id: item._id,
      ...item,
      title: {
        value: item.title,
      },
      description: {
        value: item.description,
      },
      statuses: {
        value: item.statuses,
      },
      category: {
        value: item.category,
      },
      price: {
        value: item.price,
      },
    });

    if (item.price) {
      this.props.onPostFormPriceChange(item.price);
    }
  };

  onCancel = () => {
    this.setState({
      isVisible: false,
      type: null,
      loading: false,
    });
  };

  onOk = () => {
    // fire off the relevant action
    switch (this.state.type) {
      case 'onEdit':
        return this.onEdit();
      case 'onArchive':
        return this.onToggleArchive();
      case 'onUnarchive':
        return this.onToggleArchive();
      case 'onMarkAsSold':
        return this.onToggleSold();
      case 'onMarkAsUnsold':
        return this.onToggleSold();
    }

    // reset the local state as a default
    this.setState({
      isVisible: false,
      type: null,
    });
  };

  onToggleSold = () => {
    const { item, togglePostSold } = this.props;
    const variables = { _id: item._id };
    this.setState({ loading: true });
    const optimisticResponse = {
      togglePostSold: {
        _id: item._id,
        __typename: 'Post',
        sold: !item.sold,
      },
    };
    togglePostSold({
      variables,
      optimisticResponse,
    })
      .then(() => this.onCancel())
      .catch(e => {
        this.onCancel();
        console.log(e);
      });
  };

  onToggleArchive = () => {
    const { item, togglePostArchived } = this.props;
    const variables = { _id: item._id };
    this.setState({ loading: true });
    const optimisticResponse = {
      togglePostArchived: {
        _id: item._id,
        __typename: 'Post',
        archived: item.archived,
      },
    };
    togglePostArchived({ variables, optimisticResponse })
      .then(() => this.onCancel())
      .catch(e => console.log(e));
  };

  // setModal will setup the values for the confirmation modal that shows after selecting form the actions sheet
  setModal = type => {
    this.setState({
      isVisible: true,
      type,
    });
  };

  // onSelect runs after user selects from the action sheet
  onSelect = selected => {
    // use a switch to figure out what the user selected
    switch (selected) {
      case 'Cancel':
        return this.onCancel();
      case 'Edit':
        return this.setModal('onEdit');
      case 'Archive':
        return this.setModal('onArchive');
      case 'Unarchive':
        return this.setModal('onUnarchive');
      case 'Mark as sold':
        return this.setModal('onMarkAsSold');
      case 'Mark as unsold':
        return this.setModal('onMarkAsUnsold');
      default:
        return this.onCancel();
    }
  };

  showActionSheet = () => {
    this.ActionSheet.show();
  };

  getButtons = () => {
    const { item } = this.props;

    // create an array that will hold the buttons we show in the ActionSheet
    // note: we always show "Edit", the rest are based on the "status" of the post
    const buttons = ['Edit'];

    // check if statuses exist
    if (item && item.statuses) {
      // if item is for sale and not sold, show "mark as sold"
      if (item.statuses.includes('For Sale') && !item.sold) {
        buttons.push('Mark as sold');
      }

      // if the item is for sale and already marked as sold, add an option to "mark as unsold"
      if (item.statuses.includes('For Sale') && item.sold) {
        buttons.push('Mark as unsold');
      }

      // if the item is not archived, then add a button for them to be able to archive it
      if (!item.archived) {
        buttons.push('Archive');
      }

      // if the item is  archived, then add a button for them to be able to unarchive it
      if (item.archived) {
        buttons.push('Unarchive');
      }
    }

    buttons.push('Cancel'); // we push cancel last because we want it to be the bottom (i.e. the last) button

    return buttons;
  };

  render() {
    const BUTTONS = this.getButtons(); // ['Edit', 'Archive', 'Mark as Sold', 'Delete', 'Cancel'];
    const CANCEL_INDEX = BUTTONS.length - 1;
    const DESTRUCTIVE_INDEX = BUTTONS.length - 2;

    if (!this.props.user) {
      return null;
    }

    const message = `What would you like to do with ${(this.props.item.title &&
      `"${this.props.item.title}"`) ||
      'this'}?`;

    if (this.props.user._id === this.props.item.owner._id) {
      return (
        <View>
          <Feather
            onPress={this.showActionSheet}
            name="edit"
            size={StyleHelpers.normalizeWidth(28)}
            color="#262626"
            style={{ margin: 5, marginLeft: 0 }}
          />
          <ConfirmationModal
            onCancel={this.onCancel}
            onOk={this.onOk}
            isVisible={this.state.isVisible}
            type={this.state.type}
            loading={this.state.loading}
          />
          <ActionSheet
            ref={o => (this.ActionSheet = o)}
            message={message}
            options={BUTTONS}
            cancelButtonIndex={CANCEL_INDEX}
            destructiveButtonIndex={DESTRUCTIVE_INDEX}
            onPress={buttonIndex => this.onSelect(BUTTONS[buttonIndex])}
          />
        </View>
      );
    }
    return null;
  }
}

const ComponentWithData = compose(
  graphql(Post.Mutation.togglePostSold, {
    name: 'togglePostSold',
  }),
  graphql(Post.Mutation.togglePostArchived, {
    name: 'togglePostArchived',
  })
)(EditButton);

export default ComponentWithData;
