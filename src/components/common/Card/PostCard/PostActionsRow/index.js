// @flow
import React from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import PostActionsButton from './PostActionsButton';
import LikeButton from './LikeButton';
import CommentButton from './CommentButton';
import BuyButton from './BuyButton';
import { styleConstants } from '../../../../../lib/config';
import { StyleHelpers } from '../../../../../lib/helpers';

type Props = {
  item?: Object,
  user?: Object,
};

// STYLES
// ===================================
const s = StyleSheet.create({
  container: {
    height: StyleHelpers.normalizeWidth(43),
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
    paddingHorizontal: styleConstants.defaultMargin,
  },
  buyButtonContainer: {
    flex: 1,
    top: 5,
    left: Dimensions.get('window').width / 2 - 65,
    alignSelf: 'center',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

// EXPORTED COMPONENT
// ===================================
class PostActionsRow extends React.PureComponent<Props> {
  render() {
    const { item, user, navigation, accountPage, detailPage, onPress } = this.props;
    return (
      <View style={s.container}>
        {!accountPage && (
          <View style={{ flex: 1 }}>
            <LikeButton item={item} user={user} />
          </View>
        )}
        {!detailPage && (
          <View style={{ flex: 1 }}>
            <CommentButton onPress={onPress} />
          </View>
        )}
        <View style={{ flex: 1 }}>
          <PostActionsButton item={item} user={user} navigation={navigation} {...this.props} />
        </View>
        <View style={{ flex: 1, elevation: 10 }} />
        <View style={s.buyButtonContainer}>
          <BuyButton item={item} user={user} navigation={navigation} />
        </View>
        <View style={{ flex: 1 }} />
        <View
          style={{
            flex: 2,
          }}
        />
      </View>
    );
  }
}

// Specifies the default values for props:
PostActionsRow.defaultProps = {
  item: null,
  user: null,
};

// EXPORT
// ===================================
export default PostActionsRow;
