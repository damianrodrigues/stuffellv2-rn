// @flow
import React from 'react';
import { Feather } from '@expo/vector-icons';
import { graphql, compose } from 'react-apollo';
import { Like } from '../../../../../../apollo/Like';
import { StyleHelpers } from '../../../../../../lib/helpers';

type Props = {
  item?: Object,
  user?: Object,
  toggleLikePost: Function,
};

class LikeButton extends React.PureComponent<Props> {
  onLikeClick = () => {
    const { item, toggleLikePost, user } = this.props;

    if (!user || user._id === item.owner._id) {
      return;
    }

    const variables = {
      params: {
        parentId: item._id,
        parentModelType: 'Post',
      },
    };

    const optimisticResponse = {
      toggleLikePost: {
        _id: item._id,
        __typename: 'Post',
        currenUserHasLiked: !item.currenUserHasLiked,
        numberOfLikes: !item.currenUserHasLiked ? item.numberOfLikes - 1 : item.numberOfLikes + 1,
      },
    };
    toggleLikePost({ variables, optimisticResponse })
      .then(res => {})
      .catch(e => console.log(e));
  };

  render() {
    const { item } = this.props;

    return (
      <Feather
        name={item.currenUserHasLiked ? 'heart' : 'heart'}
        size={StyleHelpers.normalizeWidth(30)}
        color={item.currenUserHasLiked ? '#e74c3c' : '#262626'}
        style={{ margin: 5, marginLeft: 0 }}
        onPress={this.onLikeClick}
      />
    );
  }
}

LikeButton.defaultProps = {
  item: null,
  user: null,
};

const ComponentWithData = compose(
  graphql(Like.Mutation.toggleLikePost, { name: 'toggleLikePost' })
)(LikeButton);

export default ComponentWithData;
