import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import LikeButton from '../index';

const instance = shallow(
  <LikeButton
    store={configureStore()({})}
    screenProps={{
      data: {
        user: {
          _id: 'TgcTAeWywrrtKBamQ',
          emails: [
            {
              address: 'bugdonepavlov@gmail.com',
              verified: false,
              __typename: 'Email',
            },
          ],
          roles: null,
          numberOfFollowers: 3,
          numberFollowing: 3,
          numberOfPosts: 3,
          following: 'none',
          follow: null,
          profile: {
            firstName: 'Bogdan',
            lastName: 'pavlov',
            image: null,
            description: null,
            __typename: 'Profile',
          },
          __typename: 'User',
        },
      },
    }}
  />
);

describe('<LikeButton />', () => {
  it('<LikeButton /> should render', () => {
    expect(instance.exists()).toBeTruthy();
    expect(instance).toMatchSnapshot();
  });
});
