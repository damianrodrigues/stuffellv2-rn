// @flow
import * as React from 'react';
import { View } from 'react-native';
import PostCard from './PostCard';
import ItemRequestCard from './ItemRequestCard';
import s from './styles';

type Props = {
  navigation: Object,
  item: Object,
  user: Object,
};

class Card extends React.PureComponent<Props, State> {
  render() {
    const { item, detailPage } = this.props;
    const isPost = item.__typename === 'Post';
    const isItemRequest = item.__typename === 'ItemRequest';

    return (
      <View
        style={
          !detailPage
            ? s.card
            : {
                flex: 1,
              }
        }>
        {isPost && <PostCard {...this.props} />}
        {isItemRequest && <ItemRequestCard {...this.props} />}
      </View>
    );
  }
}

export default Card;
