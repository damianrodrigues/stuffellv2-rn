// @flow
import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { BodyText, LinkText } from '../../../../text';
import { StyleHelpers } from '../../../../../lib/helpers';
import { styleConstants } from '../../../../../lib/config';
import avatar from '../../../../../../assets/images/avatar.jpg';

// TODO: normalize the size of the avatar based on the screen's width? similar to how we normalize fonts
// TODO: break out AvatarLink, but to where?
const AvatarLink = ({ item, onPress }) => {
  const { owner } = item;
  // if the user has an image, use that. If not, grab the defualt avatar
  const uri = (owner && owner.profile.image) || avatar;
  if (typeof uri === 'string') {
    Image.prefetch(uri);
  }
  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        style={{
          height: StyleHelpers.normalizeWidth(52),
          width: StyleHelpers.normalizeWidth(52),
          borderRadius: StyleHelpers.normalizeWidth(26),
        }}
        source={typeof uri === 'string' ? { uri } : uri}
      />
    </TouchableOpacity>
  );
};

type Props = {
  item: Object,
  navigation: Object,
  user: Object,
};

class ItemRequestTitle extends React.PureComponent<Props> {
  onGoToUserDetail = () => {
    const { item, navigation, user } = this.props;
    // we pass in the full user object so that we can show the data quicker,
    // rather than waiting to get a response from DB.
    // then we show a spinner for data like the list of his posts
    if (item.owner._id !== user._id) {
      navigation.navigate('userDetail', { _id: item.owner._id, user: item.owner });
    }
  };

  render() {
    const { item, navigation } = this.props;

    return (
      <View style={s.container}>
        <View style={s.row}>
          <View style={{ flex: 1 }}>
            <AvatarLink item={item} navigation={navigation} onPress={this.onGoToUserDetail} />
          </View>
          <View style={{ flex: 4, height: '100%', alignContent: 'space-between' }}>
            <LinkText onPress={this.onGoToUserDetail} numberOfLines={1}>
              {`${item.owner.profile.firstName} ${item.owner.profile.lastName}`}
            </LinkText>
            <BodyText numberOfLines={2}>Is looking for "{item.title || ''}"</BodyText>
          </View>
        </View>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingLeft: styleConstants.defaultMargin,
    paddingRight: styleConstants.defaultMargin,
    minHeight: 55,
    width: '100%',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default ItemRequestTitle;
