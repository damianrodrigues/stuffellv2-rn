import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import BuyButton from '../index';

const instance = shallow(
  <BuyButton
    store={configureStore()({})}
    item={{
      _id: '5WsDYY8HJv8LEEhx3',
      __typename: 'ItemRequest',
      title: 'hdhdjfjfkf',
      description: 'hxhxjfjckfjfnf',
      createdAt: '2018-11-06T12:27:30.804Z',
      rankScore: null,
      timeScore: null,
      activityScore: 9999,
      totalLikes: 0,
      buyerIds: [],
      conversationId: null,
      numberOfComments: 0,
      currenUserHasLiked: false,
      currentUserHasSeen: true,
      firstComment: [],
      lastComments: [],
      lastLikes: [],
      owner: {
        _id: 'f7KBg4hN9QMGaNJuP',
        __typename: 'User',
        following: 'accepted',
        follow: {
          _id: 'AsPrc2nbfjoJR69q4',
          accepted: true,
          rejected: false,
          parentId: 'TgcTAeWywrrtKBamQ',
          ownerId: 'f7KBg4hN9QMGaNJuP',
          __typename: 'Follow',
        },
        profile: {
          firstName: 'lux',
          lastName: 'space ',
          image: null,
          __typename: 'Profile',
        },
      },
    }}
    user={{
      _id: 'TgcTAeWywrrtKBamQ',
      emails: [
        {
          address: 'bugdonepavlov@gmail.com',
          verified: false,
          __typename: 'Email',
        },
      ],
      roles: null,
      numberOfFollowers: 3,
      numberFollowing: 3,
      numberOfPosts: 3,
      following: 'none',
      follow: null,
      profile: {
        firstName: 'Bogdan',
        lastName: 'pavlov',
        image: null,
        description: null,
        __typename: 'Profile',
      },
      __typename: 'User',
    }}
  />
);

describe('<BuyButton />', () => {
  it('<BuyButton /> should render', () => {
    expect(instance.exists()).toBeTruthy();
    expect(instance).toMatchSnapshot();
  });
});
