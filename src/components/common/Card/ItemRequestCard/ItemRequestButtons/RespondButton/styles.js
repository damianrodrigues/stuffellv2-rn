import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  buttonTextStyle: {
    display: 'flex',
    fontSize: 14,
    color: '#000',
    flex: 2,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonStyle: {
    width: 110,
    borderWidth: 2,
    height: 33,
    marginTop: 3,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  iconContainer: {
    flex: 1,
    textAlign: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
