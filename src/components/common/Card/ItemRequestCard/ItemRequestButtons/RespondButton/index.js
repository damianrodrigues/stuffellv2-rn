// @flow
import * as React from 'react';
import { View } from 'react-native';
// COMPONENTS
import RegularButton from '../../../../RegularButton';
// STYLES
import s from './styles';
// TYPES
type Props = {
  item: Object,
  navigation: Object,
  user: Object,
};
// EXPORTED COMPONENT
export default class RespondButton extends React.PureComponent<Props> {
  onRespond = () => {
    const { item, navigation } = this.props;
    const { conversationId } = item;

    const conversationExists = this.conversationExists();

    const name = `${item.owner.profile.firstName} ${item.owner.profile.lastName}`;

    // if a conversation already exists, then bring the user directly to the given conversation
    if (conversationExists) {
      return navigation.navigate('conversation', { _id: conversationId, name });
    }

    return navigation.navigate('modalScreen', {
      modalType: 'newMessage',
      _id: conversationId,
      name,
      item,
    });
  };

  conversationExists = () => {
    const { item, user } = this.props;

    return (
      item.buyerIds &&
      item.buyerIds.length > 0 &&
      user &&
      user._id &&
      item.buyerIds.includes(user._id)
    );
  };

  render() {
    const { item, user } = this.props;

    if (user._id === item.owner._id) {
      return null;
    }

    return (
      <View style={s.container}>
        <RegularButton
          onPress={this.onRespond}
          title="Respond"
          textStyle={s.buttonTextStyle}
          buttonStyle={s.buttonStyle}
        />
      </View>
    );
  }
}
