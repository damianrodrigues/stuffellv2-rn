// @flow
import { StyleSheet, Dimensions } from 'react-native';

import { styleConstants } from '../../../../../lib/config';
import { StyleHelpers } from '../../../../../lib/helpers';

export default StyleSheet.create({
  container: {
    maxHeight: StyleHelpers.normalizeWidth(43),
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
    alignItems: 'center',
    marginBottom: 10,
    paddingLeft: styleConstants.defaultMargin,
    paddingRight: styleConstants.defaultMargin,
    paddingHorizontal: styleConstants.defaultMargin,
  },
  buyButtonContainer: {
    flex: 1,
    top: 5,
    left: Dimensions.get('window').width / 2 - 65,
    alignSelf: 'center',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
