// @flow
import React from 'react';
import { Feather } from '@expo/vector-icons';
import { graphql, compose } from 'react-apollo';
import { Like } from '../../../../../../apollo/Like';
import { StyleHelpers } from '../../../../../../lib/helpers';

type Props = {
  item?: Object,
  toggleLike?: Function,
  user?: Object,
};

class LikeButton extends React.PureComponent<Props> {
  onLikeClick = () => {
    const { item, toggleLike, user } = this.props;

    if (!user || user._id === item.owner._id) {
      return;
    }

    const variables = {
      params: {
        parentId: item._id,
        parentModelType: 'ItemRequest',
      },
    };

    const optimisticResponse = {
      toggleLikeRequest: {
        _id: item._id,
        __typename: 'ItemRequest',
        currenUserHasLiked: !item.currenUserHasLiked,
        numberOfLikes: !item.currenUserHasLiked ? item.numberOfLikes - 1 : item.numberOfLikes + 1,
      },
    };

    toggleLike({ variables, optimisticResponse })
      .then(res => {})
      .catch(e => console.log(e));
  };

  render() {
    const { item } = this.props;
    return (
      <Feather
        name={item.currenUserHasLiked ? 'heart' : 'heart'}
        size={StyleHelpers.normalizeWidth(30)}
        color={item.currenUserHasLiked ? '#e74c3c' : '#262626'}
        style={{ margin: 5, marginLeft: 0 }}
        onPress={this.onLikeClick}
      />
    );
  }
}

// Specifies the default values for props:
LikeButton.defaultProps = {
  item: null,
  user: null,
  toggleLike: () => console.log('default toggleLike'),
};

const ComponentWithData = compose(
  graphql(Like.Mutation.toggleLikeItemRequest, { name: 'toggleLike' })
)(LikeButton);

export default ComponentWithData;
