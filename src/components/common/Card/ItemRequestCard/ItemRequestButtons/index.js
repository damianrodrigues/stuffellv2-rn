// @flow
import * as React from 'react';
import { View } from 'react-native';
// COMPONENTS
import ItemRequestEditButton from './ItemRequestEditButton';
import CommentButton from './CommentButton';
import LikeButton from './LikeButton';
import RespondButton from './RespondButton';
// STYLES
import s from './styles';
// TYPES
type Props = {
  item: Object,
  user: Object,
  toggleCommentForm: Function,
  navigation: Object,
  accountPage: boolean,
};

const ItemRequestButtons = (props: Props): React.Node => {
  const { item, user, navigation, accountPage, onPress, detailPage } = props;

  return (
    <View style={s.container}>
      {!accountPage && (
        <View style={{ flex: 1 }}>
          <LikeButton item={item} user={user} />
        </View>
      )}
      {!detailPage && (
        <View style={{ flex: 1 }}>
          <CommentButton onPress={onPress} />
        </View>
      )}
      <View style={{ flex: 1 }}>
        <ItemRequestEditButton {...props} />
      </View>
      <View style={{ flex: 1, elevation: 10 }} />
      <View style={s.buyButtonContainer}>
        <RespondButton item={item} user={user} navigation={navigation} />
      </View>
      <View style={{ flex: 1 }} />
      <View style={{ flex: 2 }} />
    </View>
  );
};

export default ItemRequestButtons;
