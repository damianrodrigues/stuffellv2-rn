import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import CommentButton from '../index.js';

const instance = shallow(
  <CommentButton
    store={configureStore()({})}
    screenProps={{
      data: {
        user: {
          _id: 'TgcTAeWywrrtKBamQ',
          emails: [
            {
              address: 'bugdonepavlov@gmail.com',
              verified: false,
              __typename: 'Email',
            },
          ],
          roles: null,
          numberOfFollowers: 3,
          numberFollowing: 3,
          numberOfPosts: 3,
          following: 'none',
          follow: null,
          profile: {
            firstName: 'Bogdan',
            lastName: 'pavlov',
            image: null,
            description: null,
            __typename: 'Profile',
          },
          __typename: 'User',
        },
      },
    }}
  />
);

describe('<CommentButton />', () => {
  it('<CommentButton /> should render', () => {
    expect(instance.exists()).toBeTruthy();
    expect(instance).toMatchSnapshot();
  });
});
