// @flow
import * as React from 'react';

import { Feather } from '@expo/vector-icons';
import { StyleHelpers } from '../../../../../../lib/helpers';

type Props = {
  onPress: Function,
};

const CommentButton = ({ onPress }: Props) => (
  <Feather
    name="message-circle"
    size={StyleHelpers.normalizeWidth(30)}
    color="#262626"
    style={{ margin: 5, marginLeft: 0 }}
    onPress={() => onPress('autofocus')}
  />
);

export default CommentButton;
