// @flow
import * as React from 'react';
import { View } from 'react-native';
import { Feather } from '@expo/vector-icons';
// LIB
import { StyleHelpers } from '../../../../../../lib/helpers';

type Props = {
  user: Object,
  item: Object,
  navigation: Object,
  setActiveItemRequest: Function,
};

class EditButton extends React.PureComponent<Props> {
  onToggleRequestEdit = () => {
    const { item, navigation, setActiveItemRequest } = this.props;
    const { title, description, _id } = item;
    const request = {
      title: { value: title },
      description: { value: description },
      editing: true,
      itemRequestId: _id,
    };
    navigation.navigate('modalScreen', { modalType: 'editItemRequest' });
    setActiveItemRequest(request);
  };

  render() {
    const { user, item } = this.props;
    if (!user) {
      return null;
    }
    if (user._id === item.owner._id) {
      return (
        <View>
          <Feather
            onPress={() => this.onToggleRequestEdit()}
            name="edit"
            size={StyleHelpers.normalizeWidth(28)}
            color="#262626"
            style={{ margin: 5, marginLeft: 0 }}
          />
        </View>
      );
    }
    return null;
  }
}

export default EditButton;
