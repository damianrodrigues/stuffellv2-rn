// @flow
import * as React from 'react';
import { Text } from 'react-native';
// COMPONENTS
import { BodyText, TitleText } from '../../../../text';
// TYPES
type Props = {
  like: Object,
  index: number,
  item: Object,
  navigation: Object,
};
// EXPORTED COMPONENT
const LikeItem = ({ like, index, item, navigation }: Props) => (
  <Text style={{ marginRight: 2 }}>
    <TitleText onPress={() => navigation.navigate('userDetail', { _id: like.ownerId })}>
      {like.userFullName}
    </TitleText>
    {item.lastLikes.length - 2 > index && <BodyText>, </BodyText>}
    {item.lastLikes.length - 2 === index && <BodyText> and </BodyText>}
  </Text>
);

export default LikeItem;
