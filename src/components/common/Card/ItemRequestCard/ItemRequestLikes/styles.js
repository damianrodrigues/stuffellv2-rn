// @flow
import { StyleSheet } from 'react-native';
// CONSTANTS
import { styleConstants } from '../../../../../lib/config';
// EXPORTED STYLES
export default StyleSheet.create({
  lastLikersContainer: {
    maxWidth: '100%',
    paddingVertical: styleConstants.defaultMargin,
  },
  container: {
    paddingHorizontal: styleConstants.defaultMargin,
  },
});
