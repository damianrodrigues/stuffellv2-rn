// TOP LEVE IMPORTS
import React from 'react';
import { View, Text } from 'react-native';
// COMPONENTS
import LikeItem from './LikeItem';
import { BodyText } from '../../../../text';
// STYLES
import s from './styles';

class LikesSection extends React.PureComponent {
  lastLikers = () => {
    const { item, navigation } = this.props;

    if (!item.lastLikes || !item.lastLikes.length || item.lastLikes.length === 0) {
      return null;
    }

    return (
      <Text style={s.lastLikersContainer}>
        <BodyText>Liked by {''}</BodyText>
        {item.lastLikes.map((like, index) => (
          <LikeItem key={like._id} like={like} index={index} item={item} navigation={navigation} />
        ))}
      </Text>
    );
  };

  render() {
    return <View style={s.container}>{this.lastLikers()}</View>;
  }
}

export default LikesSection;
