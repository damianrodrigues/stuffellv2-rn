import { StyleSheet, Dimensions } from 'react-native';
import { styleConstants } from '../../../../../lib/config';

const { color, fontSize, fontWeight } = styleConstants;
const { width } = Dimensions.get('window');

export default StyleSheet.create({
  content: {
    // flex: 1,
    backgroundColor: color.screenBackgroundGrey,
    padding: 25,
    width,
    // position: 'relative',
    // flexDirection: 'column',
    // display: 'flex',
    // flexDirection: 'column',
    // justifyContent: 'space-between',
  },
  text: {
    fontSize: fontSize.lg,
    lineHeight: 30,
    fontWeight: fontWeight.light,
    textAlign: 'center',
    flex: 1,
  },
  abLink: {
    position: 'relative',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    height: '100%',
    width: '100%',
    zIndex: 100,
  },
  link: {
    flex: 1,
    fontWeight: fontWeight.bold,
    color: color.primary,
    textAlign: 'center',
  },
});
