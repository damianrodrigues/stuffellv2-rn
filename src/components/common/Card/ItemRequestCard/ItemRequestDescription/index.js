// @flow
import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import s from './styles';

type Props = {
  onPress: Function,
  item: Object,
  detailPage: boolean,
};

const ItemRequestDescription = ({ onPress, item, detailPage }: Props) => {
  const { description } = item;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={s.content}>
        <View>
          <Text numberOfLines={detailPage ? null : 6} style={s.text}>
            {description.length < 299 ? `${description.slice(0, 299)}...` : description}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ItemRequestDescription;
