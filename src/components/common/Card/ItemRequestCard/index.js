// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../../redux/actions';
import ItemRequestTitle from './ItemRequestTitle';
import ItemRequestDescription from './ItemRequestDescription';
import ItemRequestButtons from './ItemRequestButtons';
import ItemRequestLikes from './ItemRequestLikes';
import ItemRequestComments from './ItemRequestComments';
import ItemRequestBottom from './ItemRequestBottom';

type State = {
  showCommentForm: boolean,
};

type Props = {
  accountPage?: boolean,
  detailPage?: boolean,
  navigation: Object,
  item: Object,
  user: Object,
};

class Card extends React.PureComponent<Props, State> {
  static defaultProps = {
    detailPage: false,
    accountPage: false,
  };

  onDescriptionPress = (autofocus = null) => {
    const { detailPage, navigation, item } = this.props;
    if (!detailPage) {
      navigation.navigate('cardDetail', {
        item,
        type: 'itemRequest',
        autofocus: autofocus === 'autofocus' ? true : null,
      });
    }
  };

  render() {
    return (
      <React.Fragment>
        <ItemRequestTitle {...this.props} />
        <ItemRequestDescription {...this.props} onPress={this.onDescriptionPress} />
        <ItemRequestButtons {...this.props} onPress={this.onDescriptionPress} />
        <ItemRequestLikes {...this.props} />
        <ItemRequestComments {...this.props} onPress={this.onDescriptionPress} />
        <ItemRequestBottom {...this.props} />
      </React.Fragment>
    );
  }
}

export default connect(null, actions)(Card);
