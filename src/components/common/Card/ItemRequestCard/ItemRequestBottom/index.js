// @flow
import * as React from 'react';
import { View, StyleSheet } from 'react-native';
// LIB
import { MomentHelpers } from '../../../../../lib/helpers';
import { styleConstants } from '../../../../../lib/config';
// COMPONENTS
import { CaptionText } from '../../../../text';

type Props = {
  item: Object,
};

const s = StyleSheet.create({
  container: {
    paddingLeft: styleConstants.defaultMargin,
    paddingRight: styleConstants.defaultMargin,
    marginTop: 4,
  },
});

const PostBottom = ({ item }: Props) => {
  const { createdAt } = item;

  return (
    <View style={s.container}>
      <CaptionText>{MomentHelpers.timeAgo(createdAt)}</CaptionText>
    </View>
  );
};

export default PostBottom;
