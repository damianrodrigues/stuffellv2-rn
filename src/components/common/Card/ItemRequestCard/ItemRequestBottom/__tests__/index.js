import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import PostBottom from '../index.js';

const instance = shallow(
  <PostBottom
    store={configureStore()({})}
    item={{ createdAt: '2018-11-06T14:17:47.379Z' }}
    screenProps={{
      data: {
        user: {
          _id: 'TgcTAeWywrrtKBamQ',
          emails: [
            {
              address: 'bugdonepavlov@gmail.com',
              verified: false,
              __typename: 'Email',
            },
          ],
          roles: null,
          numberOfFollowers: 3,
          numberFollowing: 3,
          numberOfPosts: 3,
          following: 'none',
          follow: null,
          profile: {
            firstName: 'Bogdan',
            lastName: 'pavlov',
            image: null,
            description: null,
            __typename: 'Profile',
          },
          __typename: 'User',
        },
      },
    }}
  />
);

describe('<PostBottom />', () => {
  it('<PostBottom /> should render', () => {
    expect(instance.exists()).toBeTruthy();
    expect(instance).toMatchSnapshot();
  });
});
