// TOP LEVE IMPORTS
import React from 'react';
import { View, Text } from 'react-native';
// COMPONENTS
import CommentItem from '../CommentItem';
import CommentsList from '../CommentsList';

class LastComments extends React.PureComponent {
  state = {
    showAllComments: false,
  };

  renderAllComments = () => {
    const { item } = this.props;
    // this is the "show all comments" component, show it if showAllComments is true, and hide the below components (i.e. renderSeeAllCommnets & renderLastComments)
    if (this.state.showAllComments) {
      return <CommentsList postId={item._id} {...this.props} />;
    }
  };

  renderSeeAllCommnets = () => {
    const { item } = this.props;

    if (this.state.showAllComments) {
      return null;
    } // dont show this component if you are showing all comments

    if (
      !item ||
      !item.numberOfComments ||
      !item.lastComments ||
      item.numberOfComments === 0 ||
      item.lastComments.length === 0
    ) {
      return null;
    }

    if (item.numberOfComments < 6) {
      return null;
    }
    if (item.lastComments.length < item.numberOfComments) {
      return (
        <View style={{ paddingTop: 10, paddingBottom: 10 }}>
          <Text
            style={{ color: '#999' }}
            onPress={() => this.setState({ showAllComments: !this.state.showAllComments })}>
            see all {item && item.numberOfComments} comments
          </Text>
        </View>
      );
    }
  };

  renderLastComments = () => {
    const { item, navigation } = this.props;

    if (this.state.showAllComments) {
      return null;
    } // dont show this component if you are showing all comments

    if (item.lastComments && item.lastComments.length > 0) {
      return (
        <View style={{ padding: 0 }}>
          {item.lastComments.map(comment => <CommentItem key={comment._id} comment={comment} navigation={navigation} />)}
        </View>
      );
    }
  };

  render() {
    return (
      <View>
        {this.renderAllComments()}
        {this.renderSeeAllCommnets()}
        {this.renderLastComments()}
      </View>
    );
  }
}

export default LastComments;
