// @flow
import * as React from 'react';
import { View, StyleSheet } from 'react-native';
// APOLLO
import { graphql } from 'react-apollo';
import { Message } from '../../../../../../apollo/Message';
// COMPONENTS
import CommentItem from '../CommentItem';
import { BodyText } from '../../../../../text/index';
// TYPES
type Props = {
  data: Object,
  user: Object,
  navigation: Object,
};
// STYLES
const s = StyleSheet.create({
  container: {
    paddingTop: 0,
    paddingBottom: 10,
  },
});

class CommentsList extends React.PureComponent<Props> {
  render() {
    const { data, user, navigation } = this.props;
    const { loading, messagesByPostId } = data;

    if (loading) {
      return (
        <View style={[s.container, { height: 60, paddingTop: 10 }]}>
          <BodyText>loading comments....</BodyText>
        </View>
      );
    }

    return (
      <View style={s.container}>
        {messagesByPostId.map(comment => (
          <CommentItem key={comment._id} user={user} comment={comment} navigation={navigation} />
        ))}
      </View>
    );
  }
}

const options = ({ postId }) => {
  const variables = {
    postId,
  };
  return { variables };
};

export default graphql(Message.Query.messagesByPostId, { options })(CommentsList);
