// @flow
import { graphql } from 'react-apollo';

import { Message } from '../../../../../../apollo/Message';

import CommentForm from './CommentForm';

const CommentFormWithData = graphql(Message.Mutation.createMessage, {
  name: 'createMessage',
})(CommentForm);

export default CommentFormWithData;
