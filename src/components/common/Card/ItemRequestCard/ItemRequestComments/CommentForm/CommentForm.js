// @flow
import * as React from 'react';
import { View, TextInput, Keyboard } from 'react-native';
import { Button } from 'react-native-elements';
// APOLLO
import { Message } from '../../../../../../apollo/Message';
import { ItemRequest } from '../../../../../../apollo/ItemRequest';
import { Conversation } from '../../../../../../apollo/Conversation';
// STYLES
import s from './styles';
// TYPES
type Props = {
  parentId: number,
  parentModelType: string,
  createMessage: Function,
  placeholder: string,
};
type State = {
  loading: boolean,
  messageValue: string,
};

class CommentForm extends React.PureComponent<Props, State> {
  state = {
    loading: false,
    messageValue: '',
  };

  onSuccessfulValidation = (messageValue: string) => {
    const { parentId, parentModelType, createMessage } = this.props;
    // create the variables to pass to graphql
    const variables = {
      params: {
        parentId,
        parentModelType,
        messageValue,
      },
    };

    const refetchQueries = [
      //
      { query: ItemRequest.Query.itemRequest, variables: { _id: parentId } },
      {
        query: Message.Query.messagesByPostId,
        variables: { postId: parentId },
      },
    ];

    if (parentModelType === 'itemRequestConversation') {
      refetchQueries.push({
        query: Conversation.Query.conversationById,
        variables: { _id: parentId },
      });
    }

    createMessage({ variables, refetchQueries })
      .then(() => {
        this.setState({ loading: false, messageValue: '' });
      })
      .catch(e => {
        console.log('createMessage catch');
        console.log(e);
        return this.setState({ loading: false });
      });
  };

  onChangeText = (messageValue: string) => {
    this.setState({ messageValue });
  };

  handleSubmit = () => {
    const { messageValue } = this.state;
    Keyboard.dismiss();
    // set the loading state to true
    this.setState({ loading: true });
    // do some validation on form values
    if (messageValue && messageValue.length > 2) {
      // call a function to submit to the server
      return this.onSuccessfulValidation(messageValue);
    }
    return this.setState({ loading: false });
  };

  render() {
    const { placeholder, autofocus } = this.props;
    const { messageValue, loading } = this.state;

    return (
      <View style={s.formContainer}>
        <View style={{ flex: 4 }}>
          <View style={{ margin: 0 }}>
            <TextInput
              clear
              onChangeText={this.onChangeText}
              onSubmitEditing={this.handleSubmit}
              autoFocus={autofocus}
              returnKeyType="done"
              value={messageValue}
              style={{ height: 34, fontSize: 17 }}
              placeholder={placeholder}
            />
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <Button
            onPress={this.handleSubmit}
            loading={loading}
            buttonStyle={s.buttonStyle}
            title={loading ? '' : 'Post'}
            containerViewStyle={s.containerViewStyle}
          />
        </View>
      </View>
    );
  }
}

export default CommentForm;
