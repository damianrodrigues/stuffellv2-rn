// @flow
import { StyleSheet } from 'react-native';
// LIB
import { styleConstants } from '../../../../../../lib/config';

export default StyleSheet.create({
  buttonStyle: {
    borderRadius: 1,
    backgroundColor: styleConstants.color.primary,
    height: 34,
    margin: 0,
    padding: 0,
    width: '100%',
  },
  containerViewStyle: {
    margin: 0,
    height: 34,
    marginLeft: 0,
    marginRight: 15,
    width: '100%',
  },
  formContainer: {
    borderTopWidth: 1,
    borderTopColor: styleConstants.color.screenBackgroundGrey,
    borderStyle: 'solid',
    marginTop: 40,
    padding: 3,
    marginBottom: 0,
    display: 'flex',
    flexDirection: 'row',
  },
});
