// @flow
import React from 'react';
import { View } from 'react-native';
import { UiHelpers } from '../../../lib/helpers';
import { stylesConfig } from '../../../lib/config';
import { BodyText } from '../../text/index';

// DOC
// =========================
/**
 * @namespace ServerError
 * @description used to show an error from the server, usually under a form, usually mapped over (i.e. an array of errors)
 * @prop error {array} - an array of errors that we can map over
 */

type Props = {
  errors: Array,
};

// EXPORTED COMPONENT
// =========================
const ServerErrors = ({ errors, containerStyle = {} }: Props) => {
  if (!UiHelpers.arrayExists(errors)) {
    return null;
  }

  return (
    <View style={[{ padding: 2 }, containerStyle]}>
      {errors &&
        errors.map((error, i) => {
          const cleanError = error.replace('GraphQL error', '');
          return (
            <BodyText key={error + i} style={stylesConfig.errorLabelStyle}>
              {cleanError}
            </BodyText>
          );
        })}
    </View>
  );
};

export default ServerErrors;
