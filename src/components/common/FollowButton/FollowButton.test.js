import React from 'react';
import { shallow } from 'enzyme';
import FollowButton from './index.js';
import { reduxStore } from '../../../lib/mocks';


let DEFAULT_PROPS = { 
    ...reduxStore,
    data: {
      loading: false
    }
};



describe('<FollowButton />', () => {
  const app = shallow(<FollowButton {...DEFAULT_PROPS} />);
 
  it('<FollowButton /> should render', () => {
    expect(app.exists()).toBe(true);
  });

});