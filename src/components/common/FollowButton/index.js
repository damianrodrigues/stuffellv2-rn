import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
// APOLLO
import { graphql, compose } from 'react-apollo';
import { width, totalSize } from 'react-native-dimension';
import { Conversation, ItemRequest, Follow, Friends } from '../../../apollo';

// MODULES
import { styleConstants } from '../../../lib/config';
import RegularButton from '../RegularButton';

export class FollowButton extends React.PureComponent {
  state = {
    loading: false,
    following: this.props.targetUser.following,
  };

  componentWillUnmount() {
    this.setState({ loading: false, following: '' });
  }

  handleFollow = () => {
    const { targetUser, createFollow } = this.props;
    const parentId = targetUser._id;
    const params = { parentId };
    const variables = { params };
    const optimisticResponse = {
      __typename: 'Mutation',
      createFollow: {
        _id: targetUser._id,
        params: {
          parentId,
        },
        __typename: 'User',
      },
    };

    this.setState({ loading: true }, () => {
      createFollow({
        variables,
        optimisticResponse,
        update: (store, { data: { createFollow } }) => {
          console.log('data create', store);
          try {
            console.log('createFollow', createFollow);
            const usersSearchData = store.readQuery({
              query: Friends.Query.usersSearch,
              variables: {
                params: {
                  searchText: '',
                },
              },
            });
            console.log('usersSearchData', usersSearchData);

            const friendsOfFriendsData = store.readQuery({
              query: Friends.Query.getCurrentUser,
              variables: {
                params: {
                  searchText: '',
                },
              },
            });

            usersSearchData.usersSearch = usersSearchData.usersSearch.filter(
              user => user._id !== createFollow._id
            );

            friendsOfFriendsData.friendsOfFriends = friendsOfFriendsData.friendsOfFriends.filter(
              user => user._id !== createFollow._id
            );

            store.writeQuery({ query: Friends.Query.usersSearch, usersSearchData });
            store.writeQuery({ query: Friends.Query.getCurrentUser, friendsOfFriendsData });
          } catch (e) {
            console.log('erorr', e);
          }
        },
      })
        .then(res => {
          this.setState({ loading: false, following: 'pending' });
        })
        .catch(e => this.setState({ loading: false }));
    });
  };

  handleUnFollow = () => {
    const { targetUser, unfollow } = this.props;
    const variables = { _id: targetUser._id, parentModelType: 'Friends' };
    const optimisticResponse = {
      __typename: 'Mutation',
      unfollow: {
        _id: targetUser._id,
        __typename: 'User',
      },
    };

    this.setState({ loading: true }, () => {
      unfollow({
        variables,
        optimisticResponse,
        update: (store, { data: { unfollow } }) => {
          console.log('data unflow', store);
          try {
            const data = store.readQuery({
              query: Friends.Query.getFriendsAndOutgoing,
              variables: { params: {} },
            });
            data.getFriendsAndOutgoing = data.getFriendsAndOutgoing.filter(
              user => user._id !== unfollow._id
            );

            store.writeQuery({ query: Friends.Query.getFriendsAndOutgoing, data });
          } catch (e) {
            console.log('erorr', e);
          }
        },
      })
        .then(res => {
          this.setState({ loading: false }, () => {});
        })
        .catch(e => {
          console.log('handleUnFollow error');
          console.log(e);
          this.setState({ loading: false });
        });
    });
  };

  handleAccept = () => {
    const { targetUser, follow, acceptFollow } = this.props;
    const params = { parentId: targetUser._id, parentModelType: 'Follow' };
    const optimisticResponse = {
      __typename: 'Mutation',
      acceptFollow: {
        _id: targetUser._id,
        params: {
          parentId: targetUser._id,
        },
        __typename: 'User',
      },
    };

    return this.setState({ loading: true }, () => {
      if (!targetUser || !follow) {
        return this.setState({ loading: false });
      }

      acceptFollow({
        variables: { _id: follow._id, params },
        optimisticResponse,
        update: (store, { data: { acceptFollow } }) => {
          console.log('data unflow', store);
          try {
            const data = store.readQuery({
              query: Follow.Query.incomingFollows,
              variables: { params: null },
            });

            data.incomingFollows = data.incomingFollows.filter(
              user => user._id !== acceptFollow._id
            );

            store.writeQuery({ query: Follow.Query.incomingFollows, data });
          } catch (e) {
            console.log('erorr', e);
          }
        },
      })
        .then(res => {
          this.setState({ loading: false, following: 'accepted' });
        })
        .catch(e => this.setState({ loading: false }));
    });
  };

  renderPendingButton = () => {
    const pendingStyles = {
      button: {
        backgroundColor: styleConstants.color.buttonBorder,
        borderWidth: 0,
      },
      text: {
        color: styleConstants.color.buttonPendingColor,
        fontSize: totalSize(2),
      },
    };

    return (
      <RegularButton
        onPress={() => {}}
        title="Pending"
        buttonStyle={[styles.buttonStyle, pendingStyles.button]}
        textStyle={[styles.buttonText, pendingStyles.text]}
        disabled
      />
    );
  };

  renderFollowButton = () => {
    const { loading, following } = this.state;

    const followStyles = {
      borderWidth: 2,
    };
    return (
      <RegularButton
        onPress={this.handleFollow}
        title={!loading ? 'Add Friend' : '...'}
        buttonStyle={[styles.buttonStyle, followStyles]}
        textStyle={[styles.buttonText]}
        loading={loading}
        disabled={loading || following !== 'none'}
      />
    );
  };

  renderUnfollowButton = () => {
    const { loading, following } = this.state;

    const unfollowStyles = {
      button: {
        borderColor: styleConstants.color.black,
      },
      text: {
        color: styleConstants.color.black,
        fontSize: totalSize(2),
      },
    };

    return (
      <RegularButton
        onPress={this.handleUnFollow}
        title={!loading ? 'Unfriend' : '...'}
        buttonStyle={[styles.buttonStyle, unfollowStyles.button]}
        textStyle={[styles.buttonText, unfollowStyles.text]}
        loading={loading}
        disabled={loading || following !== 'accepted'}
      />
    );
  };

  renderAcceptButton = () => {
    const { loading, following } = this.state;

    return (
      <RegularButton
        onPress={this.handleAccept}
        title={!loading ? 'Accept' : '...'}
        buttonStyle={styles.buttonStyle}
        textStyle={styles.buttonText}
        loading={loading}
        disabled={loading || following !== 'incoming'}
      />
    );
  };

  render() {
    // if no targetUser was provided, then return null
    if (!this.props.targetUser) {
      return null;
    }

    switch (this.state.following) {
      case 'incoming':
        return this.renderAcceptButton();
      case 'none':
        return this.renderFollowButton();
      case 'pending':
        return this.renderPendingButton();
      case 'accepted':
        return this.renderUnfollowButton();
      case 'rejected':
        return null;
      default:
        return null;
    }
  }
}

let styles = StyleSheet.create({
  buttonStyle: {
    padding: 0,
    width: width(25),
    height: 33,
    borderWidth: 2,
    backgroundColor: styleConstants.color.screenBackground,
    borderColor: styleConstants.color.primary,
  },
  buttonText: {
    color: styleConstants.color.primary,
    fontSize: totalSize(2),
    fontFamily: styleConstants.fontFamily.bold,
    fontWeight: styleConstants.fontWeight.bold,
  },
});

const refetchQueries = [
  { query: Friends.Query.getCurrentUser },
  { query: Friends.Query.usersSearch },
  { query: Friends.Query.getFriendsAndOutgoing },
  { query: Follow.Query.incomingFollows },
  { query: Conversation.Query.hasUnreadConversations },
  { query: ItemRequest.Query.itemRequests },
  { query: ItemRequest.Query.itemRequestsCount },
];

export default compose(
  graphql(Follow.Mutation.createFollow, {
    name: 'createFollow',
    options: {
      refetchQueries,
    },
  }),
  graphql(Follow.Mutation.acceptFollow, {
    name: 'acceptFollow',
    options: {
      refetchQueries,
    },
  }),
  graphql(Follow.Mutation.unfollow, {
    name: 'unfollow',
    options: {
      refetchQueries,
    },
  })
)(FollowButton);
