import React from 'react';
import { View } from 'react-native';
import { Feather } from '@expo/vector-icons';
// APOLLO

// COMPONENTS

import { TitleText, CaptionText } from '../../text';


const EmptyState = ({ header, subheader }) => {
  return (
    <View style={{height: 350, alignItems: 'center', justifyContent: 'center'}}>
      <View>
        <Feather style={{marginBottom: 15, textAlign: 'center', alignSelf: 'center'}}color="#ddd" name="help-circle" size={55} />
        <TitleText style={{textAlign: 'center'}}>{header}</TitleText>
        <CaptionText style={{textAlign: 'center'}}>{subheader}</CaptionText>
      </View>
    </View>
  )
}

export default EmptyState;
