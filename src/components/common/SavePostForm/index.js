// @flow
import React from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native';
import { createForm, createFormField } from 'rc-form';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';
import * as actions from '../../../redux/actions';
import { Post } from '../../../apollo';
// COMPONENTS
import PostFormSteps from './PostFormSteps';
import StepOne from './StepOne';
import StepTwo from './StepTwo';
import StepThree from './StepThree';
import FormButtons from './FormButtons';

type Props = {
  postForm: Object,
  screenProps: Object,
  savePost: Function,
  onPostFormStepChange: Function,
  form: Object,
  updateForm?: boolean,
};

class PostForm extends React.PureComponent<Props> {
  static defaultProps = {
    updateForm: false,
  };

  state = {
    loading: false,
  };

  onSubmitForm = () => {
    const { postForm, screenProps, savePost } = this.props;
    const postId = postForm._id;
    const userId = screenProps.data.user._id;

    // setup refetch variables
    const params = this.getParams();
    const variables = {
      _id: postId,
      params,
    };

    // setup refetch queries
    const refetchQueriesVariables = {
      params: { userId },
    };

    const refetchQueries = [
      {
        query: Post.Query.myPosts,
        variables: refetchQueriesVariables,
      },
      // { query: Post.Query.myPostsCount, variables: refetchQueriesVariables },
    ];

    const optimisticResponse = {
      __typename: 'Mutation',
      savePost: {
        __typename: 'Post',
        _id: postId,
        ...variables,
      },
    };

    // call apollo muation
    return savePost({
      variables,
      refetchQueries,
      optimisticResponse,
    })
      .then(res =>
        this.setState({ loading: false }, () => {
          const { clearPostForm, navigation } = this.props;

          clearPostForm();
          return navigation.dispatch(NavigationActions.back());
        })
      )
      .catch(e => {
        console.log(e);
        return this.setState({ loading: false });
      });
  };

  getParams = () => {
    const { postForm } = this.props;

    const params = {
      description: postForm.description.value,
      title: postForm.title.value,
      category: postForm.category.value,
      statuses: postForm.statuses.value,
      image: postForm.image.value,
      price: postForm.cost.value, // price or cost?
    };

    return params;
  };

  renderStep = () => {
    const { props } = this;
    switch (props.postForm.currentPosition) {
      case 1:
        return <StepOne {...props} />;
      case 2:
        return <StepTwo {...props} />;
      case 3:
        return <StepThree {...props} />;
      default:
        return null;
    }
  };

  render() {
    const { onPostFormStepChange, postForm, form, updateForm } = this.props;

    return (
      <KeyboardAvoidingView
        behavior="padding"
        // style={{ backgroundColor: '#fff', flexGrow: 1 }}
        style={s.container}>
        <View style={s.photoContainer}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            {this.renderStep()}
          </TouchableWithoutFeedback>
        </View>
        <View
          style={{
            padding: 2,
            backgroundColor: '#fff',
            borderTopWidth: 2,
            width: '100%',
            borderColor: '#efefef',
          }}>
          <FormButtons
            onPostFormStepChange={onPostFormStepChange}
            postForm={postForm}
            form={form}
            onSubmitForm={this.onSubmitForm}
            updateForm={updateForm}
          />
          <PostFormSteps />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

// STYLES
// ====================================
const s = StyleSheet.create({
  container: {
    backgroundColor: '#efefef',
    width: '100%',
    // backgroundColor: 'red',
    flex: 1,
  },
  photoContainer: {
    flex: 3.5,
    // backgroundColor: 'blue'
  },
});

const mapStateToProps = ({ postForm }) => ({
  postForm,
});

// APOLLO
// ====================================
// connect PostForm to apollo mutations for creating/saving a post
const ComponentWithData = compose(
  graphql(Post.Mutation.createPost, { name: 'createPost' }),
  graphql(Post.Mutation.savePost, { name: 'savePost' })
)(PostForm);

// RC-FORM OPTIONS
// ================================
// set form options for rc-form
const formOptions = {
  // 1. onFieldsChange runs like onChangeText
  // we can use it to fire a redux action,
  // to store the contents in redux
  onFieldsChange(props, changedFields) {
    props.onPostFormFieldChange(changedFields);
  },

  // 2. we then get the redux store props to feed back into the component
  // using mapPropsToFields, and we use those props to map back into
  // rc-form's form inputs.
  // basically this is the glue between the redux store and rc-form's state
  mapPropsToFields({ postForm }) {
    return {
      title: createFormField({
        // ...postForm.title,
        value: postForm.title.value,
      }),
      image: createFormField({
        // ...postForm.image,
        value: postForm.image.value,
      }),
    };
  },
};

// EXPORT
// ====================================
export default connect(mapStateToProps, actions)(createForm(formOptions)(ComponentWithData));
