// @flow
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { styleConstants } from '../../../lib/config';

// STYLES
// ==============================
const s = StyleSheet.create({
  text: {
    fontFamily: styleConstants.fontFamily.regular,
    textAlign: 'left',
    fontWeight: styleConstants.fontWeight.regular,
    fontSize: styleConstants.fontSize.md,
  },
});

type Props = {
  onPress?: Function,
  numberOfLines: number,
};

// DOCS
/**
 * @namespace RegularText
 * @description Reusable component to make sure we use the same styles everywhere for regular text.
 * @prop keyName {String} -
 * the name of the data you passed in.
 * @prop children {object} -
 * this is the text you pass in to <RegularText>Hi Friend</RegularText>
 * @prop numberOfLines {number}
 * @prop onPress {function}
 */

// EXPORTED COMPONENT
// ==============================
const RegularText = ({ children, numberOfLines = 1, onPress, style = {} }: Props) => (
  <Text numberOfLines={numberOfLines} onPress={() => onPress()} style={[s.text, style]}>
    {children}
  </Text>
);

RegularText.defaultProps = {
  onPress: () => {},
};

export { RegularText };
