// TOP LEVEL IMPORTS
import React from 'react';
import { StyleSheet, Text } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';

// EXPORTED COMPONENT
// ==============================
const BodyText = ({ children, style, numberOfLines }) => {
  return (
    <Text style={[s.defaultText, style]} numberOfLines={numberOfLines}>
      {children}
    </Text>
  );
};

// STYLES
// ==============================
const s = StyleSheet.create({
  defaultText: {
    fontFamily: styleConstants.fontFamily.regular,
    fontWeight: styleConstants.fontWeight.regular,
    color: styleConstants.color.black2,
    fontSize: styleConstants.fontSize.lg,
  },
});

// PROPS
// ==============================
BodyText.defaultProps = {
  style: {},
  numberOfLines: null,
};

// EXPORT
// ==============================
export { BodyText };
