// TOP LEVEL IMPORTS
import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';

// EXPORTED COMPONENT
// ==============================
const TitleText = ({ children, style, onPress, numberOfLines }) => {

  return (
    <Text
      onPress={onPress}
      style={[s.defaultText, style]}
      numberOfLines={numberOfLines}
    >
      { children }
    </Text>
  );

};

// STYLES
// ==============================
const s = StyleSheet.create({
  defaultText: {
    fontFamily: styleConstants.fontFamily.bold,
    fontWeight: styleConstants.fontWeight.bold,
    color: styleConstants.color.black2,
    fontSize: styleConstants.fontSize.lg,
  },
});

// PROPS
// ==============================
TitleText.defaultProps = {
  style: {},
  onPress: () => {},
  numberOfLines: null
};

// EXPORT
// ==============================
export { TitleText };
