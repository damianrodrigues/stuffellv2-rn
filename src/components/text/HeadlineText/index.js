// TOP LEVEL IMPORTS
import React from 'react';
import { StyleSheet, Text } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';

// EXPORTED COMPONENT
// ==============================
const HeadlineText = ({ children, style }) => {
  return <Text style={[s.defaultText, style]}>{children}</Text>;
};

// STYLES
// ==============================
const s = StyleSheet.create({
  defaultText: {
    fontFamily: styleConstants.fontFamily.bold,
    fontWeight: styleConstants.fontWeight.bold,
    fontSize: styleConstants.fontSize.xl,
  },
});

// PROPS
// ==============================
HeadlineText.defaultProps = {
  style: {},
};

// EXPORT
// ==============================
export { HeadlineText };
