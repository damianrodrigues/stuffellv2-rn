// TOP LEVEL IMPORTS
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';

// EXPORTED COMPONENT
// ==============================
const LinkText = ({ children, onPress, containerStyle, style }) => {
  return (
    <View style={containerStyle}>
      <TouchableOpacity onPress={onPress}>
        <Text style={[s.defaultLinkText, style]}>{children}</Text>
      </TouchableOpacity>
    </View>
  );
};

// STYLES
// ==============================
const s = StyleSheet.create({
  defaultLinkText: {
    fontFamily: styleConstants.fontFamily.bold,
    fontWeight: styleConstants.fontWeight.bold,
    color: styleConstants.color.black1,
    fontSize: styleConstants.fontSize.lg,
  },
});

// PROPS
// ==============================
LinkText.defaultProps = {
  style: {},
  containerStyle: {},
};

// EXPORT
// ==============================
export { LinkText };
