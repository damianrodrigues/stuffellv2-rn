// TOP LEVEL IMPORTS
import React from 'react';
import { StyleSheet, Text } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';

// used for things like the "time ago" of a post or a chat message

// EXPORTED COMPONENT
// ==============================
const CaptionText = ({ children, style, black, onPress }) => {
  return (
    <Text style={[s.defaultText, style, black && s.blackText]} onPress={onPress}>
      {children}
    </Text>
  );
};

// STYLES
// ==============================
const s = StyleSheet.create({
  defaultText: {
    fontFamily: styleConstants.fontFamily.regular,
    fontWeight: styleConstants.fontWeight.regular,
    color: styleConstants.color.black4,
    fontSize: styleConstants.fontSize.sm,
  },
  blackText: {
    color: styleConstants.color.black,
  },
});

// PROPS
// ==============================
CaptionText.defaultProps = {
  style: {},
};

// EXPORT
// ==============================
export { CaptionText };
