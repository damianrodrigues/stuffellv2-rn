export * from './HeadlineText';
export * from './BodyText';
//export * from './HeadlineText';
export * from './LinkText';
export * from './TitleText';
export * from './CaptionText';
export * from './RegularText';