// @flow
import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../redux/actions';
import { CATEGORY_OPTIONS } from '../../lib/config';
import { BodyText } from '../text/index';

const MAPPED_CATEGORY_OPTIONS = CATEGORY_OPTIONS.map(item => ({ value: item, label: item }));

type Props = {
  navigation: Object, // via react-navigation
  onPostFormFieldChange: Function, // a redux action that updates the post form reducer
};

class CategoriesOptions extends React.PureComponent<Props> {
  onPress = value => {
    const { onPostFormFieldChange, navigation } = this.props;
    // update the redux store with the new category
    onPostFormFieldChange({
      category: { value },
    });
    // go back to the PostForm (step 2)
    navigation.goBack(null);
  };

  render() {
    return (
      <View>
        {MAPPED_CATEGORY_OPTIONS.map(item => (
          <View key={item.value} style={s.itemContainer}>
            <TouchableOpacity onPress={() => this.onPress(item.value)}>
              <BodyText>{item.value}</BodyText>
            </TouchableOpacity>
          </View>
        ))}
      </View>
    );
  }
}

// STYLES
// ========================================
const s = StyleSheet.create({
  itemContainer: {
    padding: 10,
    borderTopWidth: 1,
    backgroundColor: '#fff',
    borderColor: '#efefef',
  },
});

export default connect(null, actions)(CategoriesOptions);
