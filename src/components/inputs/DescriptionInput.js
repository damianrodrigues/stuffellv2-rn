// @flow
import React from 'react';
import { TouchableWithoutFeedback, TextInput, Keyboard } from 'react-native';

type Props = {
  placeholder?: string,
};

class DescriptionInput extends React.PureComponent<Props> {
  static defaultProps = {
    label: 'Description',
    placeholder: 'Type your message here...',
    message: 'Please choose a status!',
    required: true,
    rows: 4,
    blurOnSubmit: true,
    style: {},
    returnKeyType: 'default',
  };

  constructor(props) {
    super(props);

    const value = this.props.value || {};
    this.state = {
      description: this.props.value || '',
    };
  }

  componentWillReceiveProps(nextProps) {
    // Should be a controlled component.
    if ('value' in nextProps) {
      const description = nextProps.value;
      this.setState({ description });
    }
  }

  handleChange = description => {
    if (!('value' in this.props)) {
      this.setState({ description });
    }
    this.triggerChange(description);
  };

  triggerChange = changedValue => {
    const { onChange } = this.props;
    if (onChange) {
      onChange(changedValue);
    }
  };

  render() {
    const { placeholder, returnKeyType, rows, blurOnSubmit, style } = this.props;
    const { description } = this.state;
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <TextInput
          placeholder={placeholder}
          returnKeyType={returnKeyType}
          blurOnSubmit={blurOnSubmit}
          rows={rows}
          style={[{ borderWidth: 0, borderColor: '#fff' }, style]}
          value={description}
          onChangeText={this.handleChange}
        />
      </TouchableWithoutFeedback>
    );
  }
}

export { DescriptionInput };
