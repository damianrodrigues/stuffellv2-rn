// @flow
import * as React from 'react';
import { TextInput } from 'react-native';

// DOCS
// =========================
/**
 *
 * @namespace Input
 * @description reusable input
 * @prop onChange {function}
 * @prop value {string}
 * @prop placeholder {string}
 * @prop label {string}
 * @prop inputStyle {object}
 *
 */

type Props = {
  value: string,
  keyboardType: string,
  returnKeyType: string,
  input: Object,
  onChange: Function,
  onSubmitEditing: boolean,
  placeholder: string,
  style: Object,
  multiline: boolean,
};

// EXPORTED COMPONENT
// ===================================
class Input extends React.PureComponent<Props> {
  render(): React.Node {
    const {
      value,
      keyboardType,
      returnKeyType,
      onSubmitEditing,
      onChange,
      placeholder,
      style,
      multiline,
    } = this.props;
    return (
      <TextInput
        keyboardType={keyboardType}
        autoCapitalize="none"
        defaultValue={value}
        placeholder={placeholder}
        ref={input => (this.input = input)}
        returnKeyType={returnKeyType}
        onSubmitEditing={onSubmitEditing}
        style={style}
        value={value}
        onChangeText={onChange}
        multiline={multiline}
        {...this.props}
      />
    );
  }
}

export { Input };
