// @flow
import React from 'react';
import { FormValidationMessage } from 'react-native-elements';
import { stylesConfig } from '../../lib/config';

// DOCS
// =========================

/**
 * FormInputError, is the complete signup form component (made up of several components)
 * that is used inside of AuthScreen
 *
 * @namespace FormInputError
 * @prop fieldName {string} - the name of the input this component is related to (see rc-form documentation)
 * @prop getFieldError {function} - a function to grab the latest errors for a given form field
 */

type Props = {
  fieldName: string,
  getFieldError: Function,
};

// EXPORTED COMPONENT
// =========================
class FormInputError extends React.PureComponent<Props> {
  render() {
    const { fieldName, getFieldError } = this.props;
    if (!fieldName || !getFieldError) {
      return null;
    } // if props are not passed, return null
    let errors;
    if ((errors = getFieldError(fieldName))) {
      return (
        <FormValidationMessage labelStyle={stylesConfig.errorLabelStyle}>
          {errors.join(',')}
        </FormValidationMessage>
      );
    }

    return null;
  }
}

export { FormInputError };
