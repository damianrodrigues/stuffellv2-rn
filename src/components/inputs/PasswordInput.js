// @flow
import React from 'react';
import { View, StyleSheet, Text, TextInput } from 'react-native';
import { styleConstants } from '../../lib/config';
import { StyleHelpers } from '../../lib/helpers/StyleHelpers';

// DOCS
// =========================
/**
 *
 * @namespace PasswordInput
 * @description reusable password input
 * @prop label {string}
 * @prop value {string}
 * @prop placeholder {string}
 * @prop onChange {string}
 * @prop containerStyle {object}
 * @prop inputStyle {object}
 *
 */

class PasswordInput extends React.PureComponent {
  static defaultProps = {
    containerStyle: {
      marginLeft: 0,
      marginRight: 0,
    },
    returnKeyType: null,
    onSubmitEditing: null,
    placeholder: 'Password',
    inputStyle: {
      height: 38,
    },
  };

  state = {
    secure: true,
  };

  onToggleSecure = () => {
    this.setState({ secure: !this.state.secure });
  };

  render() {
    const {
      value,
      placeholder,
      onChangeText,
      returnKeyType,
      onSubmitEditing,
      inputStyle,
    } = this.props;
    const { secure } = this.state;
    return (
      <View style={{ position: 'relative' }}>
        <TextInput
          secureTextEntry={secure}
          placeholder={placeholder}
          returnKeyType={returnKeyType}
          onSubmitEditing={onSubmitEditing}
          style={[inputStyle, s.defaultInput]}
          value={value}
          ref={input => (this.input = input)}
          onChangeText={onChangeText}
        />
        <Text onPress={this.onToggleSecure} style={s.hideShowText}>
          {secure ? 'Show' : 'Hide'}
        </Text>
      </View>
    );
  }
}

// STYLES
// =========================
const s = StyleSheet.create({
  defaultInput: {
    borderBottomWidth: 2,
    borderBottomColor: '#efefef',
    height: StyleHelpers.normalizeHeight(50),
    fontSize: styleConstants.fontSize.lg,
  },
  hideShowText: {
    position: 'absolute',
    right: 0,
    bottom: 12,
  },
});

export { PasswordInput };
