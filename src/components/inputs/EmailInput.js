// @flow
import React from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { styleConstants } from '../../lib/config/styleConstants';
import { StyleHelpers } from '../../lib/helpers/StyleHelpers';

// DOCS
// =========================
/**
 *
 * @namespace EmailInput
 * @description reusable email input
 * @prop onChange {function}
 * @prop value {string}
 * @prop placeholder {string}
 * @prop label {string}
 * @prop containerStyle {object}
 * @prop inputStyle {object}
 *
 */

type Props = {};

// EXPORTED COMPONENT
// ===================================
class EmailInput extends React.PureComponent {
  static defaultProps = {
    placeholder: 'Example@email.com',
    containerStyle: {
      marginLeft: 0,
      marginRight: 0,
    },
    returnKeyType: null,
    onSubmitEditing: () => {},
    inputStyle: {
      // color: colorConfig.primary,
      height: StyleHelpers.normalizeHeight(38),
      fontSize: styleConstants.fontSize.md,
      // fontFamily: styleConstants.fontFamily.regular,
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
    };
  }

  render() {
    const {
      onBlur,
      placeholder,
      returnKeyType,
      onSubmitEditing,
      onChangeText,
      inputStyle,
    } = this.props;

    const { value } = this.state;

    return (
      <TextInput
        keyboardType="email-address"
        autoCapitalize="none"
        onBlur={onBlur}
        placeholder={placeholder}
        ref={input => (this.input = input)}
        returnKeyType={returnKeyType}
        onSubmitEditing={onSubmitEditing}
        style={[inputStyle, s.defaultInput]}
        value={value}
        onChangeText={onChangeText}
      />
    );
  }
}

const s = StyleSheet.create({
  defaultInput: {
    borderBottomWidth: 2,
    borderBottomColor: '#efefef',
    height: StyleHelpers.normalizeHeight(50),
    fontSize: styleConstants.fontSize.lg,
  },
});

export { EmailInput };
