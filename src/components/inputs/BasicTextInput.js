// @flow
import * as React from 'react';
import { View, StyleSheet, TextInput } from 'react-native';
import { styleConstants } from '../../lib/config';
import { StyleHelpers } from '../../lib/helpers/StyleHelpers';

// DOCS
// =========================
/**
 *
 * @namespace TextInput
 * @description reusable email input
 * @prop onChange {function}
 * @prop value {string}
 * @prop placeholder {string}
 * @prop label {string}
 * @prop containerStyle {object}
 * @prop inputStyle {object}
 *
 */

type Props = {
  value: string,
  keyboardType: string,
  returnKeyType?: string,
  onChange: Function,
  onSubmitEditing?: boolean,
  inputStyle?: Object,
  placeholder?: string,
};

// EXPORTED COMPONENT
// ===================================
class BasicTextInput extends React.PureComponent<Props> {
  static defaultProps = {
    containerStyle: {
      marginLeft: 0,
      marginRight: 0,
    },
    returnKeyType: null,
    onSubmitEditing: null,
    placeholder: 'Text',
    inputStyle: {
      height: 38,
    },
  };

  render(): React.Node {
    const {
      value,
      keyboardType,
      returnKeyType,
      onSubmitEditing,
      placeholder,
      onChange,
      inputStyle,
    } = this.props;
    return (
      <View style={{ position: 'relative' }}>
        <TextInput
          keyboardType={keyboardType}
          autoCapitalize="none"
          defaultValue={value}
          placeholder={placeholder}
          ref={input => (this.input = input)}
          returnKeyType={returnKeyType}
          onSubmitEditing={onSubmitEditing}
          style={[inputStyle, s.defaultInput]}
          value={value}
          onChangeText={onChange}
        />
      </View>
    );
  }
}

const s = StyleSheet.create({
  defaultInput: {
    borderBottomWidth: 2,
    borderBottomColor: '#efefef',
    height: StyleHelpers.normalizeHeight(50),
    fontSize: styleConstants.fontSize.lg,
  },
});

export { BasicTextInput };
