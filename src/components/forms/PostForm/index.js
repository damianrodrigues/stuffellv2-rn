// @flow
import React from 'react';
import { View, StyleSheet, TouchableOpacity, Keyboard, KeyboardAvoidingView } from 'react-native';
import { createForm, createFormField } from 'rc-form';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';
import * as actions from '../../../redux/actions';
import { Post } from '../../../apollo';
import PostFormSteps from './PostFormSteps';
import StepOne from './StepOne';
import StepTwo from './StepTwo';
import StepThree from './StepThree';
import FormButtons from './FormButtons';

type Props = {
  updateForm?: Booleon, // a boolean telling us if we're updating a post or creating a new one. Decides which mutation to call, and can be used for deciing which text to show on buttons (e.g. "Create Post" vs "Save Post"), etc
};

class PostForm extends React.PureComponent<Props> {
  static defaultProps = {
    updateForm: false,
  };

  state = {
    loading: false,
  };

  onSave = () => {
    const { postForm, screenProps, savePost, clearPostForm, navigation } = this.props;
    const postId = postForm._id;
    const userId = screenProps.data.user._id;

    // setup refetch variables
    const params = this.getParams();
    const variables = {
      _id: postId,
      params,
    };

    // setup refetch queries
    const refetchQueriesVariables = {
      params: { userId },
    };

    const refetchQueries = [
      {
        query: Post.Query.myPosts,
        variables: refetchQueriesVariables,
      },
      // { query: Post.Query.myPostsCount, variables: refetchQueriesVariables },
    ];

    const optimisticResponse = {
      __typename: 'Mutation',
      savePost: {
        __typename: 'Post',
        _id: postId,
        ...variables,
      },
    };

    // call apollo muation
    return savePost({
      variables,
      refetchQueries,
      optimisticResponse,
    })
      .then(res =>
        this.setState({ loading: false }, () => {
          clearPostForm();
          return navigation.dispatch(NavigationActions.back());
        })
      )
      .catch(e => {
        console.log(e);
        return this.setState({ loading: false });
      });
  };

  onSubmitForm = () => {
    // DO VALIDATION

    // check if we're updating, if so, call update mutation
    if (this.props.updateForm) {
      return this.onSave();
    }

    // if we're creating new record, call creation mutation
    return this.onCreate();
  };

  onCreate = () => {
    const { createPost, clearPostForm, navigation } = this.props;
    const params = this.getParams();
    const variables = { params };

    this.setState({ loading: true });

    // call apollo muation
    createPost({ variables })
      .then(() => {
        this.setState({ loading: false });
        // call redux action to clear the post form
        clearPostForm();
        // TODO: navigate to the account page
        navigation.dispatch(NavigationActions.back());
        setTimeout(() => {
          navigation.navigate('account');
        }, 10);

        // return navigation.dispatch(NavigationActions.back());
      })
      .catch(e => {
        console.log(e);
        return this.setState({ loading: false });
      });
  };

  getParams = () => {
    const { postForm } = this.props;

    const params = {
      description: postForm.description.value,
      title: postForm.title.value,
      category: postForm.category.value,
      statuses: postForm.statuses.value,
      image: postForm.image,
      base64: postForm.base64,
      price: postForm.price, // price or cost?
    };
    return params;
  };

  renderStep = () => {
    const { props } = this;
    switch (props.postForm.currentPosition) {
      case 1:
        return <StepOne {...props} />;
      case 2:
        return <StepTwo {...props} />;
      case 3:
        return <StepThree {...props} />;
      default:
        return null;
    }
  };

  render() {
    const { loading } = this.state;
    const { form, onPostFormStepChange, postForm, updateForm } = this.props;
    return (
      <KeyboardAvoidingView behavior="padding" onPress={Keyboard.dismiss} style={[s.container]}>
        <TouchableOpacity activeOpacity={1} onPress={Keyboard.dismiss} style={s.stepsContainer}>
          {this.renderStep()}
        </TouchableOpacity>
        <View style={s.bottomContainer}>
          <FormButtons
            onPostFormStepChange={onPostFormStepChange}
            loading={loading}
            postForm={postForm}
            form={form}
            onSubmitForm={this.onSubmitForm}
            updateForm={updateForm}
          />
          <PostFormSteps />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const s = StyleSheet.create({
  container: {
    backgroundColor: '#efefef',
    width: '100%',
    flex: 1,
  },
  bottomContainer: {
    padding: 2,
    backgroundColor: '#fff',
    borderTopWidth: 2,
    width: '100%',
    borderColor: '#efefef',
  },
  stepsContainer: {
    flex: 3.5,
  },
});

const mapStateToProps = ({ postForm }) => ({
  postForm,
});

// APOLLO
// ====================================
// connect PostForm to apollo mutations for creating/saving a post
const ComponentWithData = compose(
  graphql(Post.Mutation.createPost, { name: 'createPost' }),
  graphql(Post.Mutation.savePost, { name: 'savePost' })
)(PostForm);

// RC-FORM OPTIONS
// ================================
// set form options for rc-form
const formOptions = {
  // 1. onFieldsChange runs like onChangeText
  // we can use it to fire a redux action,
  // to store the contents in redux
  onFieldsChange(props, changedFields) {
    console.log(changedFields);
    props.onPostFormFieldChange(changedFields);
  },

  // 2. we then get the redux store props to feed back into the component
  // using mapPropsToFields, and we use those props to map back into
  // rc-form's form inputs.
  // basically this is the glue between the redux store and rc-form's state
  mapPropsToFields({ postForm }) {
    return {
      title: createFormField({
        // ...postForm.title,
        value: postForm.title.value,
      }),
    };
  },
};

// EXPORT
// ====================================
export default connect(mapStateToProps, actions)(createForm(formOptions)(ComponentWithData));
