// @flow
import React from 'react';
import { StyleSheet } from 'react-native';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import { styleConstants, stylesConfig } from '../../../../lib/config';

// DOCS
// =========================
/**
 *
 * @namespace TextAreaInputRow
 * @description reusable email input
 * @prop onChange {function}
 * @prop value {string}
 * @prop placeholder {string}
 * @prop label {string}
 * @prop containerStyle {object}
 * @prop inputStyle {object}
 *
 */

type Props = {
  placeholder?: string,
  underlineColorAndroid?: string,
  autoFocus?: boolean,
  editable?: boolean,
};

class TextAreaInputRow extends React.PureComponent<Props> {
  static defaultProps = {
    placeholder: '',
    containerStyle: {
      marginLeft: 0,
      marginRight: 0,
    },
    // returnKeyType: 'done',
    underlineColorAndroid: 'transparent',
    numberOfLines: 1,
    autoFocus: false,
    editable: true,
    label: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      active: this.props.active || false,
    };
  }

  onActiveChange = () => {
    this.setState({ active: true });
    // fire this onBlur or onFocus
  };

  render() {
    const {
      autoFocus,
      returnKeyType,
      value,
      placeholder,
      onChange,
      onSubmitEditing,
      editable,
      underlineColorAndroid,
    } = this.props;

    return (
      <AutoGrowingTextInput
        returnKeyType={returnKeyType}
        autoCapitalize="none"
        editable={editable}
        underlineColorAndroid={underlineColorAndroid}
        clearButtonMode="while-editing"
        selectionColor={styleConstants.color.primary}
        autoFocus={autoFocus}
        style={[
          stylesConfig.textInputRowStyle,
          { width: '100%', textAlign: 'left', fontSize: styleConstants.fontSize.lg },
        ]}
        placeholder={placeholder}
        defaultValue={value}
        value={value}
        ref={input => (this.input = input)}
        onSubmitEditing={onSubmitEditing}
        onFocus={this.onActiveChange}
        onChangeText={onChange}
      />
    );
  }
}

const s = StyleSheet.create({
  textInput: {
    textAlign: 'right',
    fontSize: styleConstants.fontSize.xl,
    color: styleConstants.color.primary,
  },
  leftCol: {
    height: 30,
    flexDirection: 'row',
    // paddingLeft: styleConstants.defaultMargin,
  },
  rightCol: {
    minHeight: 30,
    flexDirection: 'row',
    paddingLeft: styleConstants.defaultMargin,
  },
});

export default TextAreaInputRow;
