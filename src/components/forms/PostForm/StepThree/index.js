// TOP LEVEL IMPORTS
import React from 'react'
import {
  View,
  StyleSheet
} from 'react-native'
// COMPONENTS
import TextAreaInputRow from './TextAreaInputRow';


const s = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#fff',
    borderRadius: 7,
    minHeight: 80,
    margin: 5, 
    padding: 10,
    marginTop: 10,
  },
  container: {
    minHeight: 80,
    marginTop: 15,
    padding: 10,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#ddd'
  }
});

class StepThree extends React.Component {
  render() {
    const { postForm, form } = this.props
    const { getFieldDecorator } = form
    return (
      <View style={{ paddingTop: 15}}>
        <View style={s.inputContainer}>
          {getFieldDecorator('description', {
            placeholder: 'description...',
            rules: [
              { required: false, message: 'Please input a description!' }
            ],
            initialValue: postForm.description && postForm.description.value
          })(
            <TextAreaInputRow 
              placeholder="Type your description here..." 
            />
          )}
        </View>
      </View>
    )
  }
}

export default StepThree
