// TOP LEVEL IMPORTS
import React from 'react'
import { View, StyleSheet } from 'react-native'
// COMPONENTS
import CategoriesArea from './CategoriesArea'
import StatusesInput from './StatusesInput'
//MODULES

// Notes on StepTwo form:
// ===================================
// StepTwo contains 3 inputs: statuses (Array), category (string), and price (string: optional)
// the price input will only show if the statuses array includes 'For Sale', otherwise, the post will
// not have a price.
// For the visual/style layout, we have a list of checkboxes for each status and
// categories opens another screen with options to tap and select.
// For the list of statuses checkboxes, there is logic that will disable the checkboxes based on slections. For instance,
// you can't offer something for free and for sale. But an item could be for sale or borrow.
// When a item is For Sale, we should a "price input" to the right of the checkbox.
// becuase of how rc-form works, we can't nest getFieldDecorator inside eachother, so we can't use getFieldDecorator
// in the StepTwo component and instead will have to use it in the StatusesInput (or just use vanilla redux)

class StepTwo extends React.Component {
  render() {
    return (
      <View style={s.container}>
        <View style={s.statusesContainer}>
          <StatusesInput {...this.props} />
        </View>
        <View style={s.statusesContainer}>
          <CategoriesArea {...this.props} />
        </View>
      </View>
    )
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15
  },
  statusesContainer: {
    margin: 5,
    minHeight: 70,
    padding: 10,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#ddd',
    justifyContent: 'center'
  }
})

export default StepTwo
