// @flow
import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { BasicTextInput } from '../../../inputs';

type Props = {
  value: String,
  onPostFormFieldChange: Function,
};

class CostInput extends React.PureComponent<Props> {
  render() {
    const { value, onPostFormFieldChange } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={s.inputContainer}>
          <BasicTextInput
            keyboardType="numeric"
            clear
            returnKeyType="done"
            onChange={value => {
              onPostFormFieldChange({
                // cost: { value },
                price: value,
              });
            }}
            inputStyle={s.input}
            value={value}
            placeholder="Price..."
            maxLength={10}
            locale={{ confirmLabel: 'Okay' }}
          />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const s = StyleSheet.create({
  inputContainer: {
    justifyContent: 'center',
    height: 45,
    marginTop: 5,
    width: 150,
  },
  input: {
    borderBottomWidth: 0,
  },
});

export default CostInput;
