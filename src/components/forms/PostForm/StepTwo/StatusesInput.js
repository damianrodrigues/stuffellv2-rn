// @flow
import React from 'react';
import { CheckBox } from 'react-native-elements';
import { View } from 'react-native';

import { styleConstants } from '../../../../lib/config';

import { BodyText } from '../../../text/index';
import CostInput from './CostInput';

export const STATUS_OPTIONS = [
  {
    value: 'For Sale',
    label: 'My item is available for sale',
  },
  {
    value: 'For Free',
    label: 'My item is available for free',
  },
  {
    value: 'For Borrow',
    label: 'My item is available to borrow',
  },
  {
    value: 'Just for Looking',
    label: 'My item just for Looking- but tell me what you think!',
  },
];

type Props = {
  postForm: Object,
  activeItem: boolean,
  onPostFormFieldChange: Function,
};

// EXPORTED COMPONENT
// ===================================
class StatusesInput extends React.PureComponent<Props> {
  static defaultProps = {
    label: 'Status',
    initialValue: null,
    message: 'Please choose a status!',
    required: true,
    data: [],
  };

  constructor(props) {
    super(props);

    this.state = {
      statuses: this.props.value || [],
    };
  }

  onCheckboxChange = (checked, value) => {
    const { onPostFormFieldChange, postForm } = this.props;

    let statuses = [...postForm.statuses.value];
    if (checked && !statuses.includes(value)) {
      statuses.push(value);
      statuses = {
        statuses: { value: statuses },
      };
      return onPostFormFieldChange(statuses);
    }
    if (!checked && statuses.includes(value)) {
      statuses = statuses.filter(e => e !== value);
      statuses = {
        statuses: { value: statuses },
      };
      return onPostFormFieldChange(statuses);
    }
  };

  render() {
    const { postForm, activeItem, onPostFormFieldChange } = this.props;
    const statuses = postForm.statuses.value;

    const defaultCheckboxProps = {
      fontFamily: styleConstants.fontFamily.regular,
      checkedColor: '#000',
      containerStyle: {
        backgroundColor: 'transparent',
        borderWidth: 0,
      },
    };

    return (
      <View
        style={{
          display: 'flex',
          alignItems: 'flex-start',
        }}>
        <BodyText>My stuff is:</BodyText>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <CheckBox
              {...defaultCheckboxProps}
              defaultChecked={statuses.includes('For Sale')}
              title="For Sale"
              checked={statuses.includes('For Sale')}
              textStyle={{
                fontSize: 17,
                color:
                  statuses.includes('Just for Looking') || statuses.includes('For Free')
                    ? '#ddd'
                    : '#000',
              }}
              onPress={() => this.onCheckboxChange(!statuses.includes('For Sale'), 'For Sale')}
              disabled={statuses.includes('Just for Looking') || statuses.includes('For Free')}
            />
          </View>
          <View style={{ flex: 1 }}>
            {statuses.length > 0 &&
              statuses.includes('For Sale') && (
                <CostInput
                  value={postForm.price}
                  activeItem={activeItem}
                  onPostFormFieldChange={onPostFormFieldChange} // updates redux store, takes in an object
                />
              )}
          </View>
        </View>
        <CheckBox
          {...defaultCheckboxProps}
          defaultChecked={statuses.includes('For Free')}
          title="For Free"
          checked={statuses.includes('For Free')}
          textStyle={{
            fontSize: 17,
            color:
              statuses.includes('Just for Looking') || statuses.includes('For Sale')
                ? '#ddd'
                : '#000',
          }}
          onPress={() => this.onCheckboxChange(!statuses.includes('For Free'), 'For Free')}
          disabled={statuses.includes('Just for Looking') || statuses.includes('For Sale')}
        />
        <CheckBox
          {...defaultCheckboxProps}
          defaultChecked={statuses.includes('For Borrow')}
          title="For Borrow"
          checked={statuses.includes('For Borrow')}
          textStyle={{
            fontSize: 17,
            color: statuses.includes('Just for Looking') ? '#ddd' : '#000',
          }}
          onPress={() => this.onCheckboxChange(!statuses.includes('For Borrow'), 'For Borrow')}
          disabled={statuses.includes('Just for Looking')}
        />
        <CheckBox
          {...defaultCheckboxProps}
          defaultChecked={statuses.includes('Just for Looking')}
          title="Just for Looking"
          checked={statuses.includes('Just for Looking')}
          textStyle={{
            fontSize: 17,
            color:
              statuses.length !== 0 && !statuses.includes('Just for Looking') ? '#ddd' : '#000',
          }}
          onPress={() =>
            this.onCheckboxChange(!statuses.includes('Just for Looking'), 'Just for Looking')
          }
          disabled={statuses.length !== 0 && !statuses.includes('Just for Looking')}
        />
      </View>
    );
  }
}

export default StatusesInput;
