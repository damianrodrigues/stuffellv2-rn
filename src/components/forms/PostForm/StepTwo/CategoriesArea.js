// TOP LEVEL IMPORTS
import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import { Icon, Button } from 'react-native-elements'
import {
  width,
  height,
  totalSize
} from 'react-native-dimension'

//MODULES
import {
  CATEGORY_OPTIONS,
  styleConstants
} from '../../../../lib/config'
import { BodyText } from '../../../text/index'

// CONSTANTS & DESTRUCTURING
// ========================================
const MAPPED_CATEGORY_OPTIONS = CATEGORY_OPTIONS.map(
  item => {
    return { value: item, label: item }
  }
)

const ICON_CONFIG = {
  name: 'chevron-right',
  color: '#ddd',
  containerStyle: {
    margin: 0,
    padding: 0,
    borderWidth: 1,
    borderColor: '#000'
  },
  iconStyle: {
    margin: 0,
    padding: 0,
    borderWidth: 1,
    borderColor: '#000'
  },
  size: 24
}

// EXPORTED COMPONENT
// ========================================
class CategoriesArea extends React.PureComponent {
  onPress = () => {
    this.props.navigation.navigate('selectOptions', {
      selectType: 'categories'
    })
  }
  render() {
    const { postForm } = this.props

    return (
      <TouchableOpacity
        onPress={this.onPress}
        style={s.container}
      >
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
          }}
        >
          <View
            style={{
              flex: 2
            }}
          >
            {postForm.category.value ? (
              <BodyText>{postForm.category.value}</BodyText>
            ) : (
              <BodyText>Select a Category</BodyText>
            )}
          </View>
          <View style={s.buttonContainer}>
            <Button
              small
              containerViewStyle={{
                padding: 0,
                height: 19,
                borderWidth: 0
              }}
              iconRight={ICON_CONFIG}
              buttonStyle={{
                backgroundColor: 'transparent',
                padding: 0
              }}
              textStyle={s.buttonTextStyle}
            />
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

// STYLES
// ========================================
const s = StyleSheet.create({
  container: {
    height: height(6),
    backgroundColor: '#fff',
    borderBottomWidth: 0, //1,
    borderTopWidth: 0, //1,
    borderColor: '#ddd',
    justifyContent: 'center'
  },
  buttonContainer: {
    flex: 1,
    display: 'flex',
    alignItems: 'flex-start'
  },
  buttonTextStyle: {
    color: styleConstants.color.primary,
    fontSize: 17
  }
})

// EXPORT
// ========================================
export default CategoriesArea
