// TOP LEVEL IMPORTS
import React from 'react';
import { Camera } from 'expo';
import { View, StyleSheet } from 'react-native';
import RegularButton from '../../../../common/RegularButton';

//
const s = StyleSheet.create({
  cameraButton: {
    // minWidth: 300,
    // alignSelf: 'flex-end',
    // margin: 'auto',
    marginBottom: 10,
    marginTop: 10,
  },
  cameraButtonWhite: {
    // minWidth: 300,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop: 10,
  },
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
  },
  takePictureContainer: {
    flex: 1,
    // position: 'absolute',
    // bottom: 0,
    // left: 0,
    alignSelf: 'center',
    backgroundColor: 'transparent',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
});

class SnapCamera extends React.PureComponent {
  state = {
    type: Camera.Constants.Type.back,
    loading: false,
  };

  onPress = async () => {
    this.setState({ loading: true });
    if (this.camera) {
      const options = { quality: 0.3, base64: true };
      const result = await this.camera.takePictureAsync(options);
      this.setState({ loading: true });
      this.props.onSnap(result);
    }
  };

  render() {
    return (
      <View style={s.container}>
        <Camera
          style={{ flex: 1, marginTop: 10 }}
          type={this.state.type}
          ref={ref => {
            this.camera = ref;
          }}>
          <View style={s.takePictureContainer}>
            <RegularButton
              title="TAKE PICTURE"
              loading={this.state.loading}
              disabled={this.state.loading}
              onPress={this.onPress}
              buttonStyle={s.cameraButton}
            />

            <RegularButton
              title="CANCEl"
              onPress={this.props.onDissmissCamera}
              buttonStyle={s.cameraButtonWhite}
              textStyle={{ color: 'black' }}
            />
          </View>
        </Camera>
      </View>
    );
  }
}

export default SnapCamera;
