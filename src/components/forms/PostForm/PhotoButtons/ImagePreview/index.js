// TOP LEVEL IMPORTS
import React from 'react';
import {
  View,
  Keyboard,
  Image,
  Text,
  StyleSheet
} from 'react-native'
import { width, height } from 'react-native-dimension'
import { styleConstants } from '../../../../../lib/config'




const s = StyleSheet.create({
    image: {
        marginTop: 10,
        marginBottom: 15,
        width: width(50),
        height: height(25),
        alignSelf: 'center'
      }
});


const ImagePreview = ({ image, imageUploading }) => {
    return (
        <View>
            <Image
                source={{ uri: image, cache: 'force-cache'}}
                style={s.image}
            />
            {imageUploading && (
                <Text style={{textAlign: 'center'}}>
                    uploading...
                </Text>
            )}
        </View>
    )
}

export default ImagePreview;