// TOP LEVEL IMPORTS
import React from 'react';
import { ImagePicker, Permissions } from 'expo';
import { View, Keyboard, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import SnapCamera from './SnapCamera';
import ImagePreview from './ImagePreview';
import RegularButton from '../../../common/RegularButton';
import { UploadHelpers } from '../../../../lib/helpers';
// ================================================================

const IMAGE_LIBRARY_OPTIONS = {
  allowsEditing: true,
  aspect: [4, 3],
  quality: 0.3,
  base64: true,
  exif: true,
};

class PhotoButtons extends React.PureComponent {
  state = {
    hasCameraPermission: null,
    showCamera: false,
  };

  componentDidMount() {
    this.setStatus();
  }

  setStatus = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  saveBase64InRedux = base64 => {
    this.props.onPostFormFieldChange({
      base64,
    });
  };

  onSuccessfulUpload = response => {
    const image = { value: response, name: 'image' };
    this.props.onPostFormFieldChange({
      image: response,
      imageUploading: false,
    });
  };

  onSuccessfulSelection = result => {
    this.saveBase64InRedux(result.base64);
    const fileToUpload = { uri: result.uri, base64: result.base64 };
    UploadHelpers.handleFileUpload(fileToUpload, (error, response) => {
      if (response) {
        return this.onSuccessfulUpload(response);
      }

      if (error) {
        console.log(error);
      }
    });
  };

  onLibraryImageClick = async () => {
    Keyboard.dismiss();
    let result;
    try {
      result = await ImagePicker.launchImageLibraryAsync(IMAGE_LIBRARY_OPTIONS);
    } catch (e) {
      this.setState({ loading: false });
      return console.log(e);
    }
    this.onSuccessfulSelection(result);
  };

  onTakeImageClick = async () => {
    Keyboard.dismiss();
    this.setState({ showCamera: true });
  };

  onDissmissCamera = () => {
    console.log('111111');
    this.setState({ showCamera: false });
  };

  onSnap = ({ uri, base64 }) => {
    const _this = this;
    this.props.onPostFormFieldChange({
      image: uri,
      base64,
      imageUploading: true,
    });
    _this.setState({ showCamera: false });
    return this.onSuccessfulSelection({ uri, base64 });
  };

  renderImage = () => {
    const { postForm } = this.props;
    if (postForm && postForm.image) {
      return <ImagePreview image={postForm.image} />;
    }
  };

  render() {
    const { postForm } = this.props;

    const BUTTON_STYLES = {
      width: 250,
      alignSelf: 'center',
      marginTop: 10,
    };

    if (this.state.showCamera) {
      return <SnapCamera onSnap={this.onSnap} onDissmissCamera={this.onDissmissCamera} />;
    }

    return (
      <View style={s.container}>
        {postForm.image && (
          <ImagePreview image={postForm.image} imageUploading={postForm.imageUploading} />
        )}
        <RegularButton
          title="Take a Photo"
          onPress={this.onTakeImageClick}
          buttonStyle={BUTTON_STYLES}
        />
        <RegularButton
          title="Select Photo"
          onPress={this.onLibraryImageClick}
          buttonStyle={BUTTON_STYLES}
        />
        <View />
      </View>
    );
  }
}

//
const s = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
  },
});

const mapStateToProps = ({ postForm }) => ({
  postForm,
});
export default connect(mapStateToProps)(PhotoButtons);
