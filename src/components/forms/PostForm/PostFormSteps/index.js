// TOP LEVEL IMPORTS
import React from 'react';
import { View } from 'react-native';
//REDUX
import { connect } from 'react-redux';
//MODULES
import { styleConstants } from '../../../../lib/config';

import StepIndicator from 'react-native-step-indicator';

const STEP_LABELS = ["Photo","Details","Description"];

const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: styleConstants.color.primary,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: styleConstants.color.primary,
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: styleConstants.color.primary,
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: styleConstants.color.primary,
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: styleConstants.color.primary,
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: styleConstants.color.primary
}

const PostFormSteps = ({ currentPosition }) => {
      return (
        <View style={{width: '100%', paddingTop: 3}}>
          <StepIndicator
           customStyles={customStyles}
           currentPosition={currentPosition-1}
           labels={STEP_LABELS}
           stepCount={3}
          />
        </View>
    )
}

let mapStateToProps = ({ postForm }) => {
  return {
    currentPosition: postForm.currentPosition
  }
}
export default connect(mapStateToProps)(PostFormSteps)