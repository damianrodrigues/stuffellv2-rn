// TOP LEVEL IMPORTS
import React from 'react';
import { View, StyleSheet } from 'react-native';
// LIB
// COMPONENTS
import PhotoButtons from '../PhotoButtons';
import { BasicTextInput } from '../../../inputs';
import { BodyText } from '../../../text/index';

// EXPORTED COMPONENT
// ====================================
class StepOne extends React.PureComponent {
  render() {
    const { postForm, form, onPostFormFieldChange } = this.props;
    const { getFieldDecorator } = form;

    // setup the options for the title input using rc-form
    const titleOptions = {
      rules: [
        {
          required: true,
          message: 'Please input your title!',
        },
      ],
      initialValue: postForm.title && postForm.title.value,
      placeholder: 'Title',
    };

    return (
      <View style={{ flex: 1 }}>
        <PhotoButtons onPostFormFieldChange={onPostFormFieldChange} />
        <View style={{ margin: 5, paddingTop: 5, backgroundColor: '#efefef' }}>
          <BodyText style={s.titleText}>Enter item title:</BodyText>
          <View style={s.titleInputContainer}>
            {getFieldDecorator('title', titleOptions)(
              <BasicTextInput
                inputStyle={s.input}
                keyboardType="default"
                placeholder="e.g. 10 foot step ladder"
              />
            )}
          </View>
        </View>
      </View>
    );
  }
}

// STYLES
// ====================================
const s = StyleSheet.create({
  titleInputContainer: {
    backgroundColor: '#fff',
    borderRadius: 7,
    minHeight: 40,
    marginTop: 10,
    marginBottom: 5,
  },
  titleText: {
    marginLeft: 3,
  },
  input: {
    paddingLeft: 10,
  },
});

// EXPORT
// ====================================
export default StepOne;
