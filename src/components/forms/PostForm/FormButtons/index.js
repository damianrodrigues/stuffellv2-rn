// @flow
import React from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import { width } from 'react-native-dimension';
import { styleConstants } from '../../../../lib/config';
import RegularButton from '../../../common/RegularButton';

type Props = {
  form: Object, // this is the form object from rc-form, a package that helps with form state
  onPostFormStepChange: Function, // a function that runs to increment/decrement the current step/view for this form
  postForm: Object, // an object from the redux store, which holds the  state for the "Post" form
  onSubmitForm: Function, // fired on the final screen
  updateForm: Booleon, // this is a flag/boolean that can tell us whether we're creating a post or editing a post. Helpful for doing this like deciding to show the button text (e.g. "Save Changes" vs "Create Post")
};

class FormButtons extends React.PureComponent<Props> {
  state = {
    loading: false,
  };

  getDisabled = () => {
    const { postForm, form } = this.props;
    const { getFieldError, getFieldValue } = form;
    let disabled = true;

    // helper function for figuring out if a field is complete,
    // pass in the name of the field (given in getFieldDecorator)
    const fieldIsClean = fieldName => !!(!getFieldError(fieldName) && getFieldValue(fieldName));

    // if we're on step 1 and the user has not
    // filled in the tile, "next" button is enabled
    if (postForm.currentPosition === 1 && fieldIsClean('title')) {
      disabled = false;
    }

    // if we've on step 2, make sure step 1 is done,
    // and make sure that statuses and categories have been chosen
    // TODO: make sure price is filled in if user selected "For Sale" let priceIsCompleteIfStatusIncludesForSale = (this.props.postForm.statuses.value.includes('For Sale') && postForm.price.value)
    const step2Complete =
      fieldIsClean('title') && postForm.statuses.value.length > 0 && postForm.category.value;
    if (postForm.currentPosition === 2 && step2Complete) {
      disabled = false;
    }

    // if we're on step 3, description is not required, so we can mark the button enabled right away
    if (postForm.currentPosition === 3) {
      disabled = false;
    }

    return disabled;
  };

  onPressInLeft = () => {
    // decrement the current step we are on
    this.props.onPostFormStepChange(this.props.postForm.currentPosition - 1);
  };

  onPressInRight = () => {
    const { postForm, onSubmitForm, onPostFormStepChange } = this.props;
    // if we're on step 3 (the final step), we don't want to
    // increment the step, instead we want to submit the form
    if (postForm.currentPosition === 3) {
      return onSubmitForm();
    }
    // increment the current step we are on
    onPostFormStepChange(postForm.currentPosition + 1);
  };

  render() {
    const { postForm, loading } = this.props;
    const { updateForm } = this.props;
    // the text that is shown on the button when we're on the final step (i.e. the text for the submit button)
    const submissionText = updateForm ? 'Save Changes' : 'Post My Stuff';

    const disabled = this.getDisabled();

    // on the first screen, we should a full-width button, on the other screens,
    // we change width so we can fit an additional "back button"
    let buttonStyle;

    if (postForm.currentPosition === 2 || postForm.currentPosition === 3) {
      buttonStyle = [s.primaryButton, s.primaryButtonThin];
    } else {
      buttonStyle = s.primaryButton;
    }

    return (
      <View style={s.buttonContainer}>
        {postForm.currentPosition > 1 && (
          <RegularButton
            title="Back"
            onPress={this.onPressInLeft}
            buttonStyle={s.backButton}
            textStyle={{ color: '#000' }}
          />
        )}
        {postForm.currentPosition === 1 && (
          <RegularButton
            title="Next"
            onPress={this.onPressInRight}
            buttonStyle={[
              buttonStyle,
              { width: Dimensions.get('window').width - styleConstants.defaultMargin * 2 },
            ]}
            disabled={disabled}
          />
        )}
        {postForm.currentPosition > 1 && (
          <RegularButton
            title={postForm.currentPosition === 3 ? submissionText : 'Next'}
            onPress={this.onPressInRight}
            buttonStyle={buttonStyle}
            loading={loading}
            disabled={disabled}
          />
        )}
      </View>
    );
  }
}

const s = StyleSheet.create({
  backButton: {
    padding: 5,
    width: width(25),
    minHeight: 34,
    backgroundColor: '#d9d9d9',
  },
  primaryButton: {
    padding: 5,
    width: width(70),
    minHeight: 34,
  },
  primaryButtonThin: {
    width: width(55),
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: 5,
    minHeight: 50,
  },
});

export default FormButtons;
