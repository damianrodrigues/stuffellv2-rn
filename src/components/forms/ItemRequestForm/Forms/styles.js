// @flow
import { StyleSheet } from 'react-native';

import { styleConstants } from '../../../../lib/config';

const { color, fontWeight } = styleConstants;

export default StyleSheet.create({
  content: {
    flex: 1,
    marginTop: 40,
    marginBottom: 15,
  },
  titleNormal: {
    fontWeight: fontWeight.light,
    marginBottom: 5,
  },
  formTitle: {
    height: 80,
  },
  boldText: {
    fontWeight: fontWeight.bold,
  },
  formDescription: {
    height: 150,
  },
  inputTitle: {
    backgroundColor: color.screenBackground,
    padding: 10,
    borderRadius: 8,
  },
  inputDescription: {
    backgroundColor: color.screenBackground,
    padding: 15,
    borderRadius: 8,
    textAlignVertical: 'top',
    flex: 1,
  },
});
