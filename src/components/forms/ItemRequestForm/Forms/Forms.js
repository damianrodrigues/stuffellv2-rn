// @flow
import * as React from 'react';
import { View, Text } from 'react-native';
import { Input } from '../../../inputs';

import s from './styles';

type Props = {
  itemRequestForm: Function,
  form: Object,
};

class RequestItemForms extends React.PureComponent<Props> {
  render(): React.Node {
    const { itemRequestForm, form } = this.props;
    const { getFieldDecorator } = form;
    // ======
    const titleOptions = {
      rules: [
        {
          required: true,
          message: 'Please input your title!',
        },
      ],
      initialValue: itemRequestForm.title && itemRequestForm.title.value,
      placeholder: 'Title',
    };
    // ======
    const descriptionOptions = {
      rules: [
        {
          required: false,
          message: 'Please input a description!',
        },
      ],
      initialValue: itemRequestForm.description && itemRequestForm.description.value,
      placeholder: 'Description',
    };
    // ======
    return (
      <View style={s.content}>
        <View style={s.formTitle}>
          <Text style={s.titleNormal}>
            <Text style={s.boldText}>Title </Text>
            (what are you looking for?):
          </Text>
          {getFieldDecorator('title', titleOptions)(
            <Input
              keyboardType="default"
              placeholder="e.g. 10 foot step ladder"
              style={s.inputTitle}
              returnKeyType="done"
            />
          )}
        </View>
        <View style={s.formDescription}>
          <Text style={s.titleNormal}>
            <Text style={s.boldText}>Description </Text>
            (details about what you are looking for):
          </Text>
          {getFieldDecorator('description', descriptionOptions)(
            <Input
              blurOnSubmit
              returnKeyType="done"
              style={s.inputDescription}
              keyboardType="default"
              placeholder="e.g. I want to borrow a ladder next weekend to paint the garage"
              multiline
            />
          )}
        </View>
      </View>
    );
  }
}

export default RequestItemForms;
