// @flow
// TOP LEVEL IMPORTS
import * as React from 'react';
import { createForm, createFormField } from 'rc-form';
import { View, Text } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { ItemRequest } from '../../../apollo';
// COMPONENTS
import ItemRequestModalButton from '../../common/ItemRequestModalButton';
import RegularButton from '../../common/RegularButton';
import Forms from './Forms';
// STYLES
import s from './styles';

type Props = {
  itemRequestForm: Object,
  createItemRequest: Function,
  navigation: Object,
  clearItemRequestForm: Function,
  onItemRequestFormFieldChange: Function,
  saveItemRequest: Function,
  screenProps: Object,
};

type State = {
  isModalVisible: boolean,
};

class ItemRequestForm extends React.PureComponent<Props, State> {
  state = {
    isModalVisible: false,
  };

  componentWillUnmount = () => {
    const { clearItemRequestForm } = this.props;
    clearItemRequestForm();
  };

  onRequestPress = async () => {
    const { itemRequestForm, createItemRequest, screenProps } = this.props;
    const refetchQueriesVariables = {
      params: {
        userId: screenProps.data.user._id,
      },
    };
    const variables = {
      params: {
        description: itemRequestForm.description.value,
        title: itemRequestForm.title.value,
      },
    };
    const refetchQueries = [
      { query: ItemRequest.Query.myItemRequests, variables: refetchQueriesVariables },
    ];
    createItemRequest({ variables, refetchQueries }).then(() => {
      this.setState({ isModalVisible: true });
    });
  };

  onSaveRequest = async () => {
    const { itemRequestForm, saveItemRequest, navigation, screenProps } = this.props;
    const refetchQueriesVariables = {
      params: {
        userId: screenProps.data.user._id,
      },
    };
    const refetchQueries = [
      { query: ItemRequest.Query.myItemRequests, variables: refetchQueriesVariables },
    ];
    const variables = {
      params: {
        description: itemRequestForm.description.value,
        title: itemRequestForm.title.value,
      },
      _id: itemRequestForm.itemRequestId,
    };

    saveItemRequest({ variables, refetchQueries }).then(() => {
      navigation.goBack(null);
    });
  };

  onCloseModal = () => {
    this.setState({ isModalVisible: false });
  };

  render(): React.Node {
    const { itemRequestForm } = this.props;
    const { isModalVisible } = this.state;
    const { editing } = itemRequestForm;
    const isButtonActive = itemRequestForm.title.value === '';
    return (
      <KeyboardAwareScrollView>
        <View style={s.content}>
          <View style={s.innerContent}>
            <View style={s.title}>
              <Text style={s.titleNormal}>
                <Text style={s.titleBold}>Request an item</Text> from your network{' '}
              </Text>
              <Text style={s.titleNormal}>to see if anyone has what you are looking for.</Text>
            </View>
            <Forms {...this.props} />
          </View>
          <View style={s.footer}>
            {!editing ? (
              <ItemRequestModalButton
                {...this.props}
                onRequestPress={this.onRequestPress}
                onCloseModal={this.onCloseModal}
                isModalVisible={isModalVisible}
                isButtonActive={isButtonActive}
                s={s}
              />
            ) : (
              <RegularButton
                onPress={() => this.onSaveRequest()}
                title="Save"
                buttonStyle={s.formButton}
                textStyle={s.formButtonText}
              />
            )}
          </View>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

// FORM OPTIONS
// ================================

const formOptions = {
  onFieldsChange({ onItemRequestFormFieldChange }, changedFields) {
    onItemRequestFormFieldChange(changedFields);
  },
  mapPropsToFields({ itemRequestForm }) {
    return {
      title: createFormField({
        value: itemRequestForm.title.value,
      }),
      description: createFormField({
        value: itemRequestForm.description.value,
      }),
    };
  },
};

export default createForm(formOptions)(ItemRequestForm);
