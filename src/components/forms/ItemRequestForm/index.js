// @flow
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';
import { ItemRequest } from '../../../apollo';
import * as actions from '../../../redux/actions';

import ItemRequestComponent from './ItemRequestForm';

const mapStateToProps = ({ itemRequestForm }) => ({
  itemRequestForm,
});

const enhancedItemRequest = compose(
  graphql(ItemRequest.Mutation.createItemRequest, { name: 'createItemRequest' }),
  graphql(ItemRequest.Mutation.saveItemRequest, { name: 'saveItemRequest' }),
  connect(mapStateToProps, actions)
)(ItemRequestComponent);

export default enhancedItemRequest;
