// @flow
import { StyleSheet } from 'react-native';
import { styleConstants } from '../../../lib/config';

const { color, fontWeight, fontSize } = styleConstants;

export default StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: color.screenBackgroundGrey,
  },
  innerContent: {
    flex: 1,
    padding: 15,
  },
  footer: {
    justifyContent: 'center',
    backgroundColor: color.screenBackground,
    height: 75,
  },
  formButton: {
    width: '100%',
    height: 35,
    borderColor: color.primary,
    backgroundColor: color.primary,
  },
  formButtonText: { color: '#fff', fontSize: 17 },
  title: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 10,
  },
  titleNormal: {
    fontSize: fontSize.md,
  },
  titleBold: {
    fontWeight: fontWeight.bold,
    fontSize: fontSize.md,
  },
  dialogContent: {
    backgroundColor: styleConstants.color.primary,
    borderColor: styleConstants.color.primary,
    borderRadius: 20,
  },
});
