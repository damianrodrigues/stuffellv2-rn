// @flow
import * as React from 'react';
import { View, Text, TextInput, Image } from 'react-native';
import { Post, ItemRequest } from '../../../apollo';
// COMPONENTS
import RegularButton from '../../common/RegularButton';
// LIB
import { appConfig } from '../../../lib/config';
// STYLES
import s from './styles';
// TYPES
type Props = {
  item: Object,
  navigation: Object,
  statuses: Object,
  categories: Object,
  searchText: string,
  createConversation: Function,
  screenProps: Props,
  name: string,
  data: Object,
};
// TYPES
type State = {
  loading: boolean,
  messageValue: string | null,
  disabled: boolean,
};

export default class NewMessageForm extends React.PureComponent<Props, State> {
  state = {
    loading: false,
    messageValue: null,
    disabled: false,
  };

  onCreateConversation = () => {
    const { item, navigation, statuses, categories, searchText, createConversation } = this.props;
    const { messageValue } = this.state;
    const { __typename } = item;

    this.setState({ loading: true });
    const conversationExists = this.conversationExists();
    const name = `${item.owner.profile.firstName} ${item.owner.profile.lastName}`;

    if (conversationExists) {
      return navigation.navigate('conversation', { _id: item._id, name });
    }

    if (__typename === 'Post') {
      const variables = {
        params: {
          parentId: item._id,
          parentModelType: 'post',
          modelType: 'postCoversation',
          messageValue,
        },
      };
      const params = {
        statuses,
        categories,
        searchText,
        skip: 0,
      };
      const refetchQueries = [
        { query: Post.Query.posts, variables: { params } },
        { query: Post.Query.postsCount, variables: { params } },
      ];

      createConversation({ variables, refetchQueries })
        .then(res => {
          this.setState({ loading: false });
          const conversationId = res.data.createConversation._id;
          return navigation.navigate('conversation', { _id: conversationId, name });
        })
        .catch(e => {
          console.log(e);
          this.setState({ loading: false });
        });
    }

    if (__typename === 'ItemRequest') {
      const variables = {
        params: {
          parentId: item._id,
          parentModelType: 'itemRequest',
          modelType: 'itemRequestCoversation',
          messageValue,
        },
      };
      const params = {
        searchText,
        skip: 0,
      };
      const refetchQueries = [
        { query: ItemRequest.Query.itemRequests, variables: { params } },
        { query: ItemRequest.Query.itemRequestsCount, variables: { params } },
      ];
      createConversation({ variables, refetchQueries })
        .then(res => {
          this.setState({ loading: false });
          const conversationId = res.data.createConversation._id;
          return navigation.navigate('conversation', { _id: conversationId, name });
        })
        .catch(e => {
          console.log(e);
          this.setState({ loading: false });
        });
    }
  };

  onChangeText = (messageValue: string) => {
    this.setState({ messageValue });
  };

  conversationExists = () => {
    const { item, screenProps } = this.props;
    const { data } = screenProps;
    const { user } = data;
    return (
      item.buyerIds &&
      item.buyerIds.length > 0 &&
      user &&
      user._id &&
      item.buyerIds.includes(user._id)
    );
  };

  render() {
    const { name } = this.props;
    const { loading, disabled } = this.state;
    return (
      <View style={s.container}>
        <View style={s.avatarHolder}>
          <Image style={s.avatarImage} alt="post-avatar" source={appConfig.images.defaultAvatar} />
          <Text style={s.avatarText}>Send a message to {name}</Text>
        </View>
        <TextInput
          onChangeText={this.onChangeText}
          autoFocus
          multiline
          style={s.formInput}
          returnKeyType="default"
        />
        <RegularButton
          onPress={this.onCreateConversation}
          title="Send"
          loading={loading}
          disabled={disabled}
          buttonStyle={s.formButton}
          textStyle={s.formButtonText}
        />
      </View>
    );
  }
}
