import { graphql, compose } from 'react-apollo';
import { connect } from 'react-redux';

import { Conversation } from '../../../apollo';

import NewMessageForm from './NewMessageForm';

const mapStateToProps = ({ search }) => ({
  statuses: search.statuses,
  categories: search.categories,
  searchText: search.searchText,
});

const NewMessageFormWithData = compose(
  graphql(Conversation.Mutation.createConversation, {
    name: 'createConversation',
  })
)(NewMessageForm);

export default connect(mapStateToProps)(NewMessageFormWithData);
