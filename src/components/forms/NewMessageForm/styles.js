// @flow
import { StyleSheet } from 'react-native';

import { styleConstants } from '../../../lib/config';

export default StyleSheet.create({
  container: { padding: 10, flex: 1 },
  avatarHolder: { flexDirection: 'row', alignItems: 'center' },
  avatarImage: { width: 40, height: 40, borderRadius: 20 },
  avatarText: { marginLeft: 15 },
  formInput: {
    marginTop: 7.5,
    height: 150,
    fontSize: 17,
    fontFamily: styleConstants.fontFamily.regular,
    borderColor: '#eee',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
  },
  formButton: {
    width: '100%',
    height: 40,
    borderColor: styleConstants.color.primary,
    alignSelf: 'center',
    marginTop: 5,
    backgroundColor: styleConstants.color.primary,
  },
  formButtonText: { color: '#fff', fontSize: 17 },
});

