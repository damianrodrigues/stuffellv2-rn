// TOP LEVEL IMPORTS
import React from 'react';
import { View, StyleSheet } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';




// DOC
// =========================
/**
 * @namespace ScreenContainer
 * @description Generic container to use as a wrapper around 95% of OCS's screens
 * @prop children {object} - children react component to display inside the ScrollView
 */



const ScreenContainer = ({ children }) => {
  return (
    <View style={s.container}>
      { children }
    </View>
  );
};

const s = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: styleConstants.color.screenBackground,
  }
})

export default ScreenContainer;