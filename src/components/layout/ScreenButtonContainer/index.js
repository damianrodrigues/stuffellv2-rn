// TOP LEVEL IMPORTS
import React from 'react';
import { View, StyleSheet } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';

// DOCS
// =========================
/**
 * @name ScreenButtonContainer
 * @description a wrapper that will hold a button at the bottom of the screen. 
 * Typically used along with ScreenContainer and ScreenContainerList
 */


 // EXPORTED COMPONENTN
// =========================
class ScreenButtonContainer extends React.PureComponent {
  render() {
    
    const { children, style } = this.props;

    return (
      <View style={[s.buttonContainer, style]}>
        { children }
      </View>
    );
  }
}


// STYLES
// =========================
const s = StyleSheet.create({
  buttonContainer: {
    //position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    height: 100,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});



// PROPS
// =========================
ScreenButtonContainer.defaultProps = {
  style: { }
}



// EXPORT
// =======================
export default ScreenButtonContainer;