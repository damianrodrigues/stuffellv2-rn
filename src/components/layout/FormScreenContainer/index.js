// TOP LEVEL IMPORTS
import React from 'react';
import { View } from 'react-native';
// CONSTANTS & DESCTRUCTURING
// ==============================

// EXPORTED COMPONENT
// ==============================
class FormScreenContainer extends React.PureComponent {
  render() {
    return (
      <View
        // keyboardVerticalOffset={0}
        // behavior={'padding'}
        style={[
          {
            flex: 1,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center',
          },
          this.props.style,
        ]}>
        {this.props.children}
        {/* <KeyboardSpacer /> */}
      </View>
    );
  }
}

// STYLES
// ==============================

// PROPS
// ==============================
FormScreenContainer.defaultProps = {
  style: {},
};

// EXPORT
// ==============================
export default FormScreenContainer;
