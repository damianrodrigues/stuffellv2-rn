import { 
	ON_POST_FORM_STEP_CHANGE, 
  ON_POST_FORM_FIELD_CHANGE,
  ON_POST_FORM_PRICE_CHANGE,
  CLEAR_POST_FORM,
  SET_ACTIVE_ITEM,
} from '../actions/types';


const INITIAL_STATE = {
  currentPosition: 1,
  image: null,
  title: { value: null },
  description: { value: null },
  statuses: { value: [] },
  category: { value: null },
  imageUploading: false,
  //cost: { value: '0' },
  price: 0,
  activeItem: null,
  editing: false
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    
    // keeps track of what step we are on (right now, we have 3 steps/forms/views)
    case ON_POST_FORM_STEP_CHANGE:
      return { ...state, currentPosition: action.payload }
    
    // clears the post form
    case CLEAR_POST_FORM:
      return INITIAL_STATE;

    // 
    case ON_POST_FORM_FIELD_CHANGE:
      return { ...state, ...action.payload }

    // we need to track the onchange for price separately because _____________
    case ON_POST_FORM_PRICE_CHANGE:
      return { ...state, price: action.payload }
    
    // set's the active item/post, which is used when editing a item/post
    case SET_ACTIVE_ITEM:
      return { ...state, activeItem: action.payload }
    
    default:
      return state;
      
  }
}