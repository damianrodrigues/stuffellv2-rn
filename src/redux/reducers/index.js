// @flow
import { combineReducers } from 'redux';
//
import { reducer as network } from 'react-native-offline';
import search from './search_reducer';
import postForm from './post_form_reducer';
import itemRequestForm from './item_request_form_reducer';
import modals from './modal_reducer';
//
//
const rootReducer = combineReducers({
  // ui,
  itemRequestForm,
  postForm,
  network,
  search,
  modals,
});

export default (state, action) =>
  action.type === 'USER_LOGOUT' ? rootReducer(undefined, action) : rootReducer(state, action);
