// @flow
import {
  ON_ITEM_REQUEST_FORM_FIELD_CHANGE,
  CLEAR_ITEM_REQUEST_FORM,
  SET_ACTIVE_ITEM_REQUEST,
} from '../actions/types';

const INITIAL_STATE = {
  title: { value: '' },
  description: { value: '' },
  editing: false,
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    // clears the post form
    case CLEAR_ITEM_REQUEST_FORM:
      return INITIAL_STATE;
    //
    case ON_ITEM_REQUEST_FORM_FIELD_CHANGE:
      return { ...state, ...action.payload };
    case SET_ACTIVE_ITEM_REQUEST:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
