import {
  ON_ARCHIVE_MODAL_OPEN,
  ON_SELL_MODAL_OPEN,
  SET_ACRHIVE_MODE,
  SET_SELL_MODE,
} from '../actions/types';

const INITIAL_STATE = {
  archive: false,
  sell: false,
  modalArchiveFunc: null,
  modalSellFunc: null,
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ON_ARCHIVE_MODAL_OPEN:
      return { ...state, modalArchiveFunc: action.payload };
    case ON_SELL_MODAL_OPEN:
      return { ...state, modalSellFunc: action.payload };
    case SET_ACRHIVE_MODE:
      return { ...state, archive: action.payload };
    case SET_SELL_MODE:
      return { ...state, sell: action.payload };
    default:
      return state;
  }
}
