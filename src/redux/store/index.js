import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'; // not implemented yet
import { createNetworkMiddleware } from 'react-native-offline';
import reducers from '../reducers/index';

// For documentation see: https://redux.js.org/
const networkMiddleware = createNetworkMiddleware();

const store = createStore(
  reducers,
  {}, // initial state
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  composeWithDevTools(applyMiddleware(networkMiddleware))
);

export { store };
