// FILTER ACTIONS
// =====================
export const SEARCH_TEXT = 'search_text';
export const ON_CLEAR_FILTERS = 'on_clear_filters';
export const ON_OFFSET_CHANGE = 'on_offset_change';

// FORM ACTIONS
// =====================
/* export const TOGGLE_EDIT_FORM = 'toggle_edit_form';
export const ON_CLEAR_FORM = 'ON_CLEAR_FORM';
export const ON_FORM_CHANGE = 'ON_FORM_CHANGE'; */

// POST FORM ACTIONS
// =====================
export const ON_POST_FORM_STEP_CHANGE = 'ON_POST_FORM_STEP_CHANGE';
export const ON_POST_FORM_FIELD_CHANGE = 'ON_POST_FORM_FIELD_CHANGE';
export const ON_POST_FORM_PRICE_CHANGE = 'ON_POST_FORM_PRICE_CHANGE';
export const CLEAR_POST_FORM = 'CLEAR_POST_FORM';
export const SET_ACTIVE_ITEM = 'SET_ACTIVE_ITEM';

// ITEM REQUEST FORM ACTIONS
// =====================
export const ON_ITEM_REQUEST_FORM_FIELD_CHANGE = 'ON_ITEM_REQUEST_FORM_FIELD_CHANGE';
export const CLEAR_ITEM_REQUEST_FORM = 'CLEAR_ITEM_REQUEST_FORM';
export const SET_ACTIVE_ITEM_REQUEST = 'SET_ACTIVE_ITEM_REQUEST';

// SEARCH ACTIONS
// =====================
export const ON_SEARCH_TEXT_CHANGE = 'ON_SEARCH_TEXT_CHANGE';
export const ON_SEARCH_CLEAR = 'ON_SEARCH_CLEAR';
export const ON_CATEGORIES_CHANGE = 'ON_CATEGORIES_CHANGE';
export const ON_FRIEND_SEARCH_TEXT_CHANGE = 'ON_FRIEND_SEARCH_TEXT_CHANGE';
export const ON_STATUSES_CHANGE = 'ON_STATUSES_CHANGE';

// MODALS ACTIONS
// =====================
export const ON_ARCHIVE_MODAL_OPEN = 'on_archive_modal_open';
export const ON_SELL_MODAL_OPEN = 'on_sell_modal_open';
export const SET_ACRHIVE_MODE = 'set_archive_mode';
export const SET_SELL_MODE = 'set_sell_mode';

export const USER_LOGOUT = 'USER_LOGOUT';
