import {
    ON_POST_FORM_STEP_CHANGE,
    ON_POST_FORM_FIELD_CHANGE,
    CLEAR_POST_FORM,
    ON_POST_FORM_PRICE_CHANGE,
    SET_ACTIVE_ITEM
  } from './types';
  
  export const onPostFormStepChange = step => {

    return {
      type: ON_POST_FORM_STEP_CHANGE,
      payload: step,
    };
  };
  
  export const onPostFormPriceChange = price => {
    return {
      type: ON_POST_FORM_PRICE_CHANGE,
      payload: price,
    };
  };
  
  export const onPostFormFieldChange = changedFields => {
    return {
      type: ON_POST_FORM_FIELD_CHANGE,
      payload: changedFields,
    };
  };
  
  export const clearPostForm = () => {
    return {
      type: CLEAR_POST_FORM,
    };
  };
  
  export const setActiveItem = item => {
    return {
      type: SET_ACTIVE_ITEM,
      payload: item,
    };
  };