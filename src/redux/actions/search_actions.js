import {
  ON_SEARCH_TEXT_CHANGE,
  ON_SEARCH_CLEAR,
  ON_CATEGORIES_CHANGE,
  ON_STATUSES_CHANGE,
  ON_FRIEND_SEARCH_TEXT_CHANGE,
} from './types';

export const onStatusesChange = status => {
  return {
    type: ON_STATUSES_CHANGE,
    payload: status,
  };
};

export const onCategoriesChange = category => {
  return {
    type: ON_CATEGORIES_CHANGE,
    payload: category,
  };
};

export const onSearchClear = () => {
  return {
    type: ON_SEARCH_CLEAR,
  };
};

export const onSearchTextChange = text => {
  return {
    type: ON_SEARCH_TEXT_CHANGE,
    payload: text,
  };
};

export const onFriendSearchTextChange = text => {
  return {
    type: ON_FRIEND_SEARCH_TEXT_CHANGE,
    payload: text,
  };
};
