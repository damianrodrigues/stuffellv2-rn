import {
  ON_ARCHIVE_MODAL_OPEN,
  ON_SELL_MODAL_OPEN,
  SET_ACRHIVE_MODE,
  SET_SELL_MODE,
} from './types';

export const onArchiveModalOpen = func => {
  return {
    type: ON_ARCHIVE_MODAL_OPEN,
    payload: typeof func === 'function' ? func : null,
  };
};

export const onSellModalOpen = func => {
  return {
    type: ON_SELL_MODAL_OPEN,
    payload: typeof func === 'function' ? func : null,
  };
};

export const setArchiveMode = mode => {
  return {
    type: SET_ACRHIVE_MODE,
    payload: mode,
  };
};

export const setSellMode = mode => {
  return {
    type: SET_SELL_MODE,
    payload: mode,
  };
};
