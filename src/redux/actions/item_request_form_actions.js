// @flow
import {
  ON_ITEM_REQUEST_FORM_FIELD_CHANGE,
  CLEAR_ITEM_REQUEST_FORM,
  SET_ACTIVE_ITEM_REQUEST,
} from './types';

type onItemRequestFormFieldChangeAction = {
  type: ON_ITEM_REQUEST_FORM_FIELD_CHANGE,
  payload: string,
};
type clearItemRequestFormAction = { type: CLEAR_ITEM_REQUEST_FORM };
type setActiveItemRequestAction = {
  type: SET_ACTIVE_ITEM_REQUEST,
  payload: Object,
};

export const onItemRequestFormFieldChange = (
  changedFields: string
): onItemRequestFormFieldChangeAction => ({
  type: ON_ITEM_REQUEST_FORM_FIELD_CHANGE,
  payload: changedFields,
});

export const clearItemRequestForm = (): clearItemRequestFormAction => ({
  type: CLEAR_ITEM_REQUEST_FORM,
});

export const setActiveItemRequest = (item: Object): setActiveItemRequestAction => ({
  type: SET_ACTIVE_ITEM_REQUEST,
  payload: item,
});
