// @flow
import { USER_LOGOUT } from './types';

export default () => ({
  type: USER_LOGOUT,
  payload: '',
});
