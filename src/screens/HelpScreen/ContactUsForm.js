// TOP LEVEL IMPORTS
import React from 'react';
import { graphql } from 'react-apollo';
import { View, Text, TextInput, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
// COMPONENTS
import LoadingIndicator from '../../components/common/LoadingIndicator';
import RegularButton from '../../components/common/RegularButton';
// LIB
import { styleConstants } from '../../lib/config';
// APOLLO
import { Support } from '../../apollo';

const HELP_MESSAGE_TOPICS_MODAL = [
  'Help or Question',
  'Feedback',
  'Feature Request',
  'Bug Report',
  'Report a Post',
  'Other',
];

class ContactUs extends React.Component {
  state = {
    loading: false,
    messageSent: false,
    messageValue: '',
  };
  onSubmit = () => {
    this.setState({ loading: true });
    let params = {
      messageValue: this.state.messageValue,
      topic: this.state.topic,
    };
    let variables = { params };
    this.props
      .createSupportMessage({ variables })
      .then(res => {
        Alert.alert('We recieved your message!!!');
        return this.props.navigation.goBack();
      })
      .catch(e => this.setState({ loading: false }));
  };
  render() {
    if (this.state.loading) {
      return <LoadingIndicator label={'Submitting your message...'} />;
    }

    return (
      <TouchableWithoutFeedback
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={() => Keyboard.dismiss()}>
        <View
          style={{
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            // flex: 1,
          }}>
          <View style={{ flex: 3 }}>
            <Text
              style={{
                marginTop: 45,
                fontFamily: styleConstants.fontFamily.regular,
                fontSize: 17,
              }}>
              How can we help you?
            </Text>
            <ModalDropdown
              defaultValue={HELP_MESSAGE_TOPICS_MODAL[0]}
              options={HELP_MESSAGE_TOPICS_MODAL}
              style={{ width: 320, marginTop: 5, marginBottom: 15, margin: 0 }}
              onSelect={topic => this.setState({ topic })}
            />
            <TextInput
              multiline
              blurOnSubmit
              style={{
                width: 320,
                minHeight: 150,
                margin: 0,
                backgroundColor: '#fff',
              }}
              value={this.state.messageValue}
              placeholder="Type your message here..."
              onChangeText={messageValue => this.setState({ messageValue })}
            />
            <RegularButton
              onPress={this.onSubmit}
              buttonStyle={{
                // left: -15,
                top: 10,
                width: 295,
                borderWidth: 0,
                borderRadius: 6,
                backgroundColor: styleConstants.color.primary,
              }}
              textStyle={{ color: '#fff' }}
              title="SEND MESSAGE"
            />
          </View>
          <View style={{ flex: 1 }} />
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default graphql(Support.Mutation.createSupportMessage, {
  name: 'createSupportMessage',
})(ContactUs);
