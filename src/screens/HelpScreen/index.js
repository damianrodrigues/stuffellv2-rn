import React from 'react';
import { KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native';
// LIB
import { UiHelpers } from '../../lib/helpers/UiHelpers';
// COMPONENTS
import ContactUsForm from './ContactUsForm';

class HelpScreen extends React.PureComponent {
  static navigationOptions = ({ navigation, screenProps }) =>
    UiHelpers.getBackNavOptions(navigation, screenProps, {
      title: 'Help & Support',
    });

  render() {
    return (
      <KeyboardAvoidingView
        keyboardVerticalOffset={65}
        behavior={'padding'}
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          flex: 1,
          backgroundColor: '#efefef',
        }}>
        <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss}>
          <ContactUsForm {...this.props} />
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

export default HelpScreen;
