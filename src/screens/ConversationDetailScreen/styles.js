import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  chatFormContainer: {
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'row',
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    left: 0,
    right: 0,
  },

  messageValue: {
    color: '#7b8b8e',
    fontSize: 13,
  },
  labelStyle: {
    fontSize: 18,
  },
  valueStyle: {
    fontSize: 18,
  },
  itemContainer: {
    height: 80,
    marginRight: 10,
    flex: 4,
  },
});
