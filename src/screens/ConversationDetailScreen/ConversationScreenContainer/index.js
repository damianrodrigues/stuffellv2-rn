import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';


const ConversationScreenContainer = ({children}) => {
  return (
    <KeyboardAvoidingView
        behavior={'padding'}
        keyboardVerticalOffset={64}
        contentContainerStyle={{
    //alignItems: 'stretch',
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    //paddingTop: 32,
  }}
        style={{
    //alignItems: 'stretch',
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    //paddingTop: 32,
  }}
      >
      {children}
    </KeyboardAvoidingView>
  )
}

export default ConversationScreenContainer;