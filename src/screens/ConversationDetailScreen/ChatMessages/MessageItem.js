// TOP LEVEL IMPORTS
import React from 'react';
import { StyleSheet, View, Image, ScrollView } from 'react-native';
// LIB
import { MomentHelpers } from '../../../lib/helpers';
import { stylesConfig, styleConstants } from '../../../lib/config';
// COMPONENTS
import { CaptionText, BodyText } from '../../../components/text/index';

const MessageItem = ({ message, user }) => {
  // setup the basic styles for every message, things like borderRadius, etc
  let baseMessageStyle = {
    position: 'relative',
    padding: 10,
    margin: 15,
    width: '65%',
    borderRadius: 9,
  };

  // setup defualt font style
  let fontStyle = {
    textAlign: 'left',
    fontSize: 17,
  };

  // setup style for when the message is from the current user
  let currentUserMessageStyle = {
    ...baseMessageStyle,
    alignSelf: 'flex-start',
    backgroundColor: '#d9d9d9',
  };

  // setup style for when the message iis from  the other user
  let otherUserMessageStyle = {
    ...baseMessageStyle,
    alignSelf: 'flex-end',
    backgroundColor: styleConstants.color.primary,
  };

  let otherUserFontStyle = {
    ...fontStyle,
    color: '#fff',
  };
  let currentUserFontStyle = {
    ...fontStyle,
    color: '#000',
  };

  // check if the message if from the current user, and apply a styling accordingly to the message body
  let messageStyle = message.ownerId === user._id ? currentUserMessageStyle : otherUserMessageStyle;

  // check if the message if from the current user, and apply a styling accordingly to the message font
  let textStyle = message.ownerId === user._id ? currentUserFontStyle : otherUserFontStyle;

  return (
    <View style={messageStyle}>
      <BodyText style={textStyle}>{message.messageValue}</BodyText>
      <CaptionText style={s.imessageTime}>
        {MomentHelpers.imessageTime(message.createdAt)}
      </CaptionText>
    </View>
  );
};

const s = StyleSheet.create({
  imessageTime: {
    fontSize: 13,
    backgroundColor: 'rgba(52, 52, 52, 0.0)',
    color: '#888888',
    position: 'absolute',
    bottom: -17,
    right: 0,
  },
});

export default MessageItem;
