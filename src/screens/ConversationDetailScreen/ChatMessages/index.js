// TOP LEVEL IMPORTS
import React from 'react';
import { graphql, compose } from 'react-apollo';
import { StyleSheet, View, ScrollView, WebView, Text } from 'react-native';
// COMPONENTS
import MessageItem from './MessageItem';
// APPOLLO
import { Conversation } from '../../../apollo/Conversation';

const MessageLoader = () => (
  <View style={{ height: 400, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
    <WebView
      source={{ uri: 'https://lottiefiles.com/iframe/890-loading-animation' }}
      style={{ height: 300, width: 300, margin: 20, borderColor: '#000', borderWidth: 2 }}
    />
  </View>
);

// EXPORTED COMPONENT
class ChatMessages extends React.Component {
  render() {
    const { data } = this.props;

    if (data.loading && data.networkStatus !== 2) {
      return <MessageLoader />;
    }

    if (!this.props.data.conversationById) {
      return (
        <View>
          <Text>We can't find this conversation!</Text>
        </View>
      );
    }

    if (this.props.data.conversationById.messages === 0) {
      return <View />;
    }

    return (
      <ScrollView
        style={s.chatMessagesContainer}
        ref={ref => (this.scrollView = ref)}
        onContentSizeChange={(contentWidth, contentHeight) =>
          this.scrollView.scrollToEnd({ animated: true })
        }>
        <View style={s.chatMessagesWrapper}>
          {this.props.data.conversationById.messages.map(message => (
            <MessageItem key={message._id} message={message} user={this.props.user} />
          ))}
        </View>
      </ScrollView>
    );
  }
}

// STYLES
const s = StyleSheet.create({
  chatMessagesContainer: {
    flex: 7,
  },
  chatMessagesWrapper: {
    flex: 1,
    paddingTop: 30,
    paddingBottom: 60,
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
  },
});

const options = props => {
  const variables = {
    _id: props.parentId,
  };
  return { variables, pollInterval: 5000 };
};

// EXPORT
export default compose(
  graphql(Conversation.Query.getMessagesByConversationId, {
    options,
  })
)(ChatMessages);
