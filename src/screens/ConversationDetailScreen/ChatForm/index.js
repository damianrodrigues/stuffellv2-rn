// @flow
import React from 'react';
import { View, StyleSheet, TextInput, Keyboard } from 'react-native';
import { Button } from 'react-native-elements';
import { graphql } from 'react-apollo';
import { styleConstants } from '../../../lib/config';
import { Message } from '../../../apollo/Message';
import { Conversation } from '../../../apollo/Conversation';

type Props = {
  navigation: Object,
  createMessage: Function,
};

const s = StyleSheet.create({
  buttonStyle: {
    // margin: 0,
    width: '100%',
    height: 40,
    padding: 0,
    marginRight: 40,
    borderRadius: 1,
    backgroundColor: styleConstants.color.primary,
    borderWidth: 1,
    borderColor: styleConstants.color.primary,
  },
});

class ChatForm extends React.PureComponent<Props> {
  state = {
    messageValue: '',
    loading: false,
  };

  onSubmit = () => {
    const { navigation } = this.props;
    const errors = [];
    this.setState({ loading: true, errors });

    // check to make sure message length is longer than two characters
    if (this.state.messageValue.length < 2) {
      const newError = 'Please type more than two characters...';
      errors.push(newError);
      return this.setState({ loading: false, errors });
    }

    const variables = {
      params: {
        parentId: navigation.state.params._id,
        messageValue: this.state.messageValue,
        parentModelType: 'postConversation',
      },
    };

    const refetchQueries = [
      {
        query: Conversation.Query.getMessagesByConversationId,
        variables: { _id: navigation.state.params._id },
      },
    ];

    this.props.createMessage({ variables, refetchQueries }).then(res => {
      this.setState({ loading: false, messageValue: '' });
      Keyboard.dismiss();
    });
  };

  render() {
    const { messageValue, loading } = this.state;

    return (
      <View
        style={{
          backgroundColor: '#fff',
          display: 'flex',
          flexDirection: 'row',
          flex: 1,
          paddingRight: 20,
        }}>
        <View style={{ flex: 6 }}>
          <TextInput
            autoFocus
            // multiline
            numberOfLines={1}
            editable
            style={{
              fontSize: 18,
              padding: 5,
              height: 40,
              borderTopWidth: 1,
              borderColor: '#efefef',
            }}
            onChangeText={messageValue => this.setState({ messageValue })}
            value={messageValue}
          />
        </View>
        <View style={{ flex: 2, justifyContent: 'flex-start', alignItems: 'flex-start' }}>
          <Button
            onPress={this.onSubmit}
            loading={loading}
            title={loading ? '' : 'Send'}
            containerViewStyle={{ margin: 0, width: '100%' }}
            buttonStyle={s.buttonStyle}
          />
        </View>
      </View>
    );
  }
}

export default graphql(Message.Mutation.createMessage, { name: 'createMessage' })(ChatForm);
