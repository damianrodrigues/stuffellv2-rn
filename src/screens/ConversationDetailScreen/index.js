// @flow
import * as React from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import { graphql, compose } from 'react-apollo';

// MODULES
import { UiHelpers } from '../../lib/helpers';
// APOLLO
import { Conversation } from '../../apollo/Conversation';
// COMPONENTS
import ChatMessages from './ChatMessages';
import ChatForm from './ChatForm';
import { styles } from './styles';

class ConversationDetailScreen extends React.Component {
  static navigationOptions = ({ navigation, screenProps }) =>
    UiHelpers.getBackNavOptions(navigation, screenProps, {
      title: navigation.state.params.name ? navigation.state.params.name : `Conversation`,
    });

  componentDidMount() {
    const { navigation, markConversationRead } = this.props;
    const conversationId = navigation.state.params._id;

    // set the conversation's _id as the _id in our mutation variables
    const variables = {
      _id: conversationId,
    };

    const optimisticResponse = {
      __typename: 'Mutation',
      markConversationRead: {
        _id: conversationId,
        __typename: 'Conversation',
        userHasRead: true,
      },
    };

    // setup refetch queries to run after the mutation
    const refetchQueries = [
      { query: Conversation.Query.conversations },
      { query: Conversation.Query.hasUnreadConversations },
      {
        query: Conversation.Query.conversationById,
        variables: { _id: conversationId },
      },
    ];

    markConversationRead({
      variables,
      optimisticResponse,
      refetchQueries,
    }).catch(e => console.log(e));
  }

  render() {
    const { navigation, screenProps } = this.props;

    return (
      <KeyboardAvoidingView
        behavior="padding"
        keyboardVerticalOffset={80}
        contentContainerStyle={{
          flex: 1,
          backgroundColor: '#fff',
          flexDirection: 'column',
        }}
        style={{
          flex: 1,
          backgroundColor: '#fff',
          flexDirection: 'column',
        }}>
        <View style={{ flex: 1 }}>
          <ChatMessages parentId={navigation.state.params._id} user={screenProps.data.user} />
        </View>
        <View style={styles.chatFormContainer}>
          <ChatForm {...this.props} />
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const options = ({ navigation }) => {
  const variables = { _id: navigation.state.params._id };
  return { variables };
};

export default compose(
  // ========================================
  // mark the conversation for this page as read. usually you fire this when they leave the sceen.
  graphql(Conversation.Mutation.markConversationRead, {
    name: 'markConversationRead',
  }),
  // ========================================
  // grab this conversation, and all the messages along with it.
  graphql(Conversation.Query.conversationById, { options })
)(ConversationDetailScreen);
