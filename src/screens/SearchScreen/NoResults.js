import React from 'react';
import { WebView, View } from 'react-native';

const s = {
  container: {
    justifyContent: 'center',
    height: 400,
    width: 400,
  },
  webview: {
    height: 290,
    width: 300,
    alignContent: 'center',
    alignSelf: 'center',
    margin: 20,
    borderColor: '#000',
    borderWidth: 2,
  },
};

// does webview not exist?
const NoResults = () => (
  <View style={s.container}>
    <WebView source={{ uri: 'https://lottiefiles.com/iframe/873-camera' }} style={s.webview} />
  </View>
);

export default NoResults;
