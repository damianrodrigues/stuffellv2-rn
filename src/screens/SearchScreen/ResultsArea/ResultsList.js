// @flow
import * as React from 'react';
import { View, ActivityIndicator, FlatList } from 'react-native';
// HELPERS
import { UiHelpers } from '../../../lib/helpers';
import { styleConstants } from '../../../lib/config';
// COMPONENTS
import Card from '../../../components/common/Card';
import ItemRequestLink from '../../../components/common/ItemRequestLink';
// TYPES
type Props = {
  postsQuery: Function,
  postsCountQuery: Function,
  itemRequestsQuery: Function,
  itemRequestsCountQuery: Function,
  navigation: Object,
  screenProps: Object,
};

type State = {
  loadingMore: boolean,
  refreshing: boolean,
};

// EXPORTED COMPONENT
class ResultsList extends React.PureComponent<Props, State> {
  state = {
    loadingMore: false,
    refreshing: false,
  };

  onEndReached = () => {
    const { postsQuery, postsCountQuery, itemRequestsCountQuery, itemRequestsQuery } = this.props;
    const { posts } = postsQuery;
    const { itemRequests } = itemRequestsQuery;

    if (!posts || !itemRequests) {
      return null;
    }

    const currentPostsDataLength = postsQuery.posts.length;
    const currentItemRequestsDataLength = itemRequestsQuery.itemRequests.length;

    const inDBPostsDataLength = postsCountQuery.postsCount.count;
    const inDBItemRequestsDataLength = itemRequestsCountQuery.itemRequestsCount.count;

    if (
      currentPostsDataLength >= inDBPostsDataLength &&
      currentItemRequestsDataLength >= inDBItemRequestsDataLength
    ) {
      return this.setState({ loadingMore: false });
    }

    this.setState({ loadingMore: true });

    const postsVariables = {
      params: {
        skip: postsQuery.posts.length,
      },
      string: '',
    };
    const itemRequestsVariables = {
      params: {
        skip: itemRequestsQuery.itemRequests.length,
      },
      string: '',
    };

    postsQuery
      .fetchMore({
        postsVariables,
        updateQuery: (prev, { fetchMoreResult }) => {
          // Don't do anything if there weren't any new items
          if (!fetchMoreResult || fetchMoreResult.posts.length === 0) {
            return prev;
          }

          return {
            posts: prev.posts.concat(fetchMoreResult.posts),
          };
        },
      })
      .then(() => {
        itemRequestsQuery.fetchMore({
          itemRequestsVariables,
          updateQuery: (prev, { fetchMoreResult }) => {
            // Don't do anything if there weren't any new items
            if (!fetchMoreResult || fetchMoreResult.itemRequests.length === 0) {
              return prev;
            }

            return {
              posts: prev.itemRequests.concat(fetchMoreResult.itemRequests),
            };
          },
        });
      });
  };

  onRefresh = () => {
    const { postsQuery } = this.props;
    const { itemRequestsQuery } = this.props;
    postsQuery.refetch().then(() => itemRequestsQuery.refetch());
  };

  showLoadingMore = () => {
    const { loadingMore } = this.state;
    if (loadingMore) {
      return (
        <View
          style={{ height: 150, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return <View style={{ height: 50 }} />;
  };

  renderItem = (item: Object) => {
    const { navigation, screenProps } = this.props;
    return <Card item={item} navigation={navigation} user={screenProps.data.user} />;
  };

  render() {
    const { refreshing } = this.state;
    const { postsQuery, navigation, itemRequestsQuery } = this.props;
    const postsAndItemRequest = UiHelpers.mergeArrays(
      itemRequestsQuery.itemRequests,
      postsQuery.posts
    );
    const orderedList = UiHelpers.orderArrayByDate(postsAndItemRequest);
    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={refreshing ? [] : orderedList}
        keyExtractor={({ _id }) => _id}
        refreshing={postsQuery.networkStatus === 4}
        onRefresh={this.onRefresh}
        ItemSeparatorComponent={() => (
          <View style={{ height: 2, backgroundColor: styleConstants.color.screenBackgroundGrey }} />
        )}
        onEndReached={this.onEndReached}
        ListHeaderComponent={<ItemRequestLink navigation={navigation} />}
        ListFooterComponent={this.showLoadingMore}
        renderItem={({ item }) => this.renderItem(item)}
      />
    );
  }
}

export default ResultsList;
