import React from 'react';
import { View, Text } from 'react-native';
// COMPONENTS
import { graphql, compose } from 'react-apollo';
import BackToBrowseButton from '../BackToBrowseButton';
import MainBreadcrumb from '../MainBreadcrumb';
import NoResults from '../NoResults';
import ResultsList from './ResultsList';
import ItemRequestLink from '../../../components/common/ItemRequestLink';
// APOLLO
import { Post, ItemRequest } from '../../../apollo';
// EXPORTED APP
class ResultsArea extends React.PureComponent {
  renderHeader = () => {
    const { categories, searchText } = this.props;

    // if there is searchText in the search input, and there are no categories selected, than we
    // want to let the user get back to the search results via the BackToBrowseButton
    if (searchText && searchText.length > 0 && (!categories || categories.length === 0)) {
      return <BackToBrowseButton {...this.props} />;
    }

    // if there are no categories, we want to show this header to tell them they can click on a category to filter by that category
    if (!categories || categories.length === 0) {
      return (
        <Text
          style={{ color: '#666', marginLeft: 10, marginBottom: 15, marginTop: 15, fontSize: 17 }}>
          or Browse within a category:
        </Text>
      );
    }

    // if there are categories selected, we want to show them the MainBreadcrumb which will show them the category
    // they selected and will also let them click to get back to the default search (i.e. clears the search stuff in redux)
    return <MainBreadcrumb {...this.props} />;
  };

  render() {
    const { categories, searchText, postsQuery, navigation, itemRequestsQuery } = this.props;
    // if there are no categories selected and no text in the search input, than don't render results lists
    if (!categories || (categories.length === 0 && (!searchText || searchText.length === 0))) {
      return null;
    }

    // if we're not loading results, and posts do not exist, that means we have no results. So show the new results screen.
    const noResults = itemRequestsQuery.itemRequests.length === 0 && postsQuery.posts.length === 0;
    // return no results with the relevant header/breadcrumb above it
    if (noResults) {
      return (
        <View>
          {this.renderHeader()}
          <ItemRequestLink navigation={navigation} />
          <NoResults />
        </View>
      );
    }
    // return the results if they exist
    return (
      <View>
        <MainBreadcrumb {...this.props} />
        <ResultsList {...this.props} />
      </View>
    );
  }
}

// APOLLO OPTIONS
// ======================
const postOptions = props => {
  const { statuses, categories, searchText } = props;
  const params = {
    statuses,
    categories,
    searchText,
    skip: 0,
  };

  return {
    variables: { params },
  };
};
const itemRequestOptions = ({ searchText, categories }) => {
  const params = {
    searchText: searchText || categories[0],
    skip: 0,
  };
  return {
    variables: { params },
  };
};

// EXPORT
// ====================================
export default compose(
  graphql(Post.Query.posts, { options: postOptions, name: 'postsQuery' }),
  graphql(Post.Query.postsCount, { options: postOptions, name: 'postsCountQuery' }),
  graphql(ItemRequest.Query.itemRequests, {
    options: itemRequestOptions,
    name: 'itemRequestsQuery',
  }),
  graphql(ItemRequest.Query.itemRequestsCount, {
    options: itemRequestOptions,
    name: 'itemRequestsCountQuery',
  })
)(ResultsArea);
