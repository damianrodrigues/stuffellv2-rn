import React from 'react';
import { Text, Keyboard, StyleSheet } from 'react-native';

const BackToBrowse = ({ categories, onSearchClear, searchText }) => {
  return (
    <Text style={s.container}>
      <Text
        onPress={() => {
          Keyboard.dismiss();
          onSearchClear();
        }}>
        Back to Search/Browse
      </Text>
    </Text>
  );
};

const s = StyleSheet.create({
  container: {
    color: '#666',
    marginLeft: 10,
    marginBottom: 15,
    marginTop: 15,
    fontSize: 17,
  },
});

export default BackToBrowse;
