// @flow
import * as React from 'react';
import { View, StyleSheet, Keyboard } from 'react-native';
// LIB
import { styleConstants } from '../../lib/config';
// COMPONENTS
import { CaptionText } from '../../components/text';
// STYLES
const s = StyleSheet.create({
  container: {
    margin: 10,
  },
});
// TYPES
type Props = {
  onSearchClear: Function,
  categories: Array<string>,
  searchText: string,
};
// EXPORTED COMPONENT
class MainBreadcrumb extends React.PureComponent<Props> {
  onClear = () => {
    const { onSearchClear } = this.props;
    Keyboard.dismiss();
    onSearchClear();
  };

  render() {
    const { categories, searchText } = this.props;

    return (
      <View style={s.container}>
        <CaptionText black>
          <CaptionText
            black
            style={{ fontWeight: styleConstants.fontWeight.bold }}
            onPress={this.onClear}>
            {' '}
            Categories{' '}
          </CaptionText>
          {categories[0] && (
            <CaptionText black>
              {' '}
              <CaptionText black>&gt;</CaptionText>
              <CaptionText black> {categories[0]} </CaptionText>
            </CaptionText>
          )}
          {searchText &&
            searchText.length > 0 && (
              <CaptionText black>
                {' '}
                <CaptionText black>&gt;</CaptionText>
                <CaptionText black> {`"${searchText}"`} </CaptionText>
              </CaptionText>
            )}
        </CaptionText>
      </View>
    );
  }
}

export default MainBreadcrumb;
