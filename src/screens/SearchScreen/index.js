// @flow
import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { SearchBar } from 'react-native-elements';
// REDUX
import { connect } from 'react-redux';
import * as actions from '../../redux/actions';
// COMPONENTS
import ResultsArea from './ResultsArea';
import CategoriesList from './CategoriesList';
// HELPERS
import { UiHelpers } from '../../lib/helpers';
// STYLES
const s = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff' },
});
// EXPORTED COMPONENT
class SearchScreen extends React.PureComponent {
  static navigationOptions = ({ navigation, screenProps }) =>
    UiHelpers.getHomeNavOptions(navigation, screenProps, {
      title: 'Search for Stuff',
    });

  onSearchTextChange = val => {
    const { onSearchTextChange } = this.props;
    onSearchTextChange(val);
  };

  render() {
    const {
      searchText,
      categories,
      onCategoriesChange,
      statuses,
      onSearchClear,
      screenProps,
      navigation,
    } = this.props;
    return (
      <View style={s.container}>
        <SearchBar
          onChangeText={this.onSearchTextChange}
          inputStyle={{ backgroundColor: '#fff' }}
          placeholder="Search..."
          value={searchText}
          lightTheme
        />
        <CategoriesList
          categories={categories}
          onCategoriesChange={onCategoriesChange}
          searchText={searchText}
          navigation={navigation}
        />
        <ResultsArea
          statuses={statuses}
          onSearchClear={onSearchClear}
          categories={categories}
          searchText={searchText}
          screenProps={screenProps}
          navigation={navigation}
        />
      </View>
    );
  }
}
const mapStateToProps = ({ search }) => ({
  statuses: search.statuses,
  categories: search.categories,
  searchText: search.searchText,
});

export default connect(mapStateToProps, actions)(SearchScreen);
