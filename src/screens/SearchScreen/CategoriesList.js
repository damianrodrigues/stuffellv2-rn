import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { width } from 'react-native-dimension';

import { CATEGORY_OPTIONS } from '../../lib/config';

import ItemRequestLink from '../../components/common/ItemRequestLink';

const CategoryItem = ({ onItemPress, item }) => (
  <Text
    style={{
      marginTop: 5,
      fontSize: width(5),
      color: 'rgba(0, 123, 181, 0.75)',
    }}
    key={item}
    onPress={onItemPress}>
    {item}
  </Text>
);

class CategoriesList extends React.PureComponent {
  onItemPress = item => {
    const { onCategoriesChange } = this.props;
    onCategoriesChange(item);
  };

  render() {
    const { categories, searchText, navigation } = this.props;

    // if there is no search text, then return null
    if (searchText && searchText.length > 0) {
      return null;
    }

    if (categories && categories.length > 0) {
      return null;
    }

    return (
      <ScrollView>
        <ItemRequestLink navigation={navigation} />
        <View style={{ padding: 10, paddingBottom: 60 }}>
          {CATEGORY_OPTIONS.map(item => (
            <CategoryItem key={item} item={item} onItemPress={() => this.onItemPress(item)} />
          ))}
        </View>
      </ScrollView>
    );
  }
}

export default CategoriesList;
