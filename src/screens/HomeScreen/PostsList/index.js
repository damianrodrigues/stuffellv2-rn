// @flow
import * as React from 'react';
import { ActivityIndicator, View, FlatList, Text, Dimensions } from 'react-native';
// CONSTANTS
import { styleConstants } from '../../../lib/config/styleConstants';
// COMPONENTS
import Card from '../../../components/common/Card';
import LoadingIndicator from '../../../components/common/LoadingIndicator';

type State = {
  loadingMore: boolean,
};

type Props = {
  postsQuery: Object,
  screenProps: Object,
  navigation: Object,
};

class PostsList extends React.Component<Props, State> {
  state = {
    loadingMore: false,
  };

  onEndReached = () => {
    const { loadingMore } = this.state;
    const { postsQuery, screenProps } = this.props;
    if (!postsQuery.posts) {
      return null;
    }

    if (loadingMore) {
      return console.log('loadingMore');
    }

    this.setState({ loadingMore: true });

    const params = {
      skip: postsQuery.posts.length,
      userId: screenProps.data.user._id,
    };

    const variables = {
      params,
    };

    postsQuery
      .fetchMore({
        variables,
        updateQuery: (prev, { fetchMoreResult }) => {
          // Don't do anything if there weren't any new items
          console.log('fetchMoreResult', fetchMoreResult);
          if (!fetchMoreResult || fetchMoreResult.posts.length === 0) {
            return prev.posts;
          }

          return {
            // Append the new feed results to the old one
            posts: [...prev.posts, ...fetchMoreResult.posts], // previousResult.tickets.concat(fetchMoreResult.tickets),
          };
        },
      })
      .then(() => this.setState({ loadingMore: false }));
  };

  showLoadingMore = () => {
    const { loadingMore } = this.state;
    if (loadingMore) {
      return (
        <View
          style={{
            height: 150,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return <View style={{ height: 50 }} />;
  };

  renderItem = (item, index) => {
    const { screenProps, navigation } = this.props;
    return (
      <Card
        key={item._id}
        user={screenProps.data.user}
        item={item}
        navigation={navigation}
        index={index}
        {...this.props}
      />
    );
  };

  renderEmptyComponent = () => (
    <View style={{ flex: 1 }}>
      <Text style={{ alignSelf: 'center', marginTop: 30 }}>No Posts....</Text>
    </View>
  );

  render() {
    const { loadingMore } = this.state;
    const { postsQuery } = this.props;
    const { height } = Dimensions.get('window');

    if (postsQuery.loading && !loadingMore) {
      return (
        <View
          style={{
            flex: 1,
            height: height - 80,
            width: '100%',
          }}>
          <LoadingIndicator label="Loading Posts..." />
        </View>
      );
    }

    return (
      <FlatList
        data={postsQuery.posts}
        keyExtractor={item => item._id}
        // scrollEnabled={false}
        // onEndReached={this.onEndReached}
        onEndReachedThreshold={1}
        disableVirtualization={false}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={this.renderEmptyComponent()}
        refreshing={postsQuery.networkStatus === 4}
        ItemSeparatorComponent={() => (
          <View style={{ height: 2, backgroundColor: styleConstants.color.screenBackgroundGrey }} />
        )}
        onRefresh={() => postsQuery.refetch()}
        ListFooterComponent={this.showLoadingMore()}
        removeClippedSubviews={false}
        renderItem={({ item, index }) => this.renderItem(item, index)}
        style={{ flex: 1 }}
      />
    );
  }
}

export default PostsList;
