import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import PostsList from '../index';
import data from './data';

const mocks = [data.postsQuery, data.postsCount];

describe('PostsList views', () => {
  it('renders PostsList', async () => {
    const instance = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <PostsList
          store={configureStore()({})}
          postsQuery={data.postsQuery.result.posts}
          screenProps={{
            data: {
              user: {
                _id: 'TgcTAeWywrrtKBamQ',
                emails: [
                  {
                    address: 'bugdonepavlov@gmail.com',
                    verified: false,
                    __typename: 'Email',
                  },
                ],
                roles: null,
                numberOfFollowers: 3,
                numberFollowing: 3,
                numberOfPosts: 3,
                following: 'none',
                follow: null,
                profile: {
                  firstName: 'Bogdan',
                  lastName: 'pavlov',
                  image: null,
                  description: null,
                  __typename: 'Profile',
                },
                __typename: 'User',
              },
            },
          }}
        />
      </MockedProvider>
    );

    await new Promise(resolve => setTimeout(resolve));
    instance.update();

    expect(instance.find('PostsList').exists()).toBeTruthy();
    expect(instance.find('FlatList').exists()).toBeTruthy();

    expect(instance).toMatchSnapshot();
  });
});
