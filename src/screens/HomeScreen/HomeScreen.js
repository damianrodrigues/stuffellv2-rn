// @flow
import * as React from 'react';
import { View, Image } from 'react-native';
import { appConfig } from '../../lib/config';
import { UiHelpers, StyleHelpers } from '../../lib/helpers';
import Tabs from './Tabs';

type Props = {
  navigation: Object,
  screenProps: Object,
};

type State = {
  refreshing: boolean,
  loadingMore: boolean,
};

class HomeScreen extends React.PureComponent<Props, State> {
  static navigationOptions = ({ navigation, screenProps }: Props) =>
    UiHelpers.getHomeNavOptions(navigation, screenProps, {
      headerTitle: (
        <Image
          source={appConfig.images.whiteLogo}
          style={{
            height: StyleHelpers.normalizeWidth(33),
            width: StyleHelpers.normalizeWidth(90),
          }}
        />
      ),
    });

  // Контейнер для табов, удалил все что было из этого компонента
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Tabs {...this.props} />
      </View>
    );
  }
}

export default HomeScreen;
