// @flow
import { StyleSheet, Dimensions } from 'react-native';
import { width, height } from 'react-native-dimension';
import { styleConstants } from '../../../lib/config';
import { StyleHelpers } from '../../../lib/helpers';

const { color } = styleConstants;

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '25%',
  },
  regularButton: {
    width: width(38),
    borderWidth: 2,
    height: height(3),
    backgroundColor: '#fff',
  },
  textStyle: {
    fontSize: height(2.2),
    color: '#000',
  },
  tabsContent: {
    flex: 1,
    backgroundColor: color.screenBackground,
  },
  tabTitle: {
    fontFamily: styleConstants.fontFamily.regular,
    fontSize: styleConstants.fontSize.md,
  },
  tabTitleContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  notification: {
    marginRight: 5,
    width: StyleHelpers.normalizeWidth(8),
    height: StyleHelpers.normalizeWidth(8),
    borderRadius: StyleHelpers.normalizeWidth(4),
    backgroundColor: 'red',
  },
});

export const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};
