import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';

import * as actions from '../../../redux/actions';
import { Post, ItemRequest } from '../../../apollo';

import Tabs from './Tabs';

export default compose(
  graphql(ItemRequest.Query.itemRequests, {
    options: () => ({
      variables: {
        params: {
          offset: 100,
        },
      },
      pollInterval: 600000,
    }),
    name: 'itemRequestsQuery',
  }),
  graphql(ItemRequest.Query.itemRequestsCount, {
    name: 'itemRequestsQueryCount',
  }),
  graphql(Post.Query.posts, {
    name: 'postsQuery',
    options: () => ({
      variables: {
        params: {
          offset: 100,
        },
      },
      notifyOnNetworkStatusChange: true,
      fetchPolicy: 'network-only',
      pollInterval: 600000,
    }),
  }),
  graphql(Post.Query.postsCount, {
    name: 'postsCount',
    options: () => ({
      notifyOnNetworkStatusChange: true,
      fetchPolicy: 'network-only',
    }),
  }),
  connect(null, actions)
)(Tabs);
