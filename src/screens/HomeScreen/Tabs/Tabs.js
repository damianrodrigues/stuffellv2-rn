// @flow
import * as React from 'react';
import { View, Animated } from 'react-native';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';
import WebViewComponent from '../../../components/common/WebViewComponent';
import { styleConstants } from '../../../lib/config';
import PostsList from '../PostsList';
import RequestList from '../RequestList';
import s, { initialLayout } from './styles';

type Props = {
  modals: Object,
  navigation: Object,
  screenProps: Object,
  tintColor: string,
  onArchiveModalOpen: Function,
  setArchiveMode: Function,
  onSellModalOpen: Function,
  setSellMode: Function,
};

type State = {
  tabNavigationState: Object,
};

export default class Tabs extends React.PureComponent<Props, State> {
  state = {
    tabNavigationState: {
      index: 0,
      routes: [{ key: 'first', title: 'Stuff' }, { key: 'second', title: 'Requests' }],
    },
  };

  componentDidMount() {
    const { navigation } = this.props;
    if (navigation.state.params && navigation.state.params.toMyRequests) {
      this.setState(prevState => ({
        tabNavigationState: {
          routes: prevState.tabNavigationState.routes,
          index: 1,
        },
      }));
    }
  }

  // ============== Tab Handlers ==============
  handleIndexChange = (index: number) => {
    const { tabNavigationState } = this.state;
    this.setState({
      tabNavigationState: {
        ...tabNavigationState,
        index,
      },
    });
  };

  // ============== Renders Tab Content (Scenes) ==============
  renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return (
          <View style={[s.container, { backgroundColor: '#fff' }]}>
            <PostsList {...this.props} />{' '}
          </View>
        );

      case 'second':
        return (
          <View style={[s.container, { backgroundColor: '#fff' }]}>
            <RequestList {...this.props} />
          </View>
        );

      default:
        return null;
    }
  };

  // ============== Render Tab Headers ==============
  renderHeader = props => (
    <TabBar
      indicatorStyle={{
        backgroundColor: styleConstants.color.primary,
      }}
      style={{
        backgroundColor: styleConstants.color.screenBackground,
        height: 45,
      }}
      tabStyle={{
        height: 45,
      }}
      renderLabel={this.renderLabel(props)} // renderer of a single Lable on the tab bar
      {...props}
    />
  );

  // ============== Render Tab labels ==============
  renderLabel = () => ({ route }) => {
    const { itemRequestsQuery, postsQuery } = this.props;

    // хелперы что есть ли непрочитанные айтемы
    const hasUnseenPosts =
      !!postsQuery.posts && postsQuery.posts.some(el => el.currentUserHasSeen === false);
    const hasUnseenRequests =
      !!itemRequestsQuery.itemRequests &&
      itemRequestsQuery.itemRequests.some(el => el.currentUserHasSeen === false);

    const color =
      (route.key === 'first' && hasUnseenPosts ? '#ff5500' : null) ||
      (route.key === 'second' && hasUnseenRequests ? '#ff5500' : null);

    // Добавления марки на таб
    return (
      <View style={s.tabTitleContent}>
        {hasUnseenPosts && route.key === 'first' && <View style={s.notification} />}
        {hasUnseenRequests && route.key === 'second' && <View style={s.notification} />}
        <Animated.Text style={[s.tabTitle, { color }]}>{route.title}</Animated.Text>
      </View>
    );
  };

  render() {
    const { tabNavigationState } = this.state;
    const { postsQuery, itemRequestsQuery, navigation } = this.props;

    return (
      <React.Fragment>
        <TabViewAnimated
          style={s.container}
          navigationState={tabNavigationState}
          renderScene={this.renderScene}
          renderHeader={this.renderHeader}
          onIndexChange={this.handleIndexChange}
          initialLayout={initialLayout}
          useNativeDriver
          {...this.props}
        />

        {itemRequestsQuery.itemRequests &&
          itemRequestsQuery.itemRequests.length === 0 &&
          postsQuery.posts &&
          postsQuery.posts.length === 0 && (
            <View
              style={{
                height: '100%',
                backgroundColor: styleConstants.color.screenBackground,
                position: 'relative',
                zIndex: 9999,
              }}>
              <WebViewComponent
                animationName="camera"
                title="Add Friends to start seeing stuff"
                buttonTitle="Go to Friends"
                onPress={() => navigation.navigate('friends')}
              />
            </View>
          )}
      </React.Fragment>
    );
  }
}
