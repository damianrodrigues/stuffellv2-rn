// @flow
import * as React from 'react';
import { compose, graphql } from 'react-apollo';
import { ActivityIndicator, View, FlatList, Text, Dimensions } from 'react-native';
import { NavigationEvents } from 'react-navigation';
// HELPERS
import { styleConstants } from '../../../lib/config/styleConstants';
// APPOLLO
import { ItemRequest } from '../../../apollo/ItemRequest';
// COMPONENTS
import Card from '../../../components/common/Card';
import LoadingIndicator from '../../../components/common/LoadingIndicator';
import ItemRequestLink from '../../../components/common/ItemRequestLink';

type State = {
  loadingMore: boolean,
};

type Props = {
  itemRequestsQuery: Object,
  screenProps: Object,
  navigation: Object,
};

class RequestList extends React.PureComponent<Props, State> {
  state = {
    loadingMore: false,
  };

  onEndReached = () => {
    const { loadingMore } = this.state;
    const { itemRequestsQuery, screenProps } = this.props;
    if (!itemRequestsQuery.itemRequests) {
      return null;
    }

    if (loadingMore) {
      return console.log('loadingMore');
    }

    this.setState({ loadingMore: true }, () => {
      const params = {
        skip: itemRequestsQuery.itemRequests.length,
        userId: screenProps.data.user._id,
      };

      const variables = {
        params,
      };

      itemRequestsQuery
        .fetchMore({
          variables,
          updateQuery: (prev, { fetchMoreResult }) => {
            // Don't do anything if there weren't any new items
            if (!fetchMoreResult || fetchMoreResult.itemRequests.length === 0) {
              return prev.itemRequests;
            }

            return {
              // Append the new feed results to the old one
              itemRequests: [...prev.itemRequests, ...fetchMoreResult.itemRequests], // previousResult.tickets.concat(fetchMoreResult.tickets),
            };
          },
        })
        .then(() => this.setState({ loadingMore: false }));
    });
  };

  showLoadingMore = () => {
    const { loadingMore } = this.state;
    if (loadingMore) {
      return (
        <View
          style={{
            height: 150,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return <View style={{ height: 50 }} />;
  };

  // функция для всех элементов которые не прочитаны
  handleInSeenRequest = () => {
    const { markItemRequestSeen, itemRequestsQuery: { itemRequests } } = this.props;
    const filtredInSeenItems = itemRequests.filter(e => e.currentUserHasSeen === false);

    filtredInSeenItems.forEach(item => {
      const { _id } = item;
      const variables = {
        _id,
      };

      const refetchQueries = [
        { query: ItemRequest.Query.itemRequests },
        {
          query: ItemRequest.Query.itemRequestById,
          variables,
        },
      ];

      markItemRequestSeen({
        variables,
        refetchQueries,
      }).catch(e => console.log(e));
    });
  };

  renderItem = (item, index) => {
    const { screenProps, navigation } = this.props;
    return (
      <Card
        key={item._id}
        user={screenProps.data.user}
        item={item}
        index={index}
        navigation={navigation}
        {...this.props}
      />
    );
  };

  renderEmptyComponent = () => (
    <View style={{ flex: 1 }}>
      <Text style={{ alignSelf: 'center', marginTop: 30 }}>No Requests....</Text>
    </View>
  );

  render() {
    const { loadingMore } = this.state;
    const { itemRequestsQuery, navigation } = this.props;
    const { height } = Dimensions.get('window');

    if (itemRequestsQuery.loading && !loadingMore) {
      return (
        <View style={{ flex: 1, height: height - 80 }}>
          <LoadingIndicator label="Loading Requests..." />
        </View>
      );
    }

    const sortItemRequests =
      itemRequestsQuery.itemRequests &&
      itemRequestsQuery.itemRequests.sort(
        (a, b) => (Date.parse(a.createdAt) > Date.parse(b.createdAt) ? -1 : 1)
      );

    return (
      <View>
        {/* костыль для не прочитанных requests item, когда пользователь уходить со вкладки помечается что было прочитанно */}
        <NavigationEvents onWillBlur={this.handleInSeenRequest} />
        <FlatList
          ListHeaderComponent={<ItemRequestLink navigation={navigation} />}
          data={sortItemRequests}
          keyExtractor={item => item._id}
          // onEndReached={this.onEndReached}
          // scrollEnabled={false}
          onEndReachedThreshold={1}
          disableVirtualization={false}
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={() => (
            <View
              style={{ height: 2, backgroundColor: styleConstants.color.screenBackgroundGrey }}
            />
          )}
          ListEmptyComponent={this.renderEmptyComponent()}
          refreshing={itemRequestsQuery.networkStatus === 4}
          onRefresh={() => itemRequestsQuery.refetch()}
          ListFooterComponent={this.showLoadingMore()}
          removeClippedSubviews={false}
          renderItem={({ item, index }) => this.renderItem(item, index)}
        />
      </View>
    );
  }
}

export default compose(
  graphql(ItemRequest.Mutation.markItemRequestSeen, {
    name: 'markItemRequestSeen',
  })
)(RequestList);
