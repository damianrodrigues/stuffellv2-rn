import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { shallow } from 'enzyme';
import { withNavigation } from 'react-navigation';
import RequestList from '../index';
import data from './data';

const mocks = [data.itemRequestsQuery, data.itemRequestsQueryCount];

describe('RequestList views', () => {
  it('renders RequestList', async () => {
    const instance = shallow(
      <MockedProvider mocks={mocks} addTypename={false}>
        <RequestList
          itemRequestsQuery={data.itemRequestsQuery.result.itemRequests}
          screenProps={{
            data: {
              user: {
                _id: 'TgcTAeWywrrtKBamQ',
                emails: [
                  {
                    address: 'bugdonepavlov@gmail.com',
                    verified: false,
                    __typename: 'Email',
                  },
                ],
                roles: null,
                numberOfFollowers: 3,
                numberFollowing: 3,
                numberOfPosts: 3,
                following: 'none',
                follow: null,
                profile: {
                  firstName: 'Bogdan',
                  lastName: 'pavlov',
                  image: null,
                  description: null,
                  __typename: 'Profile',
                },
                __typename: 'User',
              },
            },
          }}
        />
      </MockedProvider>
    );

    await new Promise(resolve => setTimeout(resolve));
    instance.update();

    expect(instance).toBeTruthy();

    expect(instance).toMatchSnapshot();
  });
});
