import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import HomeScreen from '../index';
import Tabs from '../Tabs';
import data from './data';

const mocks = [
  data.itemRequestsQuery,
  data.itemRequestsQueryCount,
  data.postsQuery,
  data.postsCount,
];
const instance = shallow(
  <MockedProvider mocks={mocks} addTypename={false}>
    <HomeScreen
      store={configureStore()({})}
      screenProps={{
        data: {
          user: {
            _id: 'TgcTAeWywrrtKBamQ',
            emails: [
              {
                address: 'bugdonepavlov@gmail.com',
                verified: false,
                __typename: 'Email',
              },
            ],
            roles: null,
            numberOfFollowers: 3,
            numberFollowing: 3,
            numberOfPosts: 3,
            following: 'none',
            follow: null,
            profile: {
              firstName: 'Bogdan',
              lastName: 'pavlov',
              image: null,
              description: null,
              __typename: 'Profile',
            },
            __typename: 'User',
          },
        },
      }}>
      <Tabs
        itemRequestsQuery={data.itemRequestsQuery.result.itemRequests}
        postsQuery={data.postsQuery.result.posts}
      />
    </HomeScreen>
  </MockedProvider>
);

describe('Tabs views', () => {
  it('renders Tabs', async () => {
    await new Promise(resolve => setTimeout(resolve));
    instance.update();

    expect(instance.exists()).toBeTruthy();

    expect(instance).toMatchSnapshot();
  });
});
