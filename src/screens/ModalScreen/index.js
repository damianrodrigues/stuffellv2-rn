import React from 'react';
import { View, Text, KeyboardAvoidingView } from 'react-native';
import { stylesConfig } from '../../lib/config';
//
import BackButton from '../../components/common/BackButton';
import PostForm from '../../components/forms/PostForm';
import NewMessageForm from '../../components/forms/NewMessageForm';
import ItemRequestForm from '../../components/forms/ItemRequestForm';

// THIS IS A RE-USABLE MODAL SCREEN,
// WE CAN PASS IN PARAMS AND SWITCH BETWEEN DIFFERENT PAGE TITLES, ETC>

const getModalNavOptions = (navigation, screenProps) => {
  const defaults = {
    headerTitleStyle: stylesConfig.titleStyle,
    headerStyle: stylesConfig.basicHeaderStyle,
    tabBarVisible: false,
    headerTitleAllowFontScaling: false,
    mode: 'modal',
    transitionConfig: {
      isModal: true,
    },
    headerLeft: <BackButton onPress={() => navigation.goBack(null)} label="Cancel" />,
  };

  if (navigation.state.params.modalType === 'newPost') {
    return {
      ...defaults,
      title: 'Post Some Stuff',
    };
  }

  if (navigation.state.params.modalType === 'editPost') {
    return {
      ...defaults,
      title: 'Edit Item',
      headerLeft: (
        <BackButton
          onPress={() => {
            // clear the post form contents so the currently
            // editing item is not there next time the modal opens
            screenProps.clearPostForm();
            navigation.goBack(null);
          }}
          label="Cancel"
        />
      ),
    };
  }

  if (navigation.state.params.modalType === 'newMessage') {
    return {
      ...defaults,
      title: 'Send a message',
    };
  }

  if (navigation.state.params.modalType === 'itemRequest') {
    return {
      ...defaults,
      title: 'Request an Item',
    };
  }

  if (navigation.state.params.modalType === 'editItemRequest') {
    return {
      ...defaults,
      title: 'Edit Request',
    };
  }

  return defaults;
};

const EmptyView = () => (
  <KeyboardAvoidingView keyboardVerticalOffset={65} behavior="padding">
    <Text>EmptyView</Text>
  </KeyboardAvoidingView>
);

// EXPORTED COMPONENT
// ====================================
class ModalScreen extends React.PureComponent {
  static navigationOptions = ({ navigation, screenProps }) =>
    getModalNavOptions(navigation, screenProps);

  renderView = () => {
    const { navigation, screenProps } = this.props;
    // check what modalType was passed in, and show the relevant content
    switch (navigation.state.params.modalType) {
      case 'newPost': // this is called new post, but I suppose we could use it for edit too. maybe we can change it from newPost to postForm?
        return <PostForm {...this.props} />;
      case 'newMessage': // for adding a chat message (usually as the first in the conversation)
        return (
          <NewMessageForm
            item={navigation.state.params.item}
            name={navigation.state.params.name}
            navigation={navigation}
            screenProps={screenProps}
          />
        );
      case 'editPost': // for adding a chat message (usually as the first in the conversation)
        return (
          <PostForm
            item={navigation.state.params.item}
            updateForm // make sure we know this is an update form
            {...this.props}
          />
        );
      case 'itemRequest': // for adding a chat message (usually as the first in the conversation)
        return <ItemRequestForm item={navigation.state.params.item} {...this.props} />;
      case 'editItemRequest': // for adding a chat message (usually as the first in the conversation)
        return <ItemRequestForm updateForm item={navigation.state.params.item} {...this.props} />;
      default:
        return <EmptyView />;
    }
  };

  render() {
    return <View style={{ backgroundColor: '#efefef', flex: 1 }}>{this.renderView()}</View>;
  }
}

export default ModalScreen;
