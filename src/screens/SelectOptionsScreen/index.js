// TOP LEVEL IMPORTS
import React from 'react';
import { View, ScrollView, Text } from 'react-native';
// LIB
import { UiHelpers } from '../../lib/helpers';
// COMPONENTS
import CategoriesOptions from '../../components/inputs/CategoriesOptions';


// CONSTANTS & DESTRUCTURING
// ====================================


// LOCAL HELPERS
// ====================================
const getSelectTitle = (navigation) => {

    switch(navigation.state.params.selectType){
        case 'categories':
            return 'Select a Category'
        default:
            return 'Select'
    }

}

// This will hopefully be a reusable screen, 
// where you pass in some options that map to arrays being held in the redux searchSore.
// In a better world, we'd decouple this form the specific redux store (in this case, searchStore)
// But to use it, basically look up the name of the array 
// in redux 'searchSore' (right now, we have selectedIndustries and selectedStates, but we could have more).


// EXPORTED COMPONENT
// ====================================
class SelectOptionsScreen extends React.Component {
  // set navigationOptions
  static navigationOptions = ({ navigation, screenProps }) => 
  UiHelpers.getBackNavOptions(navigation, screenProps, {
    title: getSelectTitle(navigation),
    headerTitleAllowFontScaling: false,
  });

  renderOptions = () => {

    const { selectType } = this.props.navigation.state.params;

    if (selectType === 'categories') {
        return (
            <CategoriesOptions {...this.props} />
        );
    }

    // if there is no params saying which options to show, return null
    // TODO: maybe show a "something went wrong" screen/empty state, with a button to navigation.goBack()
    return null;
    
  }
  render() {
    return (
        <ScrollView style={{ flex: 1 }}>
          {this.renderOptions()}
        </ScrollView>
      );
  }
}



// EXPORT
// ====================================
export default SelectOptionsScreen;