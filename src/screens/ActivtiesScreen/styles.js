// @flow
import { StyleSheet } from 'react-native';
import { height, width } from 'react-native-dimension';

import { styleConstants } from '../../lib/config';
import { StyleHelpers } from '../../lib/helpers';

const { color } = styleConstants;

export default StyleSheet.create({
  container: {
    flex: 1,
    height: height(100),
    backgroundColor: color.screenBackground,
  },
  tabsContent: {
    flex: 1,
    backgroundColor: color.screenBackground,
  },
  tabTitle: {
    fontFamily: styleConstants.fontFamily.regular,
    fontSize: styleConstants.fontSize.md,
  },
  tabTitleContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  notification: {
    marginRight: 5,
    width: StyleHelpers.normalizeWidth(8),
    height: StyleHelpers.normalizeWidth(8),
    borderRadius: StyleHelpers.normalizeWidth(4),
    backgroundColor: 'red',
  },
});

export const initialLayout = {
  height: height(100),
  width: width(100),
};
