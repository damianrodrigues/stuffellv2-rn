import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';

import * as actions from '../../redux/actions';
import { Conversation } from '../../apollo';

import ActivityScreen from './ActivityScreen';

export default compose(
  graphql(Conversation.Query.hasUnreadConversations, {
    name: 'hasUnreadConversations',
  }),
  connect(null, actions)
)(ActivityScreen);
