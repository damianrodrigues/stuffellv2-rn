import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { mount } from 'enzyme';
import MessagesList from '../MessagesList';
import data from './data';

const mocks = [data];

describe('MessagesList views', () => {
  it('renders MessagesList', async () => {
    const instance = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MessagesList
          data={{ conversations: data.result.conversations }}
          screenProps={{
            data: {
              user: {
                _id: 'TgcTAeWywrrtKBamQ',
                emails: [
                  {
                    address: 'bugdonepavlov@gmail.com',
                    verified: false,
                    __typename: 'Email',
                  },
                ],
                roles: null,
                numberOfFollowers: 3,
                numberFollowing: 3,
                numberOfPosts: 3,
                following: 'none',
                follow: null,
                profile: {
                  firstName: 'Bogdan',
                  lastName: 'pavlov',
                  image: null,
                  description: null,
                  __typename: 'Profile',
                },
                __typename: 'User',
              },
            },
          }}
        />
      </MockedProvider>
    );

    await new Promise(resolve => setTimeout(resolve));
    instance.update();
    expect(instance.find('MessagesList').exists()).toBeTruthy();
    expect(instance.find('FlatList').exists()).toBeTruthy();

    expect(instance).toMatchSnapshot();
  });
});
