import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';

import * as actions from '../../../redux/actions';
import { Conversation } from '../../../apollo';

import MessagesList from './MessagesList';

const options = () => ({ pollInterval: 20000 });

export default compose(
  // grab a list of conversations to display
  graphql(Conversation.Query.conversations, { options }),
  connect(
    null,
    actions
  )
)(MessagesList);
