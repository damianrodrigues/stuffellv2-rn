// @flow
import * as React from 'react';
import { View, Text, FlatList } from 'react-native';

import LoadingIndicator from '../../../components/common/LoadingIndicator';
import ConversationItem from '../../../components/common/ConversationItem';

type Props = {
  screenProps: Object,
  data: Object,
  navigation: Object,
};

export default class MessagesList extends React.PureComponent<Props> {
  renderItem = (item: Object) => {
    const { navigation, screenProps } = this.props;
    const conversationType = item.post ? 'post' : 'itemRequest';
    return (
      <ConversationItem
        key={item._id}
        conversation={item}
        item={item}
        navigation={navigation}
        conversationType={conversationType}
        screenProps={screenProps}
      />
    );
  };

  renderEmptyComponent = () => (
    <View style={{ flex: 1 }}>
      <Text style={{ alignSelf: 'center', marginTop: 30 }}>No Messages....</Text>
    </View>
  );

  render() {
    const { data } = this.props;
    const { conversations } = data;
    if (data.loading) {
      return (
        <View style={{ flex: 1 }}>
          <LoadingIndicator label="Loading Posts..." />
        </View>
      );
    }

    return (
      <FlatList
        data={conversations}
        keyExtractor={item => item._id}
        onEndReached={() => {}}
        onEndReachedThreshold={1}
        disableVirtualization={false}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={this.renderEmptyComponent()}
        refreshing={data.networkStatus === 4}
        onRefresh={() => data.refetch()}
        removeClippedSubviews={false}
        renderItem={({ item }) => this.renderItem(item)}
      />
    );
  }
}
