// @flow
import * as React from 'react';
import { View } from 'react-native';
import MessagesList from './MessagesList';
import { UiHelpers } from '../../lib/helpers';
import s from './styles';

type Props = {
  navigation: Object,
  screenProps: Object,
  data: Object,
};

type State = {
  tabNavigationState: Object,
};

export default class ActivityScreen extends React.PureComponent<Props, State> {
  static navigationOptions = ({ navigation, screenProps }: Props) =>
    UiHelpers.getHomeNavOptions(navigation, screenProps, {
      title: 'Messages',
    });

  render() {
    return (
      <View style={s.container}>
        <MessagesList {...this.props} />
      </View>
    );
  }
}
