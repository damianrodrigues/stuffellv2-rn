// TOP LEVEL IMPORTS
import React from 'react';
import { graphql, compose } from 'react-apollo';
import { ScrollView, TextInput, View, Alert, StyleSheet, Keyboard } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { loginWithPassword, changePassword } from 'meteor-apollo-accounts';
import { SecureStore } from 'expo';
// LIB
import { styleConstants } from '../../lib/config';
// COMPONENTS
import RegularButton from '../../components/common/RegularButton';
import AvatarEditor from './AvatarEditor';
import { CaptionText, BodyText } from '../../components/text';
// APOLLO
import { User } from '../../apollo';
import ApolloClient from '../../apollo/ApolloClient';
// LIB
import { UiHelpers } from '../../lib/helpers/UiHelpers';

class EditProfileScreen extends React.Component {
  static navigationOptions = ({ navigation, screenProps }) =>
    UiHelpers.getBackNavOptions(navigation, screenProps, {
      title: 'Edit Profile',
    });

  constructor(props) {
    super(props);
    const { user } = this.props.screenProps.data;
    this.state = {
      loading: false,
      firstName: user.profile.firstName || null,
      lastName: user.profile.lastName || null,
      email: user.emails[0].address || null,
      description: user.profile.description || null,
      image: user.profile.image || null,
    };
  }

  onSuccessfulUpload = image => {
    this.setState({ image });
  };

  onSaveAvatar = async () => {
    const { loading, user } = this.props.screenProps.data;
    const image = this.state.image;

    const optimisticResponse = {
      saveUserImage: {
        _id: user._id,
        __typename: 'User',
        'profile.image': image,
      },
    };

    const variables = { image };

    const refetchQueries = [{ query: User.Query.getCurrentUser }];
    this.props
      .saveUserImage({
        variables,
        refetchQueries,
        optimisticResponse,
      })
      .then(() => {
        this.setState({
          image: this.state.image,
          loading: false,
        });
      })
      .catch(e => {
        this.setState({ loading: false });
        console.log(e);
      });
  };

  onSuccessfulLogin = async (email, password) => {
    Keyboard.dismiss();
    await SecureStore.setItemAsync('email', email);
    await SecureStore.setItemAsync('password', password);
  };

  onChangePassword = async () => {
    const email = this.props.screenProps.data.user.emails[0].address;
    const { newPassword, oldPassword } = this.state;
    await changePassword({ newPassword, oldPassword }, ApolloClient);

    try {
      const id = await loginWithPassword({ password: newPassword, email }, ApolloClient);
      if (id) {
        Alert.alert('Password changed!');
        return this.onSuccessfulLogin(email, newPassword);
      }
    } catch (err) {
      return Alert.alert('Password was not changed!');
    }
  };

  onSaveProfile = () => {
    const { screenProps, saveUserAccount } = this.props;
    const { user } = screenProps.data;
    const { firstName, lastName, email, image, description, loading } = this.state;
    if (loading) {
      return;
    } // if already loading, return
    this.setState({ loading: true });
    const variables = {
      _id: user._id,
      params: {
        email,
        firstName,
        lastName,
        image,
        description,
      },
    };
    const refetchQueries = [{ query: User.Query.getCurrentUser }];

    saveUserAccount({ variables, refetchQueries })
      .then(() => {
        this.setState({ loading: false });
        Alert.alert('Profile saved!');
      })
      .catch(e => console.log(e));
  };

  render() {
    const { data } = this.props.screenProps;
    const { user } = data;

    return (
      <KeyboardAwareScrollView>
        <ScrollView
          contentContainerStyle={{
            backgroundColor: '#fff',
          }}>
          <View style={styles.mainContainer}>
            <AvatarEditor
              onSuccessfulUpload={this.onSuccessfulUpload}
              image={this.state.image}
              loading={this.state.loading}
              onSaveProfile={this.onSaveAvatar}
            />
          </View>
          <View style={styles.wrapper}>
            <CaptionText style={styles.captionText}>Edit your profile statement</CaptionText>
            <View style={styles.innerWrapper}>
              <View style={[styles.inputContainer, styles.inputContainerNoBorder]}>
                <TextInput
                  multiline
                  blurOnSubmit
                  style={[styles.inputField, styles.inputFieldDescription]}
                  placeholder="Profile statement"
                  maxLength={200}
                  onChangeText={description => this.setState({ description })}
                  defaultValue={
                    user.profile.description && user.profile.description.length > 200
                      ? user.profile.description.slice(0, 200)
                      : user.profile.description
                  }
                />
              </View>
              <RegularButton
                onPress={this.onSaveProfile}
                title="Save Changes"
                buttonStyle={styles.buttonStyle}
                textStyle={styles.buttonTextStyle}
                disabled={false}
              />
            </View>
            <CaptionText style={styles.captionText}>Change Your Password</CaptionText>
            <View style={styles.innerWrapper}>
              <View style={styles.inputContainer}>
                <BodyText>Old</BodyText>
                <TextInput
                  style={styles.inputField}
                  placeholder="Old password"
                  onChangeText={val => this.setState({ oldPassword: val })}
                />
              </View>
              <View style={styles.inputContainer}>
                <BodyText>New</BodyText>
                <TextInput
                  style={[styles.inputField, styles.inputFieldEmail]}
                  placeholder="New password"
                  onChangeText={val => this.setState({ newPassword: val })}
                />
              </View>
              <RegularButton
                onPress={this.onChangePassword}
                title="Update Password"
                buttonStyle={styles.buttonStyle}
                textStyle={styles.buttonTextStyle}
                disabled={false}
              />
            </View>
            <CaptionText style={styles.captionText}>Update name or email address</CaptionText>
            <View style={styles.innerWrapper}>
              <View style={styles.inputContainer}>
                <BodyText>First</BodyText>
                <TextInput
                  style={styles.inputField}
                  placeholder="First name"
                  onChangeText={val => this.setState({ firstName: val })}
                  defaultValue={user.profile.firstName}
                />
              </View>
              <View style={styles.inputContainer}>
                <BodyText>Last</BodyText>
                <TextInput
                  style={styles.inputField}
                  placeholder="Last name"
                  onChangeText={val => this.setState({ lastName: val })}
                  defaultValue={user.profile.lastName}
                />
              </View>
              <View style={[styles.inputContainer, styles.inputContainerNoBorder]}>
                <BodyText>Email</BodyText>
                <TextInput
                  style={[styles.inputField, styles.inputFieldEmail]}
                  placeholder="Email@gmail.com"
                  onChangeText={val => this.setState({ email: val })}
                  defaultValue={user.emails[0].address}
                />
              </View>
              <RegularButton
                onPress={this.onSaveProfile}
                title="Save Changes"
                buttonStyle={styles.buttonStyle}
                textStyle={styles.buttonTextStyle}
                disabled={false}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    minHeight: 85,
    display: 'flex',
    justifyContent: 'center',
  },
  wrapper: {
    flex: 4,
    borderWidth: 2,
    borderColor: '#efefef',
    backgroundColor: '#efefef',
  },
  innerWrapper: {
    backgroundColor: '#fff',
  },
  captionText: {
    backgroundColor: '#efefef',
    color: '#666',
    padding: 10,
  },
  buttonStyle: {
    marginBottom: 15,
    borderWidth: 0,
    display: 'flex',
    margin: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 33,
    marginTop: 3,
    flexDirection: 'row',
  },
  buttonTextStyle: {
    display: 'flex',
    fontSize: 14,
    flex: 2,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputContainer: {
    flexDirection: 'row',
    marginLeft: 10,
    padding: 10,
    paddingLeft: 0,
    borderColor: '#efefef',
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  inputContainerNoBorder: {
    borderBottomWidth: 0,
  },
  inputField: {
    width: '100%',
    marginLeft: 40,
    justifyContent: 'center',
    fontFamily: styleConstants.fontFamily.regular,
    textAlign: 'left',
    fontWeight: styleConstants.fontWeight.regular,
    fontSize: styleConstants.fontSize.md,
  },
  inputFieldEmail: {
    marginLeft: 32,
  },
  inputFieldDescription: {
    borderRadius: 20,
    marginLeft: 0,
    height: 150,
    marginBottom: 10,
    padding: 15,
    paddingTop: 15,
    borderWidth: 2,
    borderColor: '#efefef',
  },
});

// EXPORT
// ==============================
export default compose(
  graphql(User.Mutation.saveUserImage, {
    name: 'saveUserImage',
  }),
  graphql(User.Mutation.saveUserAccount, {
    name: 'saveUserAccount',
  })
)(EditProfileScreen);
