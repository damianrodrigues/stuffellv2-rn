// import configureStore from 'redux-mock-store';
import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { shallow } from 'enzyme';
import EditProfileScreen from '../index';

const instance = shallow(
  <MockedProvider mocks={{}} addTypename={false}>
    <EditProfileScreen
      screenProps={{
        data: {
          user: {
            _id: 'TgcTAeWywrrtKBamQ',
            emails: [
              {
                address: 'bugdonepavlov@gmail.com',
                verified: false,
                __typename: 'Email',
              },
            ],
            roles: null,
            numberOfFollowers: 3,
            numberFollowing: 3,
            numberOfPosts: 3,
            following: 'none',
            follow: null,
            profile: {
              firstName: 'Bogdan',
              lastName: 'pavlov',
              image: null,
              description: null,
              __typename: 'Profile',
            },
            __typename: 'User',
          },
        },
      }}
    />
  </MockedProvider>
);

describe('<EditProfileScreen />', () => {
  it('<EditProfileScreen /> should render', async () => {
    await new Promise(resolve => setTimeout(resolve));
    instance.update();
    expect(instance.exists()).toBe(true);

    expect(instance).toMatchSnapshot();
  });
});
