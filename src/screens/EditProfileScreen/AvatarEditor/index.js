// TOP LEVEL IMPORTS
import React from 'react';
import { Image, View, StyleSheet, ActivityIndicator, Text } from 'react-native';
import { ImagePicker, Permissions } from 'expo';
// LIB
import { width, height } from 'react-native-dimension';
import { UploadHelpers } from '../../../lib/helpers';
import { appConfig } from '../../../lib/config';
// COMPONENTS
import RegularButton from '../../../components/common/RegularButton';
import { StyleHelpers } from '../../../lib/helpers/StyleHelpers';
import ImageHelpers from '../../../lib/helpers/ImageHelpers';

const defaultImagePickerOptions = {
  mediaTypes: 'Images',
  allowsEditing: true,
  quality: 0.35, // we don't need high quality, it's a small picture and we don't want a 4gb profile picture to upload
  // TODO: use base64 to store a base64 version of the image so we can use it in previews, etc.
  // TODO: check the exif data... images are showing up sideways on the web.
};

class AvatarEditor extends React.PureComponent {
  state = {
    loading: false,
    hasCameraPermission: null,
    hasCameraRollPermission: null,
    showCamera: false,
  };

  askCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  askCameraRollPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({
      hasCameraRollPermission: status === 'granted',
    });
  };

  componentWillMount() {
    this.askCameraPermission();
    this.askCameraRollPermission();
  }

  onAfterImageSelected = async result => {
    this.setState({ loading: true });

    if (result.base64) {
      this.setState({ image: result.base64 });
    }

    let manipResult;

    try {
      manipResult = await ImageHelpers.makeAvatarSmaller(result.uri);
      console.log(manipResult);
    } catch (err) {
      console.log('error in onAfterImageSelected');
      console.log(JSON.stringify(err));
      return alert(err);
    }
    // next we manipulate the image

    return this.uploadImage(manipResult);
  };

  uploadImage = result => {
    // let _this = this // make sure we don't lose this

    // show a loader
    this.setState({ loading: true });
    console.log(result.uri);
    // upload the file to amazon s3
    UploadHelpers.handleFileUpload({ uri: result.uri }, (error, response) => {
      console.log(error);
      console.log(response);
      // check for an error
      if (error) {
        console.log(error);
        alert(error); // show an alert if there is an error
        return this.setState({ loading: false }); // remove the loader
      }
      // call onSuccessfulUpload with the response (response will be a string/url)
      this.props.onSuccessfulUpload(response);

      // remove the loader
      this.setState({ loading: false });
    });
  };

  onTakeNewPhoto = async () => {
    if (this.state.loading) {
      return null;
    }

    try {
      const result = await ImagePicker.launchCameraAsync(defaultImagePickerOptions);
      // if user does not cancel out of ImagePicker, then upload the image to amazon s3
      if (!result.cancelled) {
        return this.uploadImage(result);
      }
    } catch (err) {
      alert(err.message);
    }
  };

  onSelectFromLibrary = async () => {
    if (this.state.loading) {
      return null;
    }

    try {
      const result = await ImagePicker.launchImageLibraryAsync(defaultImagePickerOptions);

      // check if user did not exit the select library
      if (!result.cancelled) {
        return this.uploadImage(result);
      }
    } catch (err) {
      alert(err.message);
    }
  };

  onSaveChanges = () => {
    if (this.state.loading) {
      return null;
    }
    console.log('this.props.onSaveProfile()');
    this.props.onSaveProfile();
  };

  render() {
    const { loading, image } = this.props;

    // get the source for the image
    const source = image ? { uri: image } : appConfig.images.defaultAvatar;

    if (loading) {
      return <ActivityIndicator />;
    }

    return (
      <View style={s.container}>
        <View style={s.innerContainer}>
          {this.state.loading ? (
            <View>
              <Text>Uploading...</Text>
            </View>
          ) : (
            <View>
              <Image source={source} style={s.image} />
            </View>
          )}
        </View>
        <View style={s.buttonsContainer}>
          <RegularButton
            onPress={this.onTakeNewPhoto}
            title="Take New Photo"
            textStyle={s.topButtonsText}
            buttonStyle={s.topButtons}
            disabled={false}
          />
          <RegularButton
            onPress={this.onSelectFromLibrary}
            title="Select From Library"
            textStyle={s.topButtonsText}
            buttonStyle={s.topButtons}
            disabled={false}
          />
          <RegularButton
            onPress={this.onSaveChanges}
            title="Save Changes"
            buttonStyle={s.saveButton}
            textStyle={s.saveButtonsText}
            disabled={false}
          />
        </View>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainer: {
    flex: 1,
    alignItems: 'center',
  },
  buttonsContainer: {
    marginLeft: width(6),
    flex: 2,
    justifyContent: 'space-around',
    height: StyleHelpers.normalizeHeight(130),
  },
  topButtonsText: {
    fontSize: height(2.5),
    color: '#000',
  },
  saveButtonsText: {
    fontSize: height(2.5),
    color: '#fff',
  },
  topButtons: {
    width: width(50),
    borderWidth: 2,
    height: height(5.5),
    marginBottom: height(1),
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  saveButton: {
    width: width(50),
    borderWidth: 2,
    height: height(5.5),
    backgroundColor: '#007EB1',
  },
  image: {
    marginLeft: 30,
    width: StyleHelpers.normalizeWidth(90),
    height: StyleHelpers.normalizeWidth(90),
    borderRadius: StyleHelpers.normalizeWidth(45),
  },
});

export default AvatarEditor;
