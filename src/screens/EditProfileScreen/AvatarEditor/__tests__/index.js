import configureStore from 'redux-mock-store';
import React from 'react';
import { shallow } from 'enzyme';
import AvatarEditor from '../index.js';

const instance = shallow(
  <AvatarEditor
    store={configureStore()({})}
    screenProps={{
      data: {
        user: {
          _id: 'TgcTAeWywrrtKBamQ',
          emails: [
            {
              address: 'bugdonepavlov@gmail.com',
              verified: false,
              __typename: 'Email',
            },
          ],
          roles: null,
          numberOfFollowers: 3,
          numberFollowing: 3,
          numberOfPosts: 3,
          following: 'none',
          follow: null,
          profile: {
            firstName: 'Bogdan',
            lastName: 'pavlov',
            image: null,
            description: null,
            __typename: 'Profile',
          },
          __typename: 'User',
        },
      },
    }}
  />
);

describe('<AvatarEditor />', () => {
  it('<AvatarEditor /> should render', async () => {
    await new Promise(resolve => setTimeout(resolve));
    instance.update();
    expect(instance.exists()).toBe(true);
    expect(instance).toMatchSnapshot();
  });

  it('should still render when loading is true', async () => {
    await new Promise(resolve => setTimeout(resolve));
    instance.update();
    instance.setState({ loading: true });
    expect(instance.exists()).toBe(true);
    expect(instance).toMatchSnapshot();
  });
});
