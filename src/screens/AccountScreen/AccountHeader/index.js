// @flow
import { graphql } from 'react-apollo';

import { User } from '../../../apollo';

import AccountHeader from './AccountHeader';

const options = props => {
  const { screenProps } = props;
  const variables = { _id: screenProps.data.user._id };
  return { variables };
};

const AccountHeaderWithData = graphql(User.Query.getUserById, { options })(AccountHeader);

export default AccountHeaderWithData;
