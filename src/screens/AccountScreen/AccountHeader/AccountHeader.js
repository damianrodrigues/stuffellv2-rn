// @flow
import React from 'react';
import { View, Text } from 'react-native';
// COMPONENTS
import RegularButton from '../../../components/common/RegularButton';
import ProfileAvatar from '../../../components/common/ProfileAvatar';
import LoadingIndicator from '../../../components/common/LoadingIndicator';
import { RegularText } from '../../../components/text';
// STYLES
import s from './styles';
// TYPES
type Props = {
  screenProps: Object,
  navigation: Object,
  data: Object,
};
// COMPONENT
export default class AccountHeader extends React.PureComponent<Props> {
  render() {
    const { screenProps, navigation, data } = this.props;
    const { profile } = screenProps.data.user;
    if (data.loading) {
      return (
        <View style={s.container}>
          <LoadingIndicator label="Loading..." />
        </View>
      );
    }
    const { getUserById } = data;
    const { numberFollowing, numberOfFollowers, numberOfPosts } = getUserById;
    return (
      <View style={s.container}>
        <View style={s.header}>
          <View style={s.avatarContainer}>
            <ProfileAvatar user={screenProps.data.user} />
          </View>
          <View style={s.information}>
            <View>
              <RegularText style={[s.text, s.headingText]} numberOfLines={0}>
                {/* {console.log('asdasdasdasdasdasd', profile.lastName, profile.firstName)} */}
                {profile && profile.firstName} {profile && profile.lastName}
              </RegularText>
              {profile &&
                profile.description && (
                  <RegularText style={s.text} numberOfLines={3}>
                    {profile.description}
                  </RegularText>
                )}
            </View>
            <RegularButton
              title="Edit profile"
              textStyle={s.buttonTextStyle}
              buttonStyle={s.buttonStyle}
              onPress={() => {
                navigation.navigate('editProfile');
              }}
            />
          </View>
        </View>
        <View style={s.stats}>
          <View style={s.statsItem}>
            <Text style={s.statsText}>{numberFollowing}</Text>
            <Text style={s.statsTitle}>POSTS</Text>
          </View>
          <View style={s.statsItem}>
            <Text style={s.statsText}>{numberOfFollowers}</Text>
            <Text style={s.statsTitle}>FOLLOWERS</Text>
          </View>
          <View style={s.statsItem}>
            <Text style={s.statsText}>{numberOfPosts}</Text>
            <Text style={s.statsTitle}>FOLLOWING</Text>
          </View>
        </View>
      </View>
    );
  }
}
