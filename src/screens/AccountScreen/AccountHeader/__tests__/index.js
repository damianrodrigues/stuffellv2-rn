import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { mount } from 'enzyme';
import AccountHeader from '../AccountHeader';
import data from './data';

const mocks = [data];

describe('AccountHeader views', () => {
  it('renders Header', async () => {
    const instance = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AccountHeader
          data={{
            _id: 'TgcTAeWywrrtKBamQ',
            getUserById: {
              numberOfFollowers: 3,
              numberFollowing: 3,
              numberOfPosts: 3,
            },
          }}
          screenProps={{
            data: {
              user: {
                _id: 'TgcTAeWywrrtKBamQ',
                emails: [
                  {
                    address: 'bugdonepavlov@gmail.com',
                    verified: false,
                    __typename: 'Email',
                  },
                ],
                roles: null,
                numberOfFollowers: 3,
                numberFollowing: 3,
                numberOfPosts: 3,
                following: 'none',
                follow: null,
                profile: {
                  firstName: 'Bogdan',
                  lastName: 'pavlov',
                  image: null,
                  description: null,
                  __typename: 'Profile',
                },
                __typename: 'User',
              },
            },
          }}
        />
      </MockedProvider>
    );

    await new Promise(resolve => setTimeout(resolve));
    instance.update();
    expect(instance.find('AccountHeader').exists()).toBeTruthy();
    expect(instance.find('ProfileAvatar').exists()).toBeTruthy();
    expect(instance.find('RegularButton').exists()).toBeTruthy();

    expect(instance).toMatchSnapshot();
  });
});
