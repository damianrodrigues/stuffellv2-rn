import { User } from '../../../../apollo';

export default {
  request: {
    cache: false,
    query: User.Query.getUserById,
    variables: {
      _id: 'TgcTAeWywrrtKBamQ',
    },
  },
  result: {
    data: {
      getUserById: {
        _id: 'TgcTAeWywrrtKBamQ',
        emails: [
          {
            address: 'bugdonepavlov@gmail.com',
            verified: false,
            __typename: 'Email',
          },
        ],
        roles: null,
        numberOfFollowers: 3,
        numberFollowing: 3,
        numberOfPosts: 3,
        following: 'none',
        follow: null,
        profile: {
          firstName: 'Bogdan',
          lastName: 'pavlov',
          image: null,
          description: null,
          __typename: 'Profile',
        },
        __typename: 'User',
      },
    },
  },
};
