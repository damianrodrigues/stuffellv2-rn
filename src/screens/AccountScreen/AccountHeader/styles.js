import { StyleSheet } from 'react-native';
// LIB
import { styleConstants } from '../../../lib/config';
import { StyleHelpers } from '../../../lib/helpers';

const { color, defaultDoubledMargin, defaultMargin } = styleConstants;

export default StyleSheet.create({
  container: {
    paddingHorizontal: defaultDoubledMargin,
    minHeight: StyleHelpers.normalizeHeight(210),
    borderBottomColor: color.screenBackgroundGrey,
    borderBottomWidth: 1,
    backgroundColor: '#fff',
    width: '100%',
  },
  header: {
    flexDirection: 'row',
    minHeight: StyleHelpers.normalizeHeight(80),
    marginVertical: defaultDoubledMargin,
  },
  avatarContainer: {
    marginRight: 5,
  },
  text: {
    paddingLeft: 15,
    marginBottom: 5,
  },
  buttonTextStyle: {
    fontSize: 14,
    color: color.black,
    flex: 2,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: styleConstants.fontWeight.bold,
  },
  buttonStyle: {
    width: '100%',
    borderWidth: 2,
    borderColor: color.black,
    height: 33,
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  headingText: {
    paddingTop: defaultMargin,
    fontWeight: styleConstants.fontWeight.bold,
  },
  information: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  stats: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  statsTitle: {
    fontWeight: styleConstants.fontWeight.light,
    paddingBottom: defaultMargin,
    color: '#aaa',
    fontSize: 13,
  },
  statsItem: {
    paddingHorizontal: 10,
    alignItems: 'center',
    flexBasis: '33.333333%',
    maxWidth: '33.333333%',
    display: 'flex',
    justifyContent: 'center',
  },
  statsText: {
    fontWeight: styleConstants.fontWeight.bold,
  },
});
