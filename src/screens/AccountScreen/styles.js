// @flow
import { StyleSheet, Dimensions } from 'react-native';
import { width, height } from 'react-native-dimension';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '25%',
  },
  regularButton: {
    width: width(38),
    borderWidth: 2,
    height: height(3),
    backgroundColor: '#fff',
  },
  textStyle: {
    fontSize: height(2.2),
    color: '#000',
  },
});

export const initialLayout = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
