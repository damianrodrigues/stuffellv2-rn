// TOP LEVEL IMPORTS
import React from 'react';
import { WebBrowser } from 'expo';
import { logout } from 'meteor-apollo-accounts';
import { View, StyleSheet } from 'react-native';
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import { Feather } from '@expo/vector-icons';
// COMPONENTS
import { BodyText } from '../../../components/text/index';
// APOLLO
import ApolloClient from '../../../apollo/ApolloClient';

class DropdownMenu extends React.PureComponent {
  onLogout = async () => {
    await logout(ApolloClient);
    return this.props.onLogout();
  };

  onPrivacy = () => {
    WebBrowser.openBrowserAsync(
      'https://termsfeed.com/privacy-policy/2f482594f8f036a849ff639786faabec'
    );
  };

  onFAQ = () => {
    WebBrowser.openBrowserAsync('https://www.stuffell.com/support');
  };

  onHelp = () => {
    this.props.navigation.navigate('help');
  };

  render() {
    return (
      <View>
        <Menu>
          <MenuTrigger>
            <Feather name="menu" size={30} color="#fff" style={{ marginRight: 10 }} />
          </MenuTrigger>
          <MenuOptions style={{ padding: 3, marginRight: 5 }}>
            <MenuOption onSelect={this.onHelp}>
              <BodyText style={s.textStyle}>Help & Support</BodyText>
            </MenuOption>
            <MenuOption onSelect={this.onFAQ}>
              <BodyText style={s.textStyle}>FAQ</BodyText>
            </MenuOption>
            <MenuOption onSelect={this.onPrivacy}>
              <BodyText style={s.textStyle}>Privacy Policy</BodyText>
            </MenuOption>
            <MenuOption onSelect={this.onLogout}>
              <BodyText style={s.textStyle}>Logout</BodyText>
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  }
}

const s = StyleSheet.create({
  textStyle: {
    padding: 5,
  },
});

export default DropdownMenu;
