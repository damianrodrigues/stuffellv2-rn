// @flow
import * as React from 'react';
import { compose, graphql } from 'react-apollo';
import { ActivityIndicator, View, FlatList, Text, Dimensions } from 'react-native';
// HELPERS
import { styleConstants } from '../../../lib/config/styleConstants';
// APPOLLO
import { ItemRequest } from '../../../apollo/ItemRequest';
// COMPONENTS
import Card from '../../../components/common/Card';
import LoadingIndicator from '../../../components/common/LoadingIndicator';
import ItemRequestLink from '../../../components/common/ItemRequestLink';

type State = {
  loadingMore: boolean,
};

type Props = {
  myItemRequestsQuery: Object,
  screenProps: Object,
  navigation: Object,
};

class AccountItemRequestList extends React.Component<Props, State> {
  state = {
    loadingMore: false,
  };

  onEndReached = () => {
    const { loadingMore } = this.state;
    const { myItemRequestsQuery, screenProps } = this.props;
    if (!myItemRequestsQuery.itemRequests) {
      return null;
    }

    if (loadingMore) {
      return console.log('loadingMore');
    }

    this.setState({ loadingMore: true }, () => {
      const params = {
        skip: myItemRequestsQuery.myItemRequests.length,
        userId: screenProps.data.user._id,
      };

      const variables = {
        params,
      };

      myItemRequestsQuery
        .fetchMore({
          variables,
          updateQuery: (prev, { fetchMoreResult }) => {
            // Don't do anything if there weren't any new items
            if (!fetchMoreResult || fetchMoreResult.itemRequests.length === 0) {
              return prev.itemRequests;
            }

            return {
              // Append the new feed results to the old one
              itemRequests: [...prev.itemRequests, ...fetchMoreResult.itemRequests], // previousResult.tickets.concat(fetchMoreResult.tickets),
            };
          },
        })
        .then(() => this.setState({ loadingMore: false }));
    });
  };

  showLoadingMore = () => {
    const { loadingMore } = this.state;
    if (loadingMore) {
      return (
        <View
          style={{
            height: 150,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return <View style={{ height: 50 }} />;
  };

  renderItem = (item, index) => {
    const { screenProps, navigation } = this.props;
    return (
      <Card
        accountPage
        key={item._id}
        user={screenProps.data.user}
        item={item}
        index={index}
        navigation={navigation}
        {...this.props}
      />
    );
  };

  renderEmptyComponent = () => (
    <View style={{ flex: 1 }}>
      <Text style={{ alignSelf: 'center', marginTop: 30 }}>No Requests....</Text>
    </View>
  );

  render() {
    const { loadingMore } = this.state;
    const { myItemRequestsQuery, navigation } = this.props;
    const { height } = Dimensions.get('window');

    if (myItemRequestsQuery.loading && !loadingMore) {
      return (
        <View style={{ flex: 1, height: height - 160 }}>
          <LoadingIndicator label="Loading Requests..." />
        </View>
      );
    }

    return (
      <FlatList
        ListHeaderComponent={<ItemRequestLink navigation={navigation} />}
        data={myItemRequestsQuery.myItemRequests}
        keyExtractor={item => item._id}
        onEndReached={this.onEndReached}
        scrollEnabled={false}
        onEndReachedThreshold={1}
        disableVirtualization={false}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => (
          <View style={{ height: 2, backgroundColor: styleConstants.color.screenBackgroundGrey }} />
        )}
        ListEmptyComponent={this.renderEmptyComponent()}
        refreshing={myItemRequestsQuery.networkStatus === 4}
        onRefresh={() => myItemRequestsQuery.refetch()}
        ListFooterComponent={this.showLoadingMore()}
        removeClippedSubviews={false}
        renderItem={({ item, index }) => this.renderItem(item, index)}
      />
    );
  }
}

const ComponentWithData = compose(
  graphql(ItemRequest.Query.myItemRequests, {
    name: 'myItemRequestsQuery',
    fetchPolicy: 'network-only',
    options: {
      notifyOnNetworkStatusChange: true,
      fetchPolicy: 'network-only',
    },
  })
)(AccountItemRequestList);

export default ComponentWithData;
