import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { shallow } from 'enzyme';
import AccountScreen from '../AccountScreen';

describe('AccountScreen views', () => {
  it('renders AccountScreen', async () => {
    const instance = shallow(
      <MockedProvider
        mocks={{
          request: {
            cache: false,
            query: '',
          },
          result: {},
        }}
        addTypename={false}>
        <AccountScreen modals={{ archive: 'Archive Item' }} />
      </MockedProvider>
    );

    await new Promise(resolve => setTimeout(resolve));
    instance.update();

    expect(instance.exists()).toBeTruthy();

    expect(instance).toMatchSnapshot();
  });
});
