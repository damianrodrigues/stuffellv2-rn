import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import { mount } from 'enzyme';
import AccountPostsList from '../index';
import data from './data';

const mocks = [data];

describe('AccountPostsList views', () => {
  it('renders AccountPostsList', async () => {
    const instance = mount(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AccountPostsList
          screenProps={{
            user: {
              emails: [
                {
                  address: 'bugdonepavlov@gmail.com',
                  verified: false,
                  __typename: 'Email',
                },
              ],
              roles: null,
              profile: {
                firstName: 'Bogdan',
                lastName: 'pavlov',
                cellPhone: null,
                image: null,
                description: null,
                workPhone: null,
                __typename: 'Profile',
              },
              _id: 'TgcTAeWywrrtKBamQ',
              __typename: 'User',
            },
          }}
        />
      </MockedProvider>
    );

    await new Promise(resolve => setTimeout(resolve));
    instance.update();

    expect(instance.find('FlatList').exists()).toBeTruthy();
    expect(instance).toMatchSnapshot();
  });
});
