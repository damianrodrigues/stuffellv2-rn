// @flow
import * as React from 'react';
import { compose, graphql } from 'react-apollo';
import { ActivityIndicator, View, FlatList, Text, Dimensions } from 'react-native';
// CONSTANTS
import { styleConstants } from '../../../lib/config/styleConstants';
// APPOLLO
import { Post } from '../../../apollo/Post';
// COMPONENTS
import Card from '../../../components/common/Card';
import LoadingIndicator from '../../../components/common/LoadingIndicator';

type State = {
  loadingMore: boolean,
};

type Props = {
  myPostsQuery: Object,
  screenProps: Object,
  navigation: Object,
};

class AccountPostsList extends React.PureComponent<Props, State> {
  state = {
    loadingMore: false,
  };

  onEndReached = () => {
    const { loadingMore } = this.state;
    const { myPostsQuery, screenProps } = this.props;
    if (!myPostsQuery.posts) {
      return null;
    }

    if (loadingMore) {
      return console.log('loadingMore');
    }

    this.setState({ loadingMore: true });

    const params = {
      skip: myPostsQuery.myPosts.length,
      userId: screenProps.data.user._id,
    };

    const variables = {
      params,
    };

    myPostsQuery
      .fetchMore({
        variables,
        updateQuery: (prev, { fetchMoreResult }) => {
          // Don't do anything if there weren't any new items
          if (!fetchMoreResult || fetchMoreResult.posts.length === 0) {
            return prev.posts;
          }

          return {
            // Append the new feed results to the old one
            posts: [...prev.posts, ...fetchMoreResult.posts], // previousResult.tickets.concat(fetchMoreResult.tickets),
          };
        },
      })
      .then(() => this.setState({ loadingMore: false }));
  };

  showLoadingMore = () => {
    const { loadingMore } = this.state;
    if (loadingMore) {
      return (
        <View
          style={{
            height: 150,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return <View style={{ height: 50 }} />;
  };

  renderItem = (item, index) => {
    const { screenProps, navigation } = this.props;

    return (
      <Card
        accountPage
        key={item._id}
        user={screenProps.data.user}
        item={item}
        navigation={navigation}
        index={index}
        {...this.props}
      />
    );
  };

  renderEmptyComponent = () => (
    <View style={{ flex: 1 }}>
      <Text style={{ alignSelf: 'center', marginTop: 30 }}>No Posts....</Text>
    </View>
  );

  render() {
    const { loadingMore } = this.state;
    const { myPostsQuery } = this.props;
    const { height } = Dimensions.get('window');

    if (myPostsQuery.loading && !loadingMore) {
      return (
        <View style={{ flex: 1, height: height - 160 }}>
          <LoadingIndicator label="Loading Posts..." />
        </View>
      );
    }

    // console.log('this.props====', this.props);

    return (
      <FlatList
        data={myPostsQuery.myPosts}
        keyExtractor={item => item._id}
        scrollEnabled={false}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={1}
        disableVirtualization={false}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={this.renderEmptyComponent()}
        refreshing={myPostsQuery.networkStatus === 4}
        ItemSeparatorComponent={() => (
          <View style={{ height: 2, backgroundColor: styleConstants.color.screenBackgroundGrey }} />
        )}
        onRefresh={() => myPostsQuery.refetch()}
        ListFooterComponent={this.showLoadingMore()}
        removeClippedSubviews={false}
        renderItem={({ item, index }) => this.renderItem(item, index)}
      />
    );
  }
}

const ComponentWithData = compose(
  graphql(Post.Query.myPosts, {
    name: 'myPostsQuery',
    options: {
      notifyOnNetworkStatusChange: true,
      fetchPolicy: 'network-only',
    },
  })
)(AccountPostsList);

export default ComponentWithData;
