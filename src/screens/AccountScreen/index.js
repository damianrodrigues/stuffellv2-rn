import { connect } from 'react-redux';
import * as actions from '../../redux/actions';

import AccountScreen from './AccountScreen';

const mapStateToProps = ({ modals }) => ({
  modals,
});

export default connect(mapStateToProps, actions)(AccountScreen);
