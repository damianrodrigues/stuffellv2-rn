// @flow
import * as React from 'react';
import PopupDialog from 'react-native-popup-dialog';
import { View, Text, Animated, ScrollView, Keyboard } from 'react-native';
import { height } from 'react-native-dimension';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';
import { styleConstants, stylesConfig } from '../../lib/config';
import BackButton from '../../components/common/BackButton';
import RegularButton from '../../components/common/RegularButton';
import DropdownMenu from './DropdownMenu';
import AccountPostsList from './AccountPostsList';
import AccountItemRequestList from './AccountItemRequestList';
import AcountHeader from './AccountHeader';
import s, { initialLayout } from './styles';

type Props = {
  modals: Object,
  navigation: Object,
  screenProps: Object,
  tintColor: string,
  onArchiveModalOpen: Function,
  setArchiveMode: Function,
  onSellModalOpen: Function,
  setSellMode: Function,
};

type State = {
  tabNavigationState: Object,
};

export default class AccountScreen extends React.PureComponent<Props, State> {
  static navigationOptions = ({ navigation, screenProps }: Props) => ({
    title: 'Account',
    headerTitleStyle: stylesConfig.titleStyle,
    headerStyle: stylesConfig.basicHeaderStyle,
    headerRight: <DropdownMenu navigation={navigation} onLogout={screenProps.onLogout} />,
    headerLeft: (
      <BackButton
        screenProps={screenProps}
        navigation={navigation}
        onPress={() => navigation.goBack(null)}
      />
    ),
  });

  state = {
    tabNavigationState: {
      index: 0,
      routes: [{ key: 'first', title: 'My Stuff' }, { key: 'second', title: 'My Requests' }],
    },
  };

  componentDidMount() {
    const { navigation } = this.props;
    if (navigation.state.params && navigation.state.params.toMyRequests) {
      this.setState(prevState => ({
        tabNavigationState: {
          routes: prevState.tabNavigationState.routes,
          index: 1,
        },
      }));
    }
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  // ============== Modal Handlers ==============
  modalOnArchivePress = (func: Function) => {
    const { onArchiveModalOpen, setArchiveMode } = this.props;
    onArchiveModalOpen(func);
    setArchiveMode(true);
  };

  modalOnUnArchivePress = (func: Function) => {
    const { onArchiveModalOpen, setArchiveMode } = this.props;
    onArchiveModalOpen(func);
    setArchiveMode(false);
  };

  modalOnSellPress = (func: Function) => {
    const { onSellModalOpen, setSellMode } = this.props;
    onSellModalOpen(func);
    setSellMode(true);
  };

  modalOnUnSellPress = (func: Function) => {
    const { onSellModalOpen, setSellMode } = this.props;
    onSellModalOpen(func);
    setSellMode(false);
  };

  // ============== Tab Handlers ==============
  handleIndexChange = (index: number) => {
    const { tabNavigationState } = this.state;
    this.setState({
      tabNavigationState: {
        ...tabNavigationState,
        index,
      },
    });
  };

  // ============== Renders Tab Content (Scenes) ==============
  renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return (
          <View style={[s.container, { backgroundColor: '#fff' }]}>
            <AccountPostsList
              archiveModal={this.state && this.popupArchiveDialog}
              soldModal={this.state && this.popupSoldDialog}
              modalOnArchivePress={this.modalOnArchivePress}
              modalOnUnArchivePress={this.modalOnUnArchivePress}
              modalOnSellPress={this.modalOnSellPress}
              modalOnUnSellPress={this.modalOnUnSellPress}
              handleKeyboard={this.handleKeyboard}
              {...this.props}
            />
          </View>
        );

      case 'second':
        return (
          <View style={[s.container, { backgroundColor: '#fff' }]}>
            <AccountItemRequestList {...this.props} handleKeyboard={this.handleKeyboard} />
          </View>
        );

      default:
        return null;
    }
  };

  // ============== Render Tab Headers ==============
  renderHeader = props => (
    <TabBar
      indicatorStyle={{
        backgroundColor: styleConstants.color.primary,
      }}
      style={{
        backgroundColor: styleConstants.color.screenBackground,
        height: 45,
      }}
      tabStyle={{
        height: 45,
      }}
      renderLabel={this.renderLabel(props)} // renderer of a single Lable on the tab bar
      {...props}
    />
  );

  // ============== Render Tab labels ==============
  renderLabel = props => ({ route, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const outputRange = inputRange.map(
      inputIndex =>
        inputIndex === index ? styleConstants.color.primary : styleConstants.color.black1
    );
    const color = props.position.interpolate({
      inputRange,
      outputRange,
    });

    return (
      <Animated.Text
        style={[
          {
            fontFamily: styleConstants.fontFamily.regular,
            fontSize: styleConstants.fontSize.md,
          },
          { color },
        ]}>
        {route.title}
      </Animated.Text>
    );
  };

  render() {
    const { modals } = this.props;
    const { tabNavigationState } = this.state;

    return (
      <ScrollView keyboardShouldPersistTaps="always">
        <View>
          <PopupDialog
            dialogStyle={{
              backgroundColor: styleConstants.color.primary,
              borderColor: styleConstants.color.primary,
              borderRadius: 20,
            }}
            ref={popupArchiveDialog => {
              this.popupArchiveDialog = popupArchiveDialog;
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: height(4),
                  fontWeight: '600',
                  color: 'white',
                }}>
                Are you sure?
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  color: 'white',
                  fontSize: height(2.5),
                }}>
                {modals.archive
                  ? 'Your item will no longer be visible to other users. You will still see your item in your inventory. You can unarchive your item at any time.'
                  : 'Your item will become active again and will be viewable by other users.'}
              </Text>
              <View style={s.buttonContainer}>
                <RegularButton
                  onPress={modals.modalArchiveFunc}
                  title={modals.archive ? 'Archive Item' : 'Unarchive Item'}
                  textStyle={s.textStyle}
                  buttonStyle={s.regularButton}
                  disabled={false}
                />
                <RegularButton
                  onPress={() => {
                    this.popupArchiveDialog.dismiss();
                  }}
                  title="Cancel"
                  textStyle={s.textStyle}
                  buttonStyle={s.regularButton}
                  disabled={false}
                />
              </View>
            </View>
          </PopupDialog>
          <PopupDialog
            width={0.95}
            dialogStyle={{
              backgroundColor: styleConstants.color.primary,
              borderColor: styleConstants.color.primary,
              borderRadius: 20,
            }}
            ref={popupSoldDialog => {
              this.popupSoldDialog = popupSoldDialog;
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'space-around',
                alignItems: 'center',
                padding: 30,
              }}>
              <Text
                style={{
                  fontSize: height(4),
                  fontWeight: '600',
                  textAlign: 'center',
                  color: 'white',
                }}>
                Are you sure?
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  color: 'white',
                  fontSize: height(2.5),
                }}>
                {modals.sell
                  ? 'Your item will be marked as sold. Stuffell automatically archives sold items after two weeks.'
                  : 'Your item will no longer be listed as sold. You can change its status back to sold at any time.'}
              </Text>
              <View style={s.buttonContainer}>
                <RegularButton
                  onPress={modals.modalSellFunc}
                  title={modals.sell ? 'Mark as sold' : 'Mark as unsold'}
                  textStyle={s.textStyle}
                  buttonStyle={s.regularButton}
                  disabled={false}
                />
                <RegularButton
                  onPress={() => {
                    this.popupSoldDialog.dismiss();
                  }}
                  title="Cancel"
                  textStyle={s.textStyle}
                  buttonStyle={s.regularButton}
                  disabled={false}
                />
              </View>
            </View>
          </PopupDialog>

          <AcountHeader {...this.props} />

          <TabViewAnimated
            style={s.container}
            navigationState={tabNavigationState}
            renderScene={this.renderScene}
            renderHeader={this.renderHeader}
            onIndexChange={this.handleIndexChange}
            initialLayout={initialLayout}
          />
        </View>
      </ScrollView>
    );
  }
}
