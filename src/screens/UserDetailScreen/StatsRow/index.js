// @flow
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { HeadlineText, CaptionText } from '../../../components/text';

const headerStyle = {
  textAlign: 'center',
};

const labelStyle = {
  textAlign: 'center',
};

type Props = {
  user: Object, // the user's whose profile we are viewing
  loading: Boolean, // if we're still loading the user record fromm the Db or not
};

const StatCol = ({ label, value }) => (
  <View style={{ flex: 1 }}>
    <HeadlineText style={headerStyle}>{value}</HeadlineText>
    <CaptionText style={labelStyle}>{label}</CaptionText>
  </View>
);

const StatsRow = ({ user, loading }: Props) => {
  if (loading) {
    return <View style={s.container} />;
  }

  return (
    <View style={s.container}>
      <StatCol label="POSTS" value={user.numberOfPosts || 0} />
      <StatCol label="FOLLOWERS" value={user.numberOfFollowers || 0} />
      <StatCol label="FOLLOWING" value={user.numberFollowing || 0} />
    </View>
  );
};

const s = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    minHeight: 65,
  },
});

export default StatsRow;
