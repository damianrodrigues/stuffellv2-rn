// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Image } from 'react-native-expo-image-cache';
import { appConfig, styleConstants } from '../../../lib/config';
import { UiHelpers } from '../../../lib/helpers';
import FollowButton from '../../../components/common/FollowButton';
import { BodyText, HeadlineText } from '../../../components/text';

type Props = {
  targetUser: Object, // item === user record for the current profile you're viewing
  user: Object, // user === currently signed in user
  loading: Boolean,
};

const UserProfileTop = ({ targetUser, loading, user }: Props) => {
  const source = appConfig.images.defaultAvatar;

  const preview = {
    uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAA+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==',
  };

  // if (!targetUser.profile.image) {
  //   preview.uri = appConfig.images.defaultAvatar
  // }

  return (
    <View style={s.container}>
      <View>
        <Image
          style={s.image}
          {...{
            preview,
            uri:
              targetUser.profile.image ||
              'https://res.cloudinary.com/foundry41/image/upload/v1523396468/avatar.jpg',
          }}
        />
        <HeadlineText style={{ margin: 0, textAlign: 'center' }}>
          {UiHelpers.getFullName(targetUser)}
        </HeadlineText>
        <BodyText style={{ textAlign: 'center' }}>
          {(targetUser && targetUser.profile && targetUser.profile.description) || ''}
        </BodyText>
        <View style={s.followButtonContainer}>
          {!loading &&
            targetUser._id !== user._id && (
              <FollowButton follow={targetUser.follow} targetUser={targetUser} incoming={null} />
            )}
        </View>
      </View>
    </View>
  );
};

const s = StyleSheet.create({
  container: {
    marginHorizontal: styleConstants.defaultDoubledMargin,
    paddingTop: 15,
    minHeight: 150,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  followButtonContainer: {
    alignSelf: 'center',
    margin: 4,
  },
  image: {
    alignSelf: 'center',
    marginBottom: 10,
    width: 80,
    height: 80,
    borderRadius: 40,
  },
});

export default UserProfileTop;
