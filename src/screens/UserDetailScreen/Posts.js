import React from 'react';
import { View, ActivityIndicator, FlatList } from 'react-native';
import { graphql, compose } from 'react-apollo';
// APOLLO
import { Post } from '../../apollo/Post';
// COMPONENTS
import Card from '../../components/common/Card';
import EmptyState from '../../components/common/EmptyState';

// EXPORTED COMPONENT
// ====================================
class Posts extends React.PureComponent {
  state = {
    refreshing: false,
  };

  onEndReached = () => {
    const { postsQuery, postsCountQuery } = this.props;

    if (!postsQuery.posts) {
      return null;
    }

    const currentDataLength = postsQuery.posts.length;

    const inDBDataLength = postsCountQuery.postsCount.count;

    if (currentDataLength >= inDBDataLength) {
      return this.setState({ loadingMore: false });
    }

    if (this.state.loadingMore) {
      return console.log('loadingMore');
    }

    this.setState({ loadingMore: true });

    const variables = {
      params: {
        statuses: this.props.statuses,
        categories: this.props.categories,
        userId: this.props.user._id,
        searchText: this.props.searchText,
        skip: postsQuery.posts.length,
      },
      string: '',
    };

    this.props.postsQuery
      .fetchMore({
        variables,
        updateQuery: (prev, { fetchMoreResult }) => {
          // Don't do anything if there weren't any new items
          if (!fetchMoreResult || fetchMoreResult.posts.length === 0) {
            return prev;
          }
          return {
            posts: prev.posts.concat(fetchMoreResult.posts),
          };
        },
      })
      .then(() => this.setState({ loadingMore: false }));
  };

  renderItem = item => (
      <Card 
        item={item} 
        navigation={this.props.navigation} 
        user={this.props.screenProps.data.user}
      />
    );

  onRefresh = () => {
    // refetch the query
    this.props.postsQuery.refetch();
  };

  showLoadingMore = () => {
    if (this.state.loadingMore) {
      return (
        <View
          style={{
            height: 150,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return <View style={{ height: 50 }} />;
  };

  render() {
    const { postsQuery, getUserById, renderHeader, user } = this.props;
    const { following, _id } = getUserById; // the user of this "user detail" screen
    const areFriends = following === 'none' || following === 'pending' || following === 'incoming'; // are the current user and this user friends?

    // if following does not exists, show empty state OR
    // if the currently signed in user is not friends with this user AND the currently signed in user is not looking at their own page, show empty state OR
    if (!following || (!areFriends && _id !== user._id)) {
      return (
        <View style={{  width: '100%', backgroundColor: '#fff'  }}>
          {renderHeader()}
          <EmptyState
            header="You Are not Friends Yet!"
            subheader={`Send a friend request to see ${getUserById.profile.firstName}'s Posts`}
          />
        </View>
      );
    }

    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        data={this.state.refreshing ? [] : postsQuery.posts}
        keyExtractor={({ _id }, index) => _id + index}
        refreshing={postsQuery.networkStatus === 4}
        onRefresh={this.onRefresh}
        onEndReached={this.onEndReached}
        ListFooterComponent={this.showLoadingMore}
        ListHeaderComponent={renderHeader}
        renderItem={({ item }) => this.renderItem(item)}
      />
    );
  }
}

const options = props => {
  const params = {
    statuses: props.statuses,
    categories: props.categories,
    userId: props.user._id,
    searchText: props.searchText,
    skip: 0,
  };

  return {
    variables: { params },
  };
};

const ComponentWithData = compose(
  graphql(Post.Query.posts, { options, name: 'postsQuery' }),
  graphql(Post.Query.postsCount, { options, name: 'postsCountQuery' })
)(Posts);

// EXPORT
// ====================================
export default ComponentWithData;
