import React from 'react';
import { graphql } from 'react-apollo';
import { View, ActivityIndicator } from 'react-native';
// LIBS
import { UiHelpers } from '../../lib/helpers/UiHelpers';
import { styleConstants } from '../../lib/config';
// APOLLO
import { User } from '../../apollo/User';
// MODULES
import UserProfileTop from './UserProfileTop';
import StatsRow from './StatsRow';
import BackButton from '../../components/common/BackButton';
import Posts from './Posts';

class UserDetailScreen extends React.Component {
  static navigationOptions = ({ navigation, screenProps }) =>
    UiHelpers.getBackNavOptions(navigation, screenProps, {
      // we pass in the full user object so that we can show the data quicker, rather than waiting to get a response from DB.
      // then we show a spinner for data like the list of his posts
      title: UiHelpers.getFullName(navigation.state.params.user) || `User Profile`,
      headerLeft: (
        <BackButton
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
    });

  componentWillReceiveProps() {
    // to start, we just have the user we passed in via react-navigation params
    // but we are also grabbing the user from the DB in the background, when we get the user from db,
    // we want to update the params with the new data
    // doing this will mess up goBack, so we have to remember to check that we're still looking for the same person

    // check if query is loading
    if (this.props.getUserByIdQuery.loading) {
      return null;
    }

    // check if user object is on the query, if not, return null
    if (!this.props.getUserByIdQuery.getUserById || !this.props.getUserByIdQuery.getUserById._id) {
      return null;
    }

    // check the params to see if the query response is for the current param user,
    // if it is, continue. If it's an old query, don't conitue
    const userChanged =
      this.props.navigation.state.params.user &&
      this.props.navigation.state.params.user._id !== this.props.getUserByIdQuery.getUserById._id;

    // if the user has not changed and we have a response form db, update the navigation params
    if (!userChanged) {
      // return this.props.navigation.setParams({ user: this.props.getUserByIdQuery.getUserById });
    }
  }

  renderHeader = () => {
    const { navigation, getUserByIdQuery, screenProps } = this.props;

    // the target user is used to identify the "target" of the follow/unfollow button... that is, the user you're going to follow if you click the follow button. They're the targetUser.
    // sometimes we are able to pass that user in via react-navigation params, other places we don't have the user object yet so we need to wait for the query to get done
    const targetUser = navigation.state.params.user || getUserByIdQuery.getUserById;

    return (
      <View>
        <UserProfileTop
          targetUser={targetUser}
          user={screenProps.data.user}
          loading={getUserByIdQuery.loading}
        />
        <StatsRow
          user={getUserByIdQuery.getUserById}
          loading={getUserByIdQuery.loading} // if loading, we won't show the stats row
        />
      </View>
    );
  };

  render() {
    const { getUserByIdQuery, navigation } = this.props;

    // check if the user's details have loaded
    // sometimes we are able to pass that user in via react-navigation params,
    // other places we don't have the user object yet so we need to wait for the query to get done
    if (!navigation.state.params.user && getUserByIdQuery.loading) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: styleConstants.color.screenBackground,
            alignItems: 'center',
            justifyContent: 'center',
           }}>
          <ActivityIndicator size="large" />
        </View>
      );
    }

    // check if the user's details have loaded
    if (getUserByIdQuery.loading) {
      return this.renderHeader();
    }

    // if the user query is done loading, show the Posts list and render the header via FlatList
    return (
      <View
        style={{
          backgroundColor: styleConstants.color.screenBackground,
        }}>
        <Posts
          {...this.props}
          user={getUserByIdQuery.getUserById}
          renderHeader={this.renderHeader}
          getUserById={getUserByIdQuery.getUserById}
        />
      </View>
    );
  }
}

// APOLLO
// =========================
const options = ({ navigation }) => {
  const variables = {
    _id: navigation.state.params._id,
  };
  return { variables };
};

// EXPORT
// =========================
export default graphql(User.Query.getUserById, {
  options,
  name: 'getUserByIdQuery',
})(UserDetailScreen);
