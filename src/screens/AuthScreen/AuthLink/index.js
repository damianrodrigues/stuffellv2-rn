import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
//import { fontConfig } from '../../../modules/config';
import { LinkText } from '../../../components/text';

const AuthLink = ({ label, toggleForm, textColor }) => {
  return (
    <TouchableOpacity >
      {/* <Text
        style={{
          marginTop: 25,
          fontSize: 17,
          textAlign: 'center',
        }}>
        {label}
      </Text> */}
      <LinkText
        onPress={() => toggleForm()}
        style={{
          marginTop: 25,
          fontWeight: '500',
          textAlign: 'center',
        }}>
        {label}
      </LinkText>
    </TouchableOpacity>
  );
};

export default AuthLink;