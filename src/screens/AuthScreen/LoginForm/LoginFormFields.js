// @flow
import React from 'react';
import { View } from 'react-native';
import { formConfig } from '../../../lib/config';
import { FormInputError, EmailInput, PasswordInput } from '../../../components/inputs';

/**
 * LoginFormFields, is a set of form fields used inside of SignupForm
 *
 * @namespace LoginFormFields
 * @memberof LoginForm
 * @prop form {object} - the default form object from rc-form
 * @prop email {string} - the default email string
 */

type Props = {
  form?: Object,
  email?: String,
  password?: String,
  onChange: Function,
  rules?: Array,
};

class LoginFormFields extends React.PureComponent<Props> {
  static defaultProps = {
    rules: [],
    form: {},
    password: '',
    email: '',
  };

  goToPassword = () => {
    const { form: { validateFields } } = this.props;
    const fieldsToValidate = ['email'];

    validateFields(fieldsToValidate, {}, (error, values) => {
      if (error) {
        return console.log(error);
      }
      this.password.input.focus();
    });
  };

  render() {
    const { email, password, onChange, form, rules = [] } = this.props;
    const { getFieldError, getFieldDecorator, getFieldValue } = form;

    let emailErrors;
    let passwordErrors;

    // notes on fieldIsClean
    // unlike on the web, TextInput has no disabeld prop, so we use the editable prop instead, which accepts true/false
    // for lastName and phone, we want them disabled until the prior input is complete and without errors.
    // fieldIsClean takes a field name (e.g. 'firstName') and checks if that field has errors, if not,
    // it then checks it has a value, if it does, then it returns true---> i.e. the next field is editable.
    // fieldIsClean is a useful value, we can also use it to disbale/enable the primary/next button at the bottom of the form and
    // we can also use to for determining the validateTrigger (see notes below)

    const fieldIsClean = fieldName => !!(!getFieldError(fieldName) && getFieldValue(fieldName));

    return (
      <View>
        {form.getFieldDecorator('email', {
          validateTrigger: 'onSubmit',
          trigger: 'onChangeText',
          valuePropName: 'value',
          rules: [...formConfig.defaultEmailRules],
        })(<EmailInput onSubmitEditing={this.goToPassword} returnKeyType="next" />)}
        <FormInputError fieldName="email" getFieldError={getFieldError} />
        {form.getFieldDecorator('password', {
          validateTrigger: 'onSubmit',
          trigger: 'onChangeText',
          valuePropName: 'value',
          rules: [...formConfig.defaultPasswordRules],
        })(
          <PasswordInput
            // onSubmitEditing={this.props.onSubmit}
            ref={el => (this.password = el)} // we use refs to jump from one form input field to another during onSubmitEditing event
          />
        )}
        <FormInputError fieldName="password" getFieldError={getFieldError} />
      </View>
    );
  }
}

export default LoginFormFields;
