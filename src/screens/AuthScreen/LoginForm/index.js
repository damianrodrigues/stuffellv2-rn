// TOP LEVEL IMPORTS
import React from 'react';
import { WebBrowser, SecureStore } from 'expo';
import {
  View,
  Text,
  KeyboardAvoidingView,
  Dimensions,
  StyleSheet,
  Keyboard,
  Platform,
} from 'react-native';
import { createForm } from 'rc-form';
// COMPONENTS
import { loginWithPassword } from 'meteor-apollo-accounts';
import AuthHeader from '../AuthHeader';
import AuthLink from '../AuthLink';
import LoginFormFields from './LoginFormFields';
import ServerErrors from '../../../components/common/ServerErrors';
import RegularButton from '../../../components/common/RegularButton';
import ScreenButtonContainer from '../../../components/layout/ScreenButtonContainer';
import FormScreenContainer from '../../../components/layout/FormScreenContainer';
// Apollo
import ApolloClient from '../../../apollo/ApolloClient';

const width = Dimensions.get('window').width;

class LoginForm extends React.Component {
  state = {
    loading: false,
  };

  onSuccessfulLogin = async ({ email, password }) => {
    // get rid of the keyboard
    Keyboard.dismiss();
    // store the user's email and password, then we can auto-fill the form if their token expires
    await SecureStore.setItemAsync('email', email);
    await SecureStore.setItemAsync('password', password);
    // call checkUserToken which hits API to see if token is good
    this.props.screenProps.checkUserToken();
  };

  onSuccessfullyValidated = async ({ email, password }) => {
    try {
      const id = await loginWithPassword({ email, password }, ApolloClient);
      await ApolloClient.resetStore();
      return this.onSuccessfulLogin({ email, password });
    } catch (err) {
      if (Platform.OS === 'android') {
        // and if they are on andoird, then check to see
        // if their userId exists (await userId()),
        // then we can assume the user was signed up correctly,
        // theyre just having the aforementioend android problem
        if (await userId()) {
          return this.onSuccessfulLogin();
        }
      }
      const error = `${err}`;
      const cleanError = error.replace('Graphql', '');
      this.setState({ errors: [cleanError], loading: false });
    }
  };

  onSubmit = () => {
    const { form } = this.props;
    this.setState({ loading: true }, () => {
      form.validateFields((error, values) => {
        if (error) {
          return this.setState({ loading: false });
        }
        return this.onSuccessfullyValidated(values);
      });
    });
  };

  toggleForm = () => {
    const FORGOT_URL = 'http://stuffell-dev.meteorapp.com/forgot-password';
    return WebBrowser.openBrowserAsync(FORGOT_URL);
  };

  render() {
    const { errors, loading } = this.state;
    const { form, toggleForm } = this.props;
    const { getFieldProps, getFieldError } = form;
    const showButton = !!getFieldError('email') || !!getFieldError('password');
    const getValuePassword = getFieldProps('password').value;

    return (
      <FormScreenContainer style={{ flex: 1 }}>
        <AuthHeader />

        <View
          style={{
            width: width - width * 0.2,
            alignSelf: 'center',
          }}>
          <LoginFormFields form={form} onSubmit={this.onSubmit} />

          <ServerErrors errors={errors} />
          <AuthLink label="Or signup" toggleForm={() => toggleForm('signup')} />
          <AuthLink label="Forgot your password?" toggleForm={this.toggleForm} />
        </View>
        {!showButton &&
          getValuePassword &&
          getValuePassword.length > 5 && (
            <ScreenButtonContainer style={{ backgroundColor: '#fff' }}>
              <RegularButton
                loading={loading}
                disabled={loading}
                title={loading ? 'Logging In...' : 'Log In'}
                onPress={this.onSubmit}
              />
            </ScreenButtonContainer>
          )}
      </FormScreenContainer>
    );
  }
}

export default createForm()(LoginForm);
