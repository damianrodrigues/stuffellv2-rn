// TOP LEVEL IMPORTS
import React from 'react';
import { View, Platform, Dimensions, Keyboard } from 'react-native';
import { createForm } from 'rc-form';
import { createUser, userId } from 'meteor-apollo-accounts';
// COMPONENTS
import AuthHeader from '../AuthHeader';
import AuthLink from '../AuthLink';
import SignupFormFields from './SignupFormFields';
import ServerErrors from '../../../components/common/ServerErrors';
import RegularButton from '../../../components/common/RegularButton';
import ScreenButtonContainer from '../../../components/layout/ScreenButtonContainer';
import FormScreenContainer from '../../../components/layout/FormScreenContainer';
// APOLLO
import ApolloClient from '../../../apollo/ApolloClient';

const newLocal = 'Signing Up...';

const width = Dimensions.get('window').width;

class SignupForm extends React.Component {
  state = {
    loading: false,
  };

  onSuccessfulSignup = async () => {
    Keyboard.dismiss();
    this.setState({ loading: false });
    this.props.screenProps.checkUserToken();
  };

  onSuccessfullyValidated = async ({ email, password, firstName, lastName }) => {
    email.trim().toLowerCase(); // trim and lowercase the email email
    password.trim(); // trim password
    const profile = { firstName, lastName };
    let id;
    try {
      id = await createUser({ email, password, profile }, ApolloClient);
      await ApolloClient.resetStore();
      return this.onSuccessfulSignup();
    } catch (err) {
      // there is a problem with the signup/login on android devices.
      // The user will be signed in, but an error is thrown regardless
      // a short-term fix for this is to check if the user is on android (Platform.OS === 'android')
      if (Platform.OS === 'android') {
        // and if they are on andoird, then check to see if their userId exists (await userId()),
        // then we can assume the user was signed up correctly, theyre just having the aforementioend android problem
        if (await userId()) {
          return this.onSuccessfulSignup(email, password);
        }
      }
      // if they are not on android, we can assume it is a real error,
      // so capture the error and set local state to show the user the error
      const errors =
        err &&
        err.graphQLErrors &&
        err.graphQLErrors.length > 0 &&
        err.graphQLErrors.map(err => err.message);
      return this.setState({ loading: false, errors });
    }
  };

  onSubmit = () => {
    const { form } = this.props;
    this.setState({ loading: true });
    form.validateFields((error, values) => {
      if (error) {
        return this.setState({ loading: false });
      }
      return this.onSuccessfullyValidated(values);
    });
  };

  render() {
    const { errors, loading } = this.state;
    const { form, toggleForm } = this.props;
    const { getFieldProps, getFieldError } = form;
    const showButton =
      !!getFieldError('firstName') ||
      !!getFieldError('lastName') ||
      !!getFieldError('email') ||
      !!getFieldError('password');
    const getValuePassword = getFieldProps('password').value;

    return (
      <FormScreenContainer style={{ flex: 1 }}>
        <AuthHeader />
        <View
          style={{
            width: width - width * 0.2,
            alignSelf: 'center',
          }}>
          <SignupFormFields form={form} />
          <ServerErrors errors={errors} />
          <AuthLink label="Or Login" toggleForm={() => toggleForm('login')} />
        </View>
        {!showButton &&
          getValuePassword &&
          getValuePassword.length > 5 && (
            <ScreenButtonContainer>
              <RegularButton
                loading={loading}
                disabled={loading}
                title={loading ? newLocal : 'Sign Up'}
                onPress={this.onSubmit}
              />
            </ScreenButtonContainer>
          )}
      </FormScreenContainer>
    );
  }
}

export default createForm()(SignupForm);
