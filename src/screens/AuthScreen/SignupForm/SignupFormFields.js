// @flow
import React from 'react';
import { View } from 'react-native';
import { formConfig } from '../../../lib/config';
import {
  FormInputError,
  EmailInput,
  PasswordInput,
  BasicTextInput,
} from '../../../components/inputs';

/**
 * SignupFormFields, is a set of form fields used inside of SignupForm
 *
 * @namespace SignupFormFields
 * @memberof SignupForm
 * @prop form {object} - the default form object from rc-form
 * @prop email {string} - the default email string
 */

type Props = {
  form?: Object,
  email?: String,
  password?: String,
  onChange?: Function,
  rules?: Array,
};

class SignupFormFields extends React.PureComponent<Props> {
  static defaultProps = {
    rules: [],
    form: {},
    onChange: () => {},
    password: '',
    email: '',
  };

  goToLastName = () => {
    this.lastName.input.focus();
  };

  goToEmail = () => {
    this.email.input.focus();
  };

  goToPassword = () => {
    this.password.input.focus();
  };

  render() {
    const { email, password, onChange, form, rules } = this.props;
    const { getFieldError, getFieldDecorator } = form;
    let emailErrors;
    let passwordErrors;
    return (
      <View>
        {getFieldDecorator('firstName', {
          validateTrigger: 'onBlur',
          trigger: 'onChange',
          valuePropName: 'value',
          rules: [
            // ...formConfig.basicTextRules,
            { required: true, message: 'Please enter your First Name.' },
          ],
        })(
          <BasicTextInput
            placeholder="First Name"
            onSubmitEditing={this.goToLastName}
            returnKeyType="next"
          />
        )}
        <FormInputError fieldName="firstName" getFieldError={getFieldError} />
        {getFieldDecorator('lastName', {
          validateTrigger: 'onBlur',
          trigger: 'onChange',
          valuePropName: 'value',
          rules: [
            // ...formConfig.basicTextRules,
            { required: true, message: 'Please enter your Last Name.' },
          ],
        })(
          <BasicTextInput
            onSubmitEditing={this.goToEmail}
            ref={el => (this.lastName = el)}
            returnKeyType="next"
            placeholder="Last Name"
          />
        )}
        <FormInputError fieldName="lastName" getFieldError={getFieldError} />
        {getFieldDecorator('email', {
          validateTrigger: 'onSubmit',
          trigger: 'onChangeText',
          valuePropName: 'value',
          rules: [...formConfig.defaultEmailRules],
        })(
          <EmailInput
            onSubmitEditing={this.goToPassword}
            returnKeyType="next"
            ref={el => (this.email = el)}
          />
        )}
        <FormInputError fieldName="email" getFieldError={getFieldError} />
        {getFieldDecorator('password', {
          validateTrigger: 'onSubmit',
          initialValue: password,
          trigger: 'onChangeText',
          valuePropName: 'value',
          rules: [...formConfig.defaultPasswordRules],
        })(<PasswordInput ref={el => (this.password = el)} />)}
        <FormInputError fieldName="password" getFieldError={getFieldError} />
      </View>
    );
  }
}

export default SignupFormFields;
