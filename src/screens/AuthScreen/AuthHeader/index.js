import React from 'react';
// TOP LEVEL IMPORTS
import {
  View,
  Text
} from 'react-native';
//MODULES
import { appConfig } from '../../../lib/config';
import { StyleHelpers } from '../../../lib/helpers';
import Image from 'react-native-scalable-image';
import { BodyText } from '../../../components/text';

const AuthHeader = () => {
  return (
    <View style={{padding: 15, marginBottom: 50}}>
      <Image 
        width={StyleHelpers.normalizeWidth(182)*.80}
        resizeMode='contain'
        style={{ alignSelf: 'center', marginBottom: 20 }}
        source={appConfig.images.authLogo}
      />
      <BodyText style={{color: 'rgb(136, 136, 136)', textAlign: 'center'}}>
        Connect with people you know to buy, sell, share and give away your stuff.
      </BodyText>
    </View>
  )
}

export default AuthHeader