import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import LoginForm from './LoginForm';
import SignupForm from './SignupForm';

// NAVIGATION OPTIONS
// ==============================
const navigationOptions = {
  header: null,
  tabBarVisible: false,
  headerVisible: false,
};

class AuthScreen extends React.Component {
  static navigationOptions = navigationOptions;

  state = {
    formToShow: 'login',
  };

  render() {
    const { formToShow } = this.state;

    return (
      <KeyboardAvoidingView style={{ flex: 1 }} keyboardVerticalOffset={0} behavior="padding">
        {formToShow === 'signup' && (
          <SignupForm
            toggleForm={toggle => this.setState({ formToShow: toggle })}
            {...this.props}
          />
        )}
        {formToShow === 'login' && (
          <LoginForm toggleForm={toggle => this.setState({ formToShow: toggle })} {...this.props} />
        )}
      </KeyboardAvoidingView>
    );
  }
}

export default AuthScreen;
