// @flow
import * as React from 'react';
import { View, Keyboard, TouchableWithoutFeedback, ScrollView } from 'react-native';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import KeyboardAwareComponent from './KeyboardAwareComponent';
// LIB
import { UiHelpers } from '../../lib/helpers/UiHelpers';
// COMPONENTS
import Card from '../../components/common/Card';
import CommentFormRequest from '../../components/common/Card/ItemRequestCard/ItemRequestComments/CommentForm';
import CommentFormPost from '../../components/common/Card/PostCard/PostComments/CommentForm';
import LoadingIndicator from '../../components/common/LoadingIndicator';

type Props = {
  navigation: Object,
  screenProps: Object,
  data: Object,
  markItemRequestSeen: Function,
};

const s = {
  innerContainer: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollView: {
    flex: 1,
    backgroundColor: '#fff',
  },
  outterContainer: {
    backgroundColor: '#fff',
  },
};

export default class CardDetailScreen extends React.PureComponent<Props> {
  static navigationOptions = ({ navigation, screenProps }: Props) =>
    UiHelpers.getBackNavOptions(navigation, screenProps, {
      // we pass in the full post object so that we can show the data
      // quicker, rather than waiting to get a response from DB.
      title: navigation.state.params.type === 'post' ? 'Item Detail' : 'Request Detail',
    });

  render() {
    const { navigation, screenProps, data } = this.props;
    const { loading } = data;

    const item = data.post ? data.post : data.itemRequest;
    const type = data.post ? 'post' : null;
    const autofocus = navigation.state.params.autofocus;

    if (loading) {
      return (
        <View style={{ flex: 1 }}>
          <LoadingIndicator label="Loading Post..." />
        </View>
      );
    }

    return (
      // кастомная клавиатура для паддинга бот
      <KeyboardAwareComponent>
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
          <ScrollView
            // ref={ref => (this.scrollView = ref)}
            contentContainerStyle={{ backgroundColor: '#fff', minHeight: '100%' }}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} style={s.innerContainer}>
              <Card
                key={data.post ? data.post._id : data.itemRequest._id}
                user={screenProps.data.user}
                item={item}
                detailPage
                navigation={navigation}
                {...this.props}
              />
            </TouchableWithoutFeedback>
            {console.log('type=====', type, item, autofocus)}

            {/* условия для добавления комментария */}
            {type === 'post' ? (
              <CommentFormPost parentId={item._id} autofocus={autofocus} parentModelType="post" />
            ) : (
              <CommentFormRequest
                parentId={item._id}
                autofocus={autofocus}
                parentModelType="itemRequest"
              />
            )}
          </ScrollView>
        </View>
      </KeyboardAwareComponent>
    );
  }
}
