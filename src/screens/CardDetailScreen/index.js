// @flow
import { compose, graphql } from 'react-apollo';
import { connect } from 'react-redux';
// APPOLLO
import { Post, ItemRequest } from '../../apollo';
// REDUX
import * as actions from '../../redux/actions';
//
import CardDetailScreen from './CardDetailScreen';

const mapStateToProps = ({ modals }) => ({
  modals,
});

const CardDetailScreenWithData = compose(
  graphql(ItemRequest.Mutation.markItemRequestSeen, {
    skip: ({ navigation }) => navigation.state.params.type !== 'itemRequest',
    options: ({ navigation }) => {
      const variables = {
        _id: navigation.state.params.item._id,
      };
      return { variables };
    },
    name: 'markItemRequestSeen',
  }),
  graphql(Post.Query.postById, {
    skip: ({ navigation }) => navigation.state.params.type !== 'post',
    options: ({ navigation }) => {
      const variables = {
        _id: navigation.state.params.item._id,
      };
      return { variables };
    },
  }),
  graphql(ItemRequest.Query.itemRequestById, {
    skip: ({ navigation }) => navigation.state.params.type !== 'itemRequest',
    options: ({ navigation }) => {
      const variables = {
        _id: navigation.state.params.item._id,
      };
      return { variables };
    },
  })
)(CardDetailScreen);

export default connect(mapStateToProps, actions)(CardDetailScreenWithData);
