// @flow
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { graphql, compose } from 'react-apollo';
import { User } from '../../../apollo';
import UserCard from '../../../components/common/UserCard';

const s = StyleSheet.create({
  container: {
    minHeight: 120,
    width: '100%',
  },
});

// DOCS
// =========================
/**
 *
 * @namespace FriendsOfFriends
 * @description This is a component used in the Friend's Tab
 * @memberof UsersSearch
 * @prop navigation {object} - passed down to the UserCard for use in navigating to user's profile and stuff
 * @prop data {object} - data object from apollo
 * @prop data.usersSearch {array} - an array of search results, which consist of friends (users) from our DB
 */

type Props = {
  navigation: Object,
  currentUser: Object,
  data: Object,
};

const FriendsOfFriends = ({ data, currentUser, navigation }) => {
  if (data.loading) {
    return null;
  }

  // if no friends of friends exists, exit
  if (!data.friendsOfFriends || data.friendsOfFriends.length === 0) {
    return null;
  }

  // if friends of friends exist, display user cards
  return (
    <View style={s.container}>
      {data.friendsOfFriends.map(item => (
        <UserCard
          key={item._id}
          follow={item.follow}
          targetUser={item}
          navigation={navigation}
          currentUser={currentUser}
        />
      ))}
    </View>
  );
};

const options = ({ searchText }) => {
  const params = {
    searchText, // string to search the DB with
  };
  return {
    variables: { params },
    pollInterval: 2000,
  };
};

const ComponentWithData = compose(graphql(User.Query.friendsOfFriends, { options }))(
  FriendsOfFriends
);

export default ComponentWithData;
