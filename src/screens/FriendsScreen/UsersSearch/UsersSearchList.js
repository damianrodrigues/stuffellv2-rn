import React from 'react';
import { View, Text } from 'react-native';
// APOLLO
import { graphql, compose } from 'react-apollo';
import { Friends } from '../../../apollo';
// COMPONENTS
import UserCard from '../../../components/common/UserCard';
import Result from '../../../components/common/Result';

// DOCS
// =========================
/**
 *
 * @namespace UsersSearchList
 * @description This is a component used in the Friend's Tab
 * @memberof UsersSearch
 * @prop navigation {object} - passed down to the UserCard for use in navigating to user's profile and stuff
 * @prop data {object} - data object from apollo
 * @prop data.usersSearch {array} - and array of friends (users) from our DB
 */
class UsersSearchList extends React.PureComponent {
  renderUsers = () => {
    const { data, navigation, currentUser } = this.props;

    if (data.usersSearch && data.usersSearch.length !== 0) {
      return (
        <View
          style={{
            flex: 1,
          }}>
          {data.usersSearch.map(item => (
            <UserCard
              key={item._id}
              follow={item.follow}
              targetUser={item}
              navigation={navigation}
              currentUser={currentUser}
            />
          ))}
        </View>
      );
    }
    return <Text>Searching...</Text>;
  };

  render() {
    const { data, searchText } = this.props;

    if (data.loading) {
      return <Text>Searching...</Text>;
    }

    console.log(
      'data.usersSearch.length === 0 && searchText.length === 0',
      data.usersSearch.length === 0 && searchText.length === 0
    );

    if (data.usersSearch.length === 0 && searchText.length === 0) {
      return <Result description="As long as you have no friends" />;
    }

    if (data.usersSearch && data.usersSearch.length === 0) {
      return (
        <Result description="No users matched your search terms..." title="Hmm..." type="empty" />
      );
    }

    return this.renderUsers();
  }
}

const options = props => {
  const { searchText } = props;
  const params = {
    searchText,
  };

  return {
    variables: { params },
    pollInterval: 2000,
  };
};

export default compose(graphql(Friends.Query.usersSearch, { options }))(UsersSearchList);
