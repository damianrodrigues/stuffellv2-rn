// @flow
import React from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { connect } from 'react-redux';
import * as actions from '../../../redux/actions';
import UsersSearchList from './UsersSearchList';
import FriendsOfFriends from './FriendsOfFriends';

// DOCS
// =========================
/**
 *
 * @namespace UsersSearch
 * @description This is a component used in the Friend's Tab
 * @prop searchText {string} - this is a string used to search for friends in the datbase. it's stored in redux usually.
 */

type Props = {
  navigation: Object,
  screenProps: Object,
  searchText: string,
};

type State = {
  showFriendsOf: boolean,
};

class UsersSearch extends React.PureComponent<Props, State> {
  state = {
    showFriendsOf: false,
  };

  render() {
    const { showFriendsOf } = this.state;
    const {
      screenProps: { data: { user } },
      navigation,
      searchText,
      onFriendSearchTextChange,
    } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <View style={{ height: 58 }}>
          <SearchBar
            onChangeText={val => onFriendSearchTextChange(val)}
            // onBlur={val => {
            //   if (val.length > 0) {
            //     return this.setState({ showFriendsOf: false });
            //   }

            //   return this.setState({ showFriendsOf: true });
            // }}
            onClear={() => onFriendSearchTextChange('')}
            inputStyle={{ backgroundColor: '#fff' }}
            placeholder="Search..."
            lightTheme
          />
        </View>
        <ScrollView style={{ paddingRight: 8, paddingLeft: 8 }}>
          <View style={{ marginTop: 10 }}>
            <FriendsOfFriends navigation={navigation} searchText={searchText} currentUser={user} />
          </View>
          {!showFriendsOf && (
            <UsersSearchList navigation={navigation} currentUser={user} searchText={searchText} />
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ search }) => ({
  searchText: search.searchText_friends,
});

export default connect(mapStateToProps, actions)(UsersSearch);
