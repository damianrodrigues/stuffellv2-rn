// @flow
import { graphql, compose } from 'react-apollo';
import { Follow } from '../../apollo/Follow';
import FriendsScreen from './FriendsScreen';

export default compose(
  graphql(Follow.Query.incomingFollows, {
    options: {
      variables: {
        offset: 1000,
      },
    },
    pollInterval: 2000,
    name: 'followers',
  })
)(FriendsScreen);
