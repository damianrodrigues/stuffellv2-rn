import { Follow } from '../../../apollo/Follow';

export default {
  request: {
    cache: false,
    query: Follow.Query.incomingFollows,
  },
  result: {
    followers: {
      incomingFollows: [],
    },
  },
};
