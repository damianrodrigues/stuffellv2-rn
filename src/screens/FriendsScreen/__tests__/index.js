import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import configureStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import FriendsScreen from '../index';
import data from './data';

const mocks = [data];
const instance = shallow(
  <MockedProvider mocks={mocks} addTypename={false}>
    <FriendsScreen
      store={configureStore()({})}
      screenProps={{
        data: {
          user: {
            _id: 'TgcTAeWywrrtKBamQ',
            emails: [
              {
                address: 'bugdonepavlov@gmail.com',
                verified: false,
                __typename: 'Email',
              },
            ],
            roles: null,
            numberOfFollowers: 3,
            numberFollowing: 3,
            numberOfPosts: 3,
            following: 'none',
            follow: null,
            profile: {
              firstName: 'Bogdan',
              lastName: 'pavlov',
              image: null,
              description: null,
              __typename: 'Profile',
            },
            __typename: 'User',
          },
        },
      }}
    />
  </MockedProvider>
);

describe('FriendsScreen views', () => {
  it('renders FriendsScreen', async () => {
    await new Promise(resolve => setTimeout(resolve));
    instance.update();

    expect(instance.exists()).toBeTruthy();

    expect(instance).toMatchSnapshot();
  });
});
