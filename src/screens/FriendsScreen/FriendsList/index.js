import { graphql, compose } from 'react-apollo';
import { Friends } from '../../../apollo';
import FriendsList from './FriendsList';

const options = ({ searchText }) => ({
  variables: {
    params: {
      searchText,
    },
  },
  pollInterval: 2000,
});

export default compose(
  graphql(Friends.Query.getFriendsAndOutgoing, {
    options,
    name: 'friendsAndOutgoing',
  })
)(FriendsList);
