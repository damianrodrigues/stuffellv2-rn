// @flow
import React from 'react';
import { ScrollView, ActivityIndicator } from 'react-native';
import UserCard from '../../../components/common/UserCard';

type Props = {
  navigation: Object,
  screenProps: Object,
  friendsAndOutgoing: Object,
};

export default class FriendsList extends React.PureComponent<Props> {
  render() {
    const { friendsAndOutgoing, navigation, screenProps: { data: { user } } } = this.props;

    if (friendsAndOutgoing.loading) {
      return <ActivityIndicator size="large" />;
    }

    if (
      !friendsAndOutgoing.getFriendsAndOutgoing ||
      friendsAndOutgoing.getFriendsAndOutgoing.length === 0
    ) {
      return null;
    }

    if (!user) {
      return null;
    }

    return (
      <ScrollView style={{ flex: 1 }}>
        {friendsAndOutgoing.getFriendsAndOutgoing.map(
          item =>
            item.following !== 'none' ? (
              <UserCard
                follow={item.follow}
                key={item._id}
                incoming={false}
                targetUser={item}
                currentUser={user}
                navigation={navigation}
                label="FOLLOWING"
              />
            ) : null
        )}
      </ScrollView>
    );
  }
}
