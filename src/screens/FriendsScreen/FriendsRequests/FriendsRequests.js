// @flow
import React from 'react';
import { View } from 'react-native';
import Result from '../../../components/common/Result';
import UserCard from '../../../components/common/UserCard';

type Props = {
  navigation: Object,
  screenProps: Object,
  followers: Object,
};

export default class FriendsRequest extends React.PureComponent<Props> {
  render() {
    const { navigation, followers, screenProps: { data: { user } } } = this.props;

    if (!followers || followers.loading) {
      return null;
    }

    // if there are no follows (after loading), then show an empty state
    if (!followers.incomingFollows || followers.incomingFollows.length === 0) {
      return (
        <Result description="No incoming requests at the moment." title="Hmm..." type="empty" />
      );
    }

    return (
      <View>
        {followers.incomingFollows.map(
          follow =>
            !follow.owner.accepted ? (
              <UserCard
                follow={follow}
                navigation={navigation}
                key={follow.owner._id}
                incoming
                targetUser={follow.owner}
                currentUser={user}
              />
            ) : null
        )}
      </View>
    );
  }
}
