// @flow
import * as React from 'react';
import { View, Animated, StyleSheet, Dimensions } from 'react-native';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';
import { styleConstants } from '../../lib/config';
import { UiHelpers } from '../../lib/helpers';
import FriendsRequests from './FriendsRequests';
import UsersSearch from './UsersSearch';
import FriendsList from './FriendsList';

type Props = {
  navigation: Object,
  screenProps: Object,
};

type State = {
  navigationOptions: Object,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

export default class FriendsScreen extends React.PureComponent<Props, State> {
  static navigationOptions = ({ navigation, screenProps }) =>
    UiHelpers.getHomeNavOptions(navigation, screenProps, {
      title: 'Friends',
    });

  state = {
    navigationOptions: {
      index: 0,
      routes: [
        { key: 'first', title: 'Invites' },
        { key: 'second', title: 'People' },
        { key: 'third', title: 'Friends' },
      ],
    },
  };

  handleIndexChange = index => {
    const { navigationOptions } = this.state;
    this.setState({
      navigationOptions: {
        ...navigationOptions,
        index,
      },
    });
  };

  renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return (
          <View style={[styles.container, { backgroundColor: '#fff' }]}>
            <FriendsRequests {...this.props} />
          </View>
        );
      case 'second':
        return (
          <View style={[styles.container, { backgroundColor: '#fff' }]}>
            <UsersSearch {...this.props} />
          </View>
        );
      case 'third':
        return (
          <View style={[styles.container, { backgroundColor: '#fff' }]}>
            <FriendsList {...this.props} />
          </View>
        );

      default:
        return null;
    }
  };

  renderHeader = props => (
    <TabBar
      indicatorStyle={{
        backgroundColor: styleConstants.color.primary,
      }}
      style={{
        backgroundColor: styleConstants.color.screenBackground,
        height: 45,
      }}
      tabStyle={{
        height: 45,
      }}
      renderLabel={this.renderLabel(props)} // renderer of a single Lable on the tab bar
      {...props}
    />
  );

  renderLabel = props => ({ route, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const outputRange = inputRange.map(
      inputIndex =>
        inputIndex === index ? styleConstants.color.primary : styleConstants.color.black1
    );
    const color = props.position.interpolate({
      inputRange,
      outputRange,
    });

    return (
      <Animated.Text
        style={[
          {
            fontFamily: styleConstants.fontFamily.regular,
            fontSize: styleConstants.fontSize.md,
          },
          { color },
        ]}>
        {route.title}
      </Animated.Text>
    );
  };

  render() {
    const { navigationOptions } = this.state;
    const { followers } = this.props;

    console.log('=======props', this.props);

    return (
      !followers.loading && (
        <TabViewAnimated
          style={styles.container}
          navigationState={navigationOptions}
          renderScene={this.renderScene}
          renderHeader={this.renderHeader}
          onIndexChange={this.handleIndexChange}
          initialLayout={initialLayout}
        />
      )
    );
  }
}
