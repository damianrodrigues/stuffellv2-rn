import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
// LIB


class OfflineScreen extends React.PureComponent {
    render(){
        return (
            <View style={s.container}>
                <View style={s.imageContainer}>
                    {/* <Image 
                        source={require('../../../../assets/ocs-logo-no-text.png')} 
                        style={s.image} 
                    /> */}
                </View>
                <Text>
                    Offline Screen
                </Text>
            </View>
        )
    }
};

const s = StyleSheet.create({
    conatainer: {
        flex: 1
    },
    image: {
        width: 156, // todo: make this responsive to screen size
        height: 181, // todo: make this responsive to screen size
        marginTop: 40, // todo: make this responsive to screen size
    },
    imageContainer: {
        flex: 2,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
});

export default OfflineScreen;