// FORM RULES
// ====================================

const DEFAULT_EMAIL_RULES = [
  { required: true, message: 'Enter your Email.' },
  { type: 'email', message: 'Not a valid email address.' },
  { max: 100, message: 'Must be no more than 100 characters.' },
];


const DEFAULT_PASSWORD_RULES = [
  {
    min: 6, 
    message: 'Minimum six characters',
  },
  {
    required: true,
    message: 'Please enter your Password.',
  },
  {
    max: 25,
    message: 'Must be no more than 25 characters.',
  },
];

const DEFAULT_BASIC_TEXT_RULES = [
  {
    required: true,
    message: 'This field is required.',
  },
  {
    min: 3,
    message: 'This must be at least 3 characters',
  },
  {
    max: 100,
    message: 'This must be less than 100 characters',
  },
];

export const formConfig = {
  defaultEmailRules: DEFAULT_EMAIL_RULES,
  defaultPasswordRules: DEFAULT_PASSWORD_RULES,
  basicTextRules: DEFAULT_BASIC_TEXT_RULES,
};
