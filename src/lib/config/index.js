export * from './formConfig';
export * from './stylesConfig';
export * from './styleConstants';

export const appConfig = {
  s3: {
    bucketName: 'stuffell-west',
    url: `https://stuffell-west.s3.amazonaws.com/`,
    region: 'us-west-2',
    accessKey: 'AKIAIAPLTGW4CXVHGXCA',
    secretKey: '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',
    cdn: 'https://d8nd371nnttyb.cloudfront.net',
  },
  sentry: {
    dsn: 'https://2f493d3ed4e349e6b461ae5423d9b210@sentry.io/267833',
  },
  images: {
    whiteLogo: require('../../../assets/images/logo-white.png'),
    authLogo: require('../../../assets/images/logo-blue.png'),
    defaultAvatar: require('../../../assets/images/avatar.jpg'),
  },
  // TODO: Not sure if that's the right URL, but here's a temp solution
  apiUrl: 'https://www.stuffell.com',
};

export const CATEGORY_OPTIONS = [
  // FIRST
  'Art, Crafts, Handmade',
  'Baby & Kids',
  'Beauty & Health',
  'Books, CDs & DVDs',
  'Cars, Vehicles, Boats & Parts',
  'Cell Phones & Tablets',
  'Clothes, Shoes & Jewelry',
  'Computers & Accessories',
  'Electronics & Photography',
  'Food & Produce',
  'Furniture',
  'Home & Kitchen',
  'Pets & Supplies',
  'Services / For Hire',
  'Sports, Outdoors & Fitness',
  'Tickets',
  'Tools & Garden',
  'Toys & Video Games',
  'General / Other',
];
