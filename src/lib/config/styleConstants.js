import { Platform } from 'react-native';
import { StyleHelpers } from '../helpers/StyleHelpers';

export const xl = 20;
export const lg = 17;
export const md = 15;
export const sm = 13;
export const xs = 12;

export const styleConstants = {
  color: {
    primary: '#007bb5',
    buttonBorder: '#666',
    screenBackground: '#FFF',
    screenBackgroundGrey: '#EBEBEB',
    errorColor: 'red',
    // greys
    buttonPendingColor: '#999',
    // blacks
    black: 'rgba(0, 0 , 0, 1)', // android standard for primary text  (use in segmented control and total section)
    black1: 'rgba(0, 0 , 0, .87)', // android standard for primary text  (use in segmented control and total section)
    black2: 'rgba(0, 0 , 0, .72)', // non-standard for primary text
    black3: 'rgba(0, 0 , 0, .54)', // android standard for helper text
    black4: 'rgba(0, 0 , 0, .38)', // android standard for disabled text
    black5: 'rgba(0, 0 , 0, .12)', // for the divider line
  },
  headerHeight: StyleHelpers.normalizeHeight(50),
  headerTitleFontSize: StyleHelpers.normalizeWidth(18),

  subHeaderHeight: StyleHelpers.normalizeHeight(60),
  //
  defaultMargin: StyleHelpers.normalizeWidth(10),
  defaultDoubledMargin: StyleHelpers.normalizeWidth(10),
  // fontFamily
  fontFamily: {
    light: Platform.OS === 'android' ? 'Roboto' : 'System',
    regular: Platform.OS === 'android' ? 'Roboto' : 'System',
    medium: Platform.OS === 'android' ? 'Roboto' : 'System',
    semibold: Platform.OS === 'android' ? 'Roboto' : 'System',
    bold: Platform.OS === 'android' ? 'Roboto' : 'System',
  },
  //
  // font weights
  fontWeight: {
    light: Platform.OS === 'android' ? '300' : '300',
    regular: Platform.OS === 'android' ? '400' : '400',
    medium: Platform.OS === 'android' ? '500' : '500',
    semibold: Platform.OS === 'android' ? '600' : '600',
    bold: Platform.OS === 'android' ? '700' : '700',
  },
  //
  // font sizes
  fontSize: {
    xl: StyleHelpers.normalizeWidth(xl),
    lg: StyleHelpers.normalizeWidth(lg),
    md: StyleHelpers.normalizeWidth(md),
    sm: StyleHelpers.normalizeWidth(sm),
    xs: StyleHelpers.normalizeWidth(xs),
  },
};
