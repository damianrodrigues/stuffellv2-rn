import { styleConstants } from './styleConstants';

export const stylesConfig = {
  basicHeaderStyle: {
    height: styleConstants.headerHeight,
    borderColor: styleConstants.color.primary,
    backgroundColor: styleConstants.color.primary, // '#fff',
  },
  titleStyle: {
    fontFamily: styleConstants.fontFamily.bold,
    fontSize: styleConstants.headerTitleFontSize,
    color: '#fff',
  },
  errorLabelStyle: {
    marginLeft: 0,
    marginRight: 0,
    // fontFamily: styleConstants.fontFamily.regular,
    color: styleConstants.color.errorColor,
  },
};
