


export const reduxStore = {

};


// mock fetch for the components that need it... not sure best practice right now in react-native world
// https://stackoverflow.com/questions/36069731/how-to-unit-test-api-calls-with-mocked-fetch-in-react-native-with-jest/36071387
export const mockedFetch = ({ jsonToReturn={} }) => jest.fn(() => new Promise(resolve => resolve({
    ok: true,
    json: () => new Promise(resolve => resolve(jsonToReturn))
})));


