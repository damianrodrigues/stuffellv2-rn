import { Dimensions, Platform, PixelRatio } from 'react-native';

const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');

const StyleHelpers = {};
/**
 * @name normalizeWidth
 * @description returns normalized width based on scaling by screen width
 * @namespace getBackNavOptions
 * @memberof StyleHelpers
 * @param size {number} - provided by navigationOptions/react-navigation
 */
StyleHelpers.normalizeWidth = size => {
  //Guideline sizes are based on standard ~5" screen mobile device
  const scale = SCREEN_WIDTH / 350;
  // normalize by multiplying by the scale, and rounding.
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(size * scale));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(size * scale)) - 2;
  }
};

/**
 * @name normalizeHeight
 * @description returns normalized width based on scaling by screen width
 * @namespace getBackNavOptions
 * @memberof StyleHelpers
 * @param size {number} - provided by navigationOptions/react-navigation
 */
StyleHelpers.normalizeHeight = size => {
  let heightScale = SCREEN_HEIGHT / 667; //https://mydevice.io/devices/
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(size * heightScale));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(size * heightScale)) - 2;
  }
};

export { StyleHelpers };
