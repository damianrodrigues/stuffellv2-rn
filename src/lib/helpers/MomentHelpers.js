import moment from 'moment';
import timezone from 'moment-timezone';

// create MomentHelpers name space
const MomentHelpers = {};

// Get current user time zone
const USER_TIMEZONE = timezone.tz.guess() || 'America/New_York';

//HOW LONG AGO FROM NOW
//formats time ad X minutes ago
MomentHelpers.timeAgo = date => {
  var now = new Date();
  timezone(date)
    .tz(USER_TIMEZONE)
    .format('MMM DD h:mm A');
  var nowWrapper = timezone(now).tz(USER_TIMEZONE); //moment(now);
  var pastDateWrapper = timezone(date).tz(USER_TIMEZONE); //moment(date);
  var displayDate = pastDateWrapper.from(nowWrapper);
  return displayDate; // timezone(displayDate).tz(USER_TIMEZONE)
};

MomentHelpers.imessageTime = date => {
  return timezone(date)
    .tz(USER_TIMEZONE)
    .format('MMM DD h:mm A');
};

export { MomentHelpers };
