import { RNS3 } from 'react-native-aws3'
import { appConfig } from '../config'
import { Image } from 'react-native'

let keyPrefix = new Date().valueOf()

const s3options = {
  keyPrefix: `mobile`,
  bucket: appConfig.s3.bucketName,
  region: appConfig.s3.region,
  accessKey: appConfig.s3.accessKey,
  secretKey: appConfig.s3.secretKey,
  successActionStatus: 201
}

const UploadHelpers = {}
// import {Image} from "react-native-expo-image-cache";

// const preview = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg==";
// const uri = "https://firebasestorage.googleapis.com/v0/b/react-native-e.appspot.com/o/b47b03a1e22e3f1fd884b5252de1e64a06a14126.png?alt=media&token=d636c423-3d94-440f-90c1-57c4de921641";
// <Image style={{ height: 100, width: 100 }} {...{preview, uri}} />
UploadHelpers.handleFileUpload = (file, callback) => {
  let fileToUpload = {
    uri: file.uri,
    name: new Date().valueOf(),
    type: 'image/png'
  }

  console.log('UploadHelpers.handleFileUpload')
  console.log(fileToUpload)

  if (!fileToUpload.uri) { return null } 

  RNS3.put(fileToUpload, s3options)
  .then(
    ({ status, body }) => {
      if (status !== 201) {
        console.error('Failed to upload image to S3')
        callback({ reason: 'Failed to upload image to S3' })
      }
      // s3 returns a url, we want to slighlty change that, becuase we will want to grab it form our cdn.
      // so we strip out the c3 portion of the url and put in our CDN url as the base url.
      //let cleanUrl = body.postResponse.location.replace(appConfig.s3.url, '');
      //let cdnUrl = `${appConfig.s3.cdn}/${cleanUrl}`;
      // prefetch and cache the URL via react-native
      Image.prefetch(body.postResponse.location)
      callback(null, body.postResponse.location)
    }
  ).catch(err => {
    console.log(err)
    callback(err)
  })

}

export { UploadHelpers }
