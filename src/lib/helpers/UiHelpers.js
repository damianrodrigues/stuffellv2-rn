import React from 'react';
import * as R from 'ramda';
import { Text, Platform } from 'react-native';
import InviteModal from '../../components/common/InviteModal/index';
import AccountButton from '../../components/common/AccountButton/index';
import BackButton from '../../components/common/BackButton/index';
import { stylesConfig } from '../config';

// DOCS
// =========================
/**
 * @name UiHelpers
 * @description a set of reusable helpers function to be used around the app.
 * These will be broken into separate files as patterns/groupings become apparent.
 * @namespace UiHelpers
 */

const UiHelpers = {};

/**
 * @name arrayExists
 * @description returns a boolean of whether the array exists or not,
 * usually I use this to check if I should show a list or a empty state
 * @namespace arrayExists
 * @memberof UiHelpers
 * @param arr {array} - an array property to check
 */
UiHelpers.arrayExists = arr => Array.isArray(arr) && arr.length;

/**
 * @name getAvailability
 * @description provide this function an array of the selected statuses and a price (if the price exists),
 * and this function returns a string of text that can be shown on the PostCard
 * @namespace arrayExists
 * @memberof UiHelpers
 * @param statuses {array} - an array of the current item/post's statuses
 * @param price {string} - a string of the price (if it exists)
 */

UiHelpers.getAvailability = ({ statuses, price }) => {
  let text = '';

  if (!statuses) {
    return '';
  }

  // if it's for sale and the price exists
  if (statuses.includes('For Sale') && price) {
    text = `Buy for $${price}`;
  }

  // if it's for sale and there is no price
  if (statuses.includes('For Sale') && !price) {
    text = `Buy for ?`;
  }

  // if it's just for borrow
  if (statuses.includes('For Borrow')) {
    text = `Borrow`;
  }

  // if it's just for free
  if (statuses.includes('For Free')) {
    text = `Free`;
  }

  // if it's for free and for borrow
  if (statuses.includes('For Free') && statuses.includes('For Borrow')) {
    text = `Free or Borrow`;
  }

  if (statuses.includes('For Sale') && statuses.includes('For Borrow') && price) {
    text = `Buy for $${price} or Borrow`;
  }
  if (statuses.includes('For Sale') && statuses.includes('For Borrow') && !price) {
    text = `Buy for ? or Borrow`;
  }
  if (statuses.includes('Just for Looking')) {
    text = `Just for looking`;
  }

  return <Text>{text}</Text>;
};

/**
 * @name getHomeNavOptions
 * @description returns the object to be used in static navigationOptions for all Home (top level) screens.
 * @namespace getHomeNavOptions
 * @memberof UiHelpers
 * @param navigation {object} - provided by navigationOptions/react-navigation
 * @param screenProps {object} - provided by navigationOptions/react-navigation
 * @param drawerLabel {string} - this is the label that will show up on the drawer
 */
UiHelpers.getHomeNavOptions = (navigation, screenProps, options) => ({
  headerTitleStyle: stylesConfig.titleStyle,
  headerVisible: Platform.OS !== 'android',
  headerTitleAllowFontScaling: false,
  headerStyle: stylesConfig.basicHeaderStyle,
  headerRight: <AccountButton navigation={navigation} />,
  headerLeft: <InviteModal navigation={navigation} screenProps={screenProps} />,
  ...options,
});

/**
 * @name getBackNavOptions
 * @description returns the object to be used in static navigationOptions
 * for screens that are deeper in the StackNavigator and need a back/cancel button
 * @namespace getBackNavOptions
 * @memberof UiHelpers
 * @param navigation {object} - provided by navigationOptions/react-navigation
 * @param screenProps {object} - provided by navigationOptions/react-navigation
 * @param drawerLabel {string} - this is the label that will show up on the drawer
 */
UiHelpers.getBackNavOptions = (navigation, screenProps, options) => ({
  headerTitleStyle: stylesConfig.titleStyle,
  headerVisible: Platform.OS !== 'android',
  headerStyle: stylesConfig.basicHeaderStyle,
  tabBarVisible: false,
  headerTitleAllowFontScaling: false,
  headerLeft: <BackButton onPress={() => navigation.goBack()} />,
  ...options,
});

/**
 * @name getFullName
 * @description pass in a user object and get back a string of the users full name
 * @namespace getBackNavOptions
 * @memberof UiHelpers
 * @param size {number} - provided by navigationOptions/react-navigation
 */
UiHelpers.getFullName = user => {
  if (!user) {
    return null;
  }
  return `${user.profile.firstName} ${user.profile.lastName}`;
};

/**
 * @name mergeArrays
 * @description pass two arrays to merge into one
 * @namespace mergeArrays
 * @memberof UiHelpers
 * @param array
 */
UiHelpers.mergeArrays = (arr1, arr2) => R.concat(arr1, arr2);
//
UiHelpers.orderArrayByDate = arr => R.sort(R.descend(R.prop('createdAt')), arr);

/**
 * @name normalizeWidth
 * @description returns normalized width based on scaling by screen width
 * @namespace getBackNavOptions
 * @memberof UiHelpers
 * @param size {number} - provided by navigationOptions/react-navigation
 */
// UiHelpers.normalizeWidth = (size) => {
//   let {
//     width: SCREEN_WIDTH,
//     height: SCREEN_HEIGHT,
//   } = Dimensions.get('window');
//   //Guideline sizes are based on standard ~5" screen mobile device
//   let scale = SCREEN_WIDTH / 350;
//   // normalize by multiplying by the scale, and rounding.
//   if (Platform.OS === 'ios') {
//     return Math.round(PixelRatio.roundToNearestPixel(size*scale))
//   } else {
//     return Math.round(PixelRatio.roundToNearestPixel(size*scale))-2
//   }
// }

/**
 * @name normalizeHeight
 * @description returns normalized width based on scaling by screen width
 * @namespace getBackNavOptions
 * @memberof UiHelpers
 * @param size {number} - provided by navigationOptions/react-navigation
 */
// UiHelpers.normalizeHeight = (size) => {
//   let {
//     width: SCREEN_WIDTH,
//     height: SCREEN_HEIGHT,
//   } = Dimensions.get('window');
//   let heightScale = SCREEN_HEIGHT / 667; //https://mydevice.io/devices/
//   if (Platform.OS === 'ios') {
//     return Math.round(PixelRatio.roundToNearestPixel(size*heightScale))
//   } else {
//     return Math.round(PixelRatio.roundToNearestPixel(size*heightScale))-2
//   }
// }

export { UiHelpers };
