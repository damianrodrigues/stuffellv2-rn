import { ImageManipulator } from "expo";



const RESIZE_OPTIONS = [
  { resize: { width: 75, height: 75 } },
  { crop: { originX: 0, originY: 0, width: 75, height: 75 } }
]

const SAVE_OPTIONS = { 
  compress: .5,
  format: "jpeg",
  base64: true
}

const ImageHelpers = {};



ImageHelpers.makeAvatarSmaller = (uri) => new Promise(async (resolve, reject) => {
  let manipResult;
  console.log('ImageHelpers.makeAvatarSmaller ==========> ')
  console.log(uri)
  try {
    manipResult = await ImageManipulator.manipulate(
      uri, 
      RESIZE_OPTIONS, 
      SAVE_OPTIONS
    );
  }
  catch(err){
    console.log('there was an error inside of ImageHelpers.makeAvatarSmaller ')
    reject(err)
  }

  console.log(manipResult.uri)
  console.log('END ImageHelpers.makeAvatarSmaller ==========> ')
  resolve(manipResult);

});



export default ImageHelpers;
  