import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { AppRoutes } from '../index';

const instance = shallow(<AppRoutes store={configureStore()({})} />);

describe('<AppRoutes />', () => {
  it('<AppRoutes /> should render', () => {
    expect(instance.exists()).toBeTruthy();
    expect(instance).toMatchSnapshot();
  });
});
