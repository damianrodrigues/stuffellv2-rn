// TOP LEVEL IMPORTS
import React from 'react';
import { AppLoading } from 'expo';
import { View, Image, Text } from 'react-native';
// REDUX
import { connect } from 'react-redux';
import { graphql, compose } from 'react-apollo';
import * as actions from '../redux/actions';
// NAVIGATORS
import TopLevelNavigator from './TopLevelNavigator';
import AuthNavigator from './AuthNavigator';
import OfflineScreen from '../screens/OfflineScreen';
// APOLLO
import ApolloClient from '../apollo/ApolloClient';
import { User } from '../apollo';

const ErrorScreen = () => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text>Something went wrong.</Text>
  </View>
);
// EXPORTED COMPONENT
// =================================
export class AppRoutes extends React.Component {
  state = {
    authenticated: false,
    authenticating: true,
  };

  componentDidMount() {
    this.checkUserToken();
  }

  onSuccessfulTokenCheck = ({ data }) => {
    if (data.user && data.user._id) {
      // check if the user has a profile image, if yes, then prefetch it.
      if (data.user.profile && data.user.profile.image) {
        Image.prefetch(data.user.profile.image);
      }
      // if the user _id exists, the user is authenticated.
      return this.setState({ authenticated: true, authenticating: false });
    }
    // if the user _id does not exist, we assum ehe is not authenticated
    return this.setState({ authenticated: false, authenticating: false });
  };

  onFailedTokenCheck = () => {
    this.setState({ authenticated: false, authenticating: false });
  };

  onLogin = async () => {
    await ApolloClient.resetStore();
    this.setState({ authenticated: true });
  };

  onLogout = async () => {
    this.props.userLogout();
    this.setState({ authenticated: false });
    await ApolloClient.resetStore();
  };

  checkUserToken = async () => {
    this.setState({ authenticating: true });
    let result;
    try {
      // query to see if current use is logged in
      result = await ApolloClient.query({ query: User.Query.getCurrentUser });
      // see if there was a user in the result
      if (!result.data.user) {
        return this.onFailedTokenCheck();
      }
      //
      return this.onSuccessfulTokenCheck(result);
    } catch (err) {
      console.log('checkUserToken ===========> error');
      console.log(err);
      return this.onFailedTokenCheck();
    }
  };

  render() {
    console.log(this.props);
    // if the app is still loading or we havne't checked their authentication status,
    // then show the expo AppLoading component
    if (!this.props.network || !this.props.network.isConnected) {
      return <OfflineScreen />;
    }

    // appIsReady: are we stil loading assets and fonts? Show AppLoading
    // data.loading: are we stil waiting to get the user query? Show AppLoading
    // authenticating: are we stil authenticating if the user is signed in? Show AppLoading
    if (this.props.data.loading || this.state.authenticating) {
      return <AppLoading />;
    }

    if (this.props.data.error) {
      <ErrorScreen />;
    }

    // authenticated: is the user not authenticated? Then show them login/signup
    if (!this.state.authenticated) {
      return (
        <AuthNavigator
          onNavigationStateChange={null}
          screenProps={{
            checkUserToken: this.checkUserToken,
            onLogin: this.onLogin,
            onSignup: this.onSignup,
            ...this.props,
          }}
        />
      );
    }

    if (!this.props.data.user) {
      return (
        <View>
          <Text>Something went wrong.</Text>
        </View>
      );
    }

    // TODO: when can we prefetch the user's avatar?
    return (
      <TopLevelNavigator
        onNavigationStateChange={null}
        screenProps={{
          ...this.props,
          onLogout: this.onLogout,
        }}
      />
    );
  }
}

const mapStateToProps = ({ network }) => ({
  network,
});

export default compose(
  graphql(User.Query.getCurrentUser, {
    fetchPolicy: 'network-only',
    options: {
      fetchPolicy: 'network-only',
    },
  }),
  connect(
    mapStateToProps,
    actions
  )
)(AppRoutes);
