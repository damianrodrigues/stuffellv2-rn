// @flow
import { createStackNavigator } from 'react-navigation';
// SCREENS
import AuthScreen from '../../screens/AuthScreen';
//
// MainNavigator
// =================================

const MAIN_NAVIGATOR_OPTIONS = {
  lazy: true,
  swipeEnabled: false,
  navigationOptions: {
    tabBarVisible: false,
    headerStyle: {
      marginTop: 24,
    },
  },
  headerMode: 'none',
  initialRouteName: 'auth',
};

const MAIN_NAVIGATOR_ROUTES = {
  auth: {
    screen: AuthScreen,
    path: '/auth',
  },
};

export default createStackNavigator(MAIN_NAVIGATOR_ROUTES, MAIN_NAVIGATOR_OPTIONS);
