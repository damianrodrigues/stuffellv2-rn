// @flow
import { createStackNavigator } from 'react-navigation';
// SCREENS
import AppNavigator from './AppNavigator';
import ModalNavigator from './ModalNavigator';
//
// MainNavigator
// =================================

const MAIN_NAVIGATOR_OPTIONS = {
  lazy: false,
  swipeEnabled: true,
  navigationOptions: {
    headerStyle: {
      marginTop: 24,
    },
  },
  headerMode: 'none',
  initialRouteName: 'main',
};

const RootStack = createStackNavigator(
  {
    home: {
      screen: AppNavigator,
    },
    modalScreen: {
      screen: ModalNavigator,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    headerMode: 'none',
    mode: 'modal',
  }
);

const MAIN_NAVIGATOR_ROUTES = {
  main: {
    screen: RootStack,
    path: '/main',
  },
};

export default createStackNavigator(MAIN_NAVIGATOR_ROUTES, MAIN_NAVIGATOR_OPTIONS);
