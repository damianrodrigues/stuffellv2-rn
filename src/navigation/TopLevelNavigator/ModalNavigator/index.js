// TOP LEVEL IMPORTS
import { createStackNavigator } from 'react-navigation';
// SCREENS
import ModalScreen from '../../../screens/ModalScreen';
import SelectOptionsScreen from '../../../screens/SelectOptionsScreen';

// Navigator
// =================================
const NewPostNavigator = createStackNavigator({
  // modal is a re-usable screen where we pass in a param
  modal: {
    screen: ModalScreen,
  },

  // selectOptions is a re-usable screen where we pass in a param
  // to display different options (usually a list of checkboxes)
  selectOptions: {
    screen: SelectOptionsScreen,
  },
});

// EXPORTED NAVIGATOR
// =================================
export default NewPostNavigator;
