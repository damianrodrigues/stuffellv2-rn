// @flow
import { createStackNavigator } from 'react-navigation';
// SCREENS
import SearchScreen from '../../../screens/SearchScreen';
import UserDetailScreen from '../../../screens/UserDetailScreen';
import CardDetailScreen from '../../../screens/CardDetailScreen';
import AccountNavigator from './AccountNavigator';

const SearchNavigator = createStackNavigator(
  {
    search: {
      screen: SearchScreen,
    },
    cardDetail: { screen: CardDetailScreen },
    userDetail: { screen: UserDetailScreen },
    account: {
      screen: AccountNavigator,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    headerMode: 'screen',
  }
);

SearchNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default SearchNavigator;
