// import React from 'react';
import { createStackNavigator } from 'react-navigation';
// SCREENS
import AccountScreen from '../../../screens/AccountScreen';
import EditProfileScreen from '../../../screens/EditProfileScreen';
import HelpScreen from '../../../screens/HelpScreen';

const AccountNavigator = createStackNavigator(
  {
    top: {
      screen: AccountScreen,
    },
    help: { screen: HelpScreen },
    editProfile: { screen: EditProfileScreen },
  },
  {
    swipeEnabled: true,
    headerMode: 'screen',
  }
);

AccountNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default AccountNavigator;
