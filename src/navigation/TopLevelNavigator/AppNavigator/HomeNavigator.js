// @flow
import { createStackNavigator } from 'react-navigation';
// SCREENS
import HomeScreen from '../../../screens/HomeScreen';
import AccountNavigator from './AccountNavigator';
import UserDetailScreen from '../../../screens/UserDetailScreen';
import CardDetailScreen from '../../../screens/CardDetailScreen';

const HomeNavigator = createStackNavigator(
  {
    top: { screen: HomeScreen },
    cardDetail: { screen: CardDetailScreen },
    userDetail: { screen: UserDetailScreen },
    account: {
      screen: AccountNavigator,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    headerMode: 'screen',
  }
);

HomeNavigator.navigationOptions = ({ navigation }) => {
  const { routeName } = navigation.state.routes[navigation.state.index];
  const navigationOptions = {};
  if (routeName !== 'top') {
    navigationOptions.tabBarVisible = false;
    return navigationOptions;
  }
  return null;
};

export default HomeNavigator;
