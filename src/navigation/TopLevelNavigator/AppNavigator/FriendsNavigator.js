// import React from 'react';
import { createStackNavigator } from 'react-navigation';
// SCREENS
import FriendsScreen from '../../../screens/FriendsScreen';
import AccountNavigator from './AccountNavigator';
import UserDetailScreen from '../../../screens/UserDetailScreen';

const FriendsNavigator = createStackNavigator(
  {
    friends: { screen: FriendsScreen },
    userDetail: { screen: UserDetailScreen },
    account: {
      screen: AccountNavigator,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    headerMode: 'screen',
  }
);

FriendsNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

export default FriendsNavigator;
