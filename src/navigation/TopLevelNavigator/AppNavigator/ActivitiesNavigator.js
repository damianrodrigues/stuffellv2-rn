// @flow
import { createStackNavigator } from 'react-navigation';
// SCREENS
import AccountScreen from '../../../screens/AccountScreen';
import ActivtiesScreen from '../../../screens/ActivtiesScreen';
import ConversationDetailScreen from '../../../screens/ConversationDetailScreen';
import UserDetailScreen from '../../../screens/UserDetailScreen';
import CardDetailScreen from '../../../screens/CardDetailScreen';

// Navigator
// =================================
const ActivitiesNavigator = createStackNavigator(
  {
    top: { screen: ActivtiesScreen },
    conversation: { screen: ConversationDetailScreen },
    userDetail: { screen: UserDetailScreen },
    cardDetail: { screen: CardDetailScreen },
    account: { screen: AccountScreen },
  },
  {
    headerMode: 'screen',
  }
);

ActivitiesNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

// EXPORTED NAVIGATOR
// =================================
export default ActivitiesNavigator;
