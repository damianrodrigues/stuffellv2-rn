// @flow
import * as React from 'react';
import { View, Platform } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { BottomTabBar } from 'react-navigation-tabs';
import { createBottomTabNavigator } from 'react-navigation';
// NAVIGATORS
import HomeNavigator from './HomeNavigator';
import SearchNavigator from './SearchNavigator';
import FriendsNavigator from './FriendsNavigator';
import ActivitiesNavigator from './ActivitiesNavigator';
// HELPERS
import { StyleHelpers } from '../../../lib/helpers';
import { styleConstants } from '../../../lib/config';
// COMPONENTS
import TabNoticationIcon from '../../../components/common/TabNoticationIcon';

// AppNavigator
// =================================
const APP_NAVIGATOR_OPTIONS = {
  swipeEnabled: false,
  lazy: false,
  initialRouteName: 'home',
  tabBarComponent: props => <BottomTabBar {...props} />,
  tabBarOptions: {
    showLabel: false,
    activeTintColor: styleConstants.color.primary,
    inactiveTintColor: styleConstants.color.lightGrey,
    style: {
      backgroundColor: '#fff',
      height: StyleHelpers.normalizeHeight(45),
      borderTopWidth: 0,
      borderTopColor: styleConstants.color.black5,
      marginTop: Platform.OS === 'android' ? 24 : 0,
      elevation: 5,
      shadowOffset: { width: 0, height: 1 },
      shadowRadius: 3,
      shadowOpacity: 0.15,
    },
  },
  navigationOptions: {
    tabBarOnPress: ({ navigation, defaultHandler }) => {
      const { state } = navigation;

      if (state.key === 'newPost') {
        navigation.navigate('modalScreen', { modalType: 'newPost' });
      } else {
        defaultHandler();
      }
    },
  },
};

const NewPostScreen = () => <View />;

const APP_NAVIGATOR_ROUTES = {
  home: {
    screen: HomeNavigator,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <TabNoticationIcon type="main" name="home" tintColor={tintColor} />
      ),
    }),
  },
  search: {
    screen: SearchNavigator,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Feather name="search" size={StyleHelpers.normalizeWidth(30)} color={tintColor} />
      ),
    }),
  },
  newPost: {
    screen: NewPostScreen,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <Feather name="camera" size={StyleHelpers.normalizeWidth(30)} color={tintColor} />
      ),
    }),
  },
  friends: {
    screen: FriendsNavigator,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <TabNoticationIcon type="friends" name="users" tintColor={tintColor} />
      ),
    }),
  },
  activity: {
    screen: ActivitiesNavigator,
    navigationOptions: () => ({
      tabBarIcon: ({ tintColor }) => (
        <TabNoticationIcon type="activity" name="bell" tintColor={tintColor} />
      ),
    }),
  },
};

export default createBottomTabNavigator(APP_NAVIGATOR_ROUTES, APP_NAVIGATOR_OPTIONS);
