// TOP LEVEL IMPORTS
import React from 'react';
import { Platform, TextInput, Text, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import { MaterialIcons, Feather } from '@expo/vector-icons';
import Sentry from 'sentry-expo';
// ROUTES
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import { MenuProvider } from 'react-native-popup-menu';
import AppRoutes from './src/navigation';
// APOLLO
import client from './src/apollo/ApolloClient';
// REDUX
import { store } from './src/redux/store';
// LIB
import { appConfig } from './src/lib/config';
// your entry point

// DEFAULT PROPS FOR THE APP
// ============================================
// Text.defaultProps.allowFontScaling = false;
// TextInput.defaultProps.allowFontScaling = false;

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    }

    return (
      <MenuProvider>
        <Provider store={store}>
          <ApolloProvider client={client}>
            <View style={styles.container}>
              <StatusBar barStyle="light-content" />
              <AppRoutes />
            </View>
          </ApolloProvider>
        </Provider>
      </MenuProvider>
    );
  }

  componentDidMount() {
    Sentry.config(appConfig.sentry.dsn).install();
  }

  _loadResourcesAsync = async () =>
    Promise.all([
      Asset.loadAsync([
        require('./assets/images/splash.png'),
        require('./assets/images/avatar.jpg'),
        require('./assets/images/logo-blue.png'),
        require('./assets/images/logo-white.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...MaterialIcons.font,
        ...Feather.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});
